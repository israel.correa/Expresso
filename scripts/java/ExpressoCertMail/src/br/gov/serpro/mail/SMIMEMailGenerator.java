/*
 * TODO: Licensing
 */
package br.gov.serpro.mail;

import br.gov.serpro.applet.SmimeApplet;
import br.gov.serpro.cert.DigitalCertificate;
import br.gov.serpro.cert.WrongPasswordException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.activation.DataHandler;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource;
import net.htmlparser.jericho.*;
import org.bouncycastle.mail.smime.SMIMEException;

/**
 *
 * @author 75779242020
 */
public class SMIMEMailGenerator {
    
    private SmimeApplet applet;
    private DigitalCertificate dc;
    private Map<String, Object> messageModel;
    private String userAgent;

    public SMIMEMailGenerator(SmimeApplet applet, DigitalCertificate dc, Map<String, Object> messageModel, String userAgent) {
        this.applet = applet;
        this.dc = dc;
        this.messageModel = messageModel;
        this.userAgent = userAgent;
    }
    
    /**
     * 
     * @param attachmentsRef
     * @param userAgent
     * @return
     * @throws URISyntaxException
     * @throws UnsupportedEncodingException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws MessagingException
     */
    private Map<String, Part> _getAttachments(boolean isEmbeddedImage) throws
            URISyntaxException, UnsupportedEncodingException, IOException, NoSuchAlgorithmException,
            KeyManagementException, MessagingException
    {
       
        List<Map<String, Object>> attachmentsRef = isEmbeddedImage ? 
                (List<Map<String, Object>>) messageModel.get("embedded_images") :
                (List<Map<String, Object>>) messageModel.get("attachments");
            
        
        Map<String, Part> attachments = new HashMap<String, Part>(attachmentsRef.size());
        for (Map<String, Object> attach : attachmentsRef)
        {
            String id,
                   session_id,
                   name;
            
            URL page = applet.getDocumentBase();
            URI url;
            String port = page.getPort() == -1 ? "" : ":"+page.getPort();
            if (isEmbeddedImage) {
                id = (String) attach.get("id");
                session_id = (String) attach.get("session_id");
                name = (String) attach.get("name");
                url = new URI(page.getProtocol()+"://" + page.getHost() + port
                        +"/index.php?method=Expressomail.showTempImage&tempImageId="+id);
            }
            else {
                Map<String, Object> tempFile= (Map<String, Object>) attach.get("tempFile");
                id = (String) tempFile.get("id");
                session_id = (String) tempFile.get("session_id");
                name = (String) tempFile.get("name");
                url = new URI(page.getProtocol()+"://" + page.getHost() + port
                        +"/index.php?method=Tinebase.getTempFile&id="+id);
            }
            
            SmimeApplet.FileDownloader downloader = this.applet.getDownloader(this.userAgent);
            
            MimeBodyPart part = downloader.downloadFile(url, session_id);
            part.setDisposition(isEmbeddedImage ? "inline" : "attachment");
            if (name != null) part.setFileName(MimeUtility.encodeText(name, "UTF-8", null));
            if (isEmbeddedImage) part.setHeader("Content-ID", "<"+id+">");
            attachments.put(id, part);
            
            // TODO: resolv security checking, probably properties.

        }

        return attachments;
    }
    
    private MimeBodyPart _buildUnsignedBodyPart(Map<String, Part> embeddedImages, Map<String, Part> attachments) throws MessagingException, UnsupportedEncodingException, IOException, URISyntaxException{
        
        String body = (String) this.messageModel.get("body");
        String contentType = (String) messageModel.get("content_type");
        
        MimeBodyPart completeBody = new MimeBodyPart();
        MimeMultipart mm = null;
        MimeBodyPart firstPart = new MimeBodyPart();

        MimeMultipart bodyContent = new MimeMultipart("alternative");

        MimeBodyPart textPlainPart = new MimeBodyPart();

        Source bodyHtmlSource = new Source(body);
        String textPlainBody = new Renderer(bodyHtmlSource).toString();
        textPlainPart.setDataHandler(new DataHandler(new ByteArrayDataSource(textPlainBody, "text/plain")));
        textPlainPart.setHeader("Content-Type", "text/plain; charset=UTF-8");
        textPlainPart.setHeader("Content-Transfer-Encoding", "quoted-printable");

        
        if (contentType.equalsIgnoreCase("text/html")) {
            // text/html body
        
            bodyHtmlSource.fullSequentialParse();
            OutputDocument outputBody = new OutputDocument(bodyHtmlSource);

            List<Element>  images = bodyHtmlSource.getAllElements("img");

            for (Element img : images){
                StartTag startTag = img.getStartTag();
                Map<String, String> attrs = new HashMap<String, String>();
                startTag.getAttributes().populateMap(attrs, true);

                String src = attrs.get("src");
                if (src == null) continue;
                URI imgUri = new URI(src);
                String query = imgUri.getQuery();
                // get image name
                Pattern regexp = Pattern.compile(".*method=\\w+.showTempImage&tempImageId=(.+)");
                Matcher m = regexp.matcher(query);

                if (!m.matches()) {
                    continue;
                }

                String id = m.group(1);

                attrs.put("src", "cid:"+id);

                String newTag = "<img " + Attributes.generateHTML(attrs) + " />";
                outputBody.replace(startTag, newTag);

            }

            // add text/plain
            bodyContent.addBodyPart(textPlainPart);

            MimeBodyPart textHtmlPart = new MimeBodyPart();

            textHtmlPart.setDataHandler(new DataHandler(new ByteArrayDataSource(outputBody.toString(), contentType)));
            textHtmlPart.setHeader("Content-Type", contentType+"; charset=UTF-8");
            textHtmlPart.setHeader("Content-Transfer-Encoding", "quoted-printable");

            if (!embeddedImages.isEmpty()){

                MimeMultipart relatedMultipart = new MimeMultipart("related");
                relatedMultipart.addBodyPart(textHtmlPart);

                for (String key : embeddedImages.keySet())
                {
                    relatedMultipart.addBodyPart((MimeBodyPart) embeddedImages.get(key));
                }

                MimeBodyPart relatedBodyPart = new MimeBodyPart();
                relatedBodyPart.setContent(relatedMultipart);
                bodyContent.addBodyPart(relatedBodyPart);

            } else {
                bodyContent.addBodyPart(textHtmlPart);
            }

            firstPart.setContent(bodyContent);
        } else {
            // text/plain body
            firstPart = textPlainPart;
            
        }
        
        // use MimeUtility.encode with a ByteArrayOutputStream
        if (!attachments.isEmpty()) {
            mm = new MimeMultipart("mixed");
            mm.addBodyPart(firstPart);
            for (Part attach : attachments.values())
            {
                mm.addBodyPart((MimeBodyPart) attach);
            }

            completeBody.setContent(mm);
        } else if (contentType.equalsIgnoreCase("text/html")) {
            completeBody.setContent(bodyContent);
        } else {
            completeBody = firstPart;
        }

        return completeBody;
    }
    
    // todo: need more tests
    private InternetAddress _tine2Rfc822Adress(String address) throws AddressException
    {
        Logger.getLogger(SMIMEMailGenerator.class.getName()).log(Level.SEVERE, "Address: {0}", address);
        
        Pattern regex = Pattern.compile("^(.*)\\((.*@.*)\\)$");
        Matcher match = regex.matcher(address.trim());
        String name = "";
        String mail = "";
        
        if (match.matches()) {
            Logger.getLogger(SMIMEMailGenerator.class.getName()).log(Level.SEVERE, "match: {0}", match.group());
            name = match.group(1).trim();
            mail = match.group(2).trim();
        }
        
        //name = name.trim().matches("^\".*\"$") ? name : ("\"" + name + "\"");
        InternetAddress rfc822Adress = new InternetAddress(name + " <" + mail + ">");
        
        return rfc822Adress;
    }
    
    private MimeMessage _buildMessage(MimeMultipart signedBody) throws MessagingException, UnsupportedEncodingException {
        
        MimeMessage message = new MimeMessage(Session.getInstance(System.getProperties()));

        message.setContent(signedBody);

        // Subject
        message.setSubject((String) this.messageModel.get("subject"), "UTF-8");
        // From
        Address from = new InternetAddress((String) this.messageModel.get("from_email"), (String) this.messageModel.get("from_name"), "UTF-8");
        message.setFrom(from);

        // To comply with the rest of Tine we'll have to set those headers ourselves.
        // format used: name (mail)
        ArrayList<String> addressArray = (ArrayList<String>) this.messageModel.get("to");
        for (String to : addressArray)
        {
            try {           
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            } catch (AddressException ex) {
                message.addRecipient(Message.RecipientType.TO, this._tine2Rfc822Adress(to));
            }
        }

        // CC
        addressArray = (ArrayList<String>) this.messageModel.get("cc");
        for (String cc : addressArray)
        {
            try {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(cc));
            } catch (AddressException ex) {
                message.addRecipient(Message.RecipientType.TO, this._tine2Rfc822Adress(cc));
            }
        }

        // BCC
        addressArray = (ArrayList<String>) this.messageModel.get("bcc");
        for (String bcc : addressArray)
        {
            try {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(bcc));
            } catch (AddressException ex) {
                message.addRecipient(Message.RecipientType.TO, this._tine2Rfc822Adress(bcc));
            }
        }
        
        //{ name: 'importance' },
        Object value = this.messageModel.get("importance");
        if (value != null ? ((Boolean) value).booleanValue() : false) {
            message.addHeader("Importance", "high");
        }
        
        // reading_conf
        value = this.messageModel.get("reading_conf");
        if (value != null ? ((Boolean) value).booleanValue() : false) {
            message.addHeader("Disposition-Notification-To", from.toString());
        }
        
        return message;
    }
    
//    
//    
//    public MimeMessage generateEncryptedMail(DigitalCertificate dc, Map messageModel){
//        throw new NotImplementedException();
//    }
    
    /**
     * 
     * @param dc
     * @param messageModel
     * @param userAgent
     * @return
     * @throws MessagingException
     * @throws URISyntaxException
     * @throws UnsupportedEncodingException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws GeneralSecurityException
     * @throws SMIMEException 
     * @todo implements text/plain body
     */
    
    public MimeMessage generateSignedMail()
            throws MessagingException, URISyntaxException, UnsupportedEncodingException, IOException,
            NoSuchAlgorithmException, KeyManagementException, GeneralSecurityException, SMIMEException, WrongPasswordException {
        
        long begin = System.currentTimeMillis();
        
        // download attachments
        Map<String,Part> embbededImages = this._getAttachments(true);
        Map<String,Part> attachmentParts = this._getAttachments(false);
        long end = System.currentTimeMillis();
        System.out.println("download time: " + ((end - begin)/1000) + " seconds");
        
        // get the bodyPart of the unsigned message body.
        MimeBodyPart unsignedBodyPart = this._buildUnsignedBodyPart(embbededImages, attachmentParts);
                    
        // Complete the message
        MimeMultipart signedMultipartBody = this.dc.signMail(unsignedBodyPart);
        
        if (signedMultipartBody != null){
            // pass model, and signed multipart body
            return this._buildMessage(signedMultipartBody);
        } else {
            return null;
        }
    }
    
}
