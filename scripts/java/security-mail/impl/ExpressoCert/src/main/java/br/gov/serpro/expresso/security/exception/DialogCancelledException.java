/*
 * TODO: header
 * 
 */
package br.gov.serpro.expresso.security.exception;

/**
 *
 * @author Mario César Kolling <mario.kolling@serpro.gov.br>
 */
public class DialogCancelledException extends Exception {
    
    public DialogCancelledException(String string) {
        super(string);
    }

    public DialogCancelledException() {
        this("Dialog cancelled");
    }
    
}
