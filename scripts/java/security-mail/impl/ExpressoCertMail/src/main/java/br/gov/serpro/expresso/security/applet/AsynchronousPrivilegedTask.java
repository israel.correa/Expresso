package br.gov.serpro.expresso.security.applet;

import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.concurrent.Callable;

public class AsynchronousPrivilegedTask<T> {

    private final Callable<T> callable;
    private ExceptionHandler handler;

    public AsynchronousPrivilegedTask(Callable<T> callable) {
        this(callable, null);
    }

    public AsynchronousPrivilegedTask(Callable<T> callable, ExceptionHandler handler) {
        this.callable = callable;
        this.handler = handler;

        if((handler == null) && (callable instanceof ExceptionHandler)) {
            this.handler = (ExceptionHandler) callable;
        }
    }

    public void start() {
        new Thread() {
            @Override
            public void run() {
                try {
                    AccessController.doPrivileged(new PrivilegedExceptionAction<T>() {
                        @Override
                        public T run() throws Exception {
                            return callable.call();
                        }
                    });
                }
                catch(PrivilegedActionException ex) {
                    if(handler != null) {
                        handler.onException(ex.getException());
                    }
                }
                catch(RuntimeException ex) {
                    if(handler != null) {
                        handler.onException(ex);
                    }
                }
                finally {
                    System.gc();
                }
            }
        }
        .start();
    }
}
