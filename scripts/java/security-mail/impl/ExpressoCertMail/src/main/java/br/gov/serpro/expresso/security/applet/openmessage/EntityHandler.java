package br.gov.serpro.expresso.security.applet.openmessage;

import java.util.Deque;
import org.apache.james.mime4j.dom.Entity;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.field.AddressListField;
import org.apache.james.mime4j.dom.field.ContentDescriptionField;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.dom.field.ContentIdField;
import org.apache.james.mime4j.dom.field.ContentLanguageField;
import org.apache.james.mime4j.dom.field.ContentLengthField;
import org.apache.james.mime4j.dom.field.ContentLocationField;
import org.apache.james.mime4j.dom.field.ContentMD5Field;
import org.apache.james.mime4j.dom.field.ContentTransferEncodingField;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.dom.field.DateTimeField;
import org.apache.james.mime4j.dom.field.MailboxField;
import org.apache.james.mime4j.dom.field.MailboxListField;
import org.apache.james.mime4j.dom.field.MimeVersionField;
import org.apache.james.mime4j.dom.field.UnstructuredField;
import org.apache.james.mime4j.message.BodyPart;
import org.apache.james.mime4j.stream.Field;

public abstract class EntityHandler extends BodyHandler {

    public void entity(Entity entity, Deque<Integer> level) {}    
    public void endEntity(Entity entity, Deque<Integer> level) {}

    public void message(Message message, Deque<Integer> level) {}    
    public void endMessage(Message message, Deque<Integer> level) {}

    public void bodyPart(BodyPart bodyPart, Deque<Integer> level) {}
    public void endBodyPart(BodyPart bodyPart, Deque<Integer> level) {}

    public void header(Header header, Deque<Integer> level) {}
    public void endHeader(Header header, Deque<Integer> level) {}
    
    public void field(Field field, Deque<Integer> level) {}
    
    public void addressList(AddressListField field, Deque<Integer> level) {}
    public void contentDescription(ContentDescriptionField field, Deque<Integer> level) {}
    public void contentDisposition(ContentDispositionField field, Deque<Integer> level) {}
    public void contentId(ContentIdField field, Deque<Integer> level) {}
    public void contentLanguage(ContentLanguageField field, Deque<Integer> level) {}
    public void contentLength(ContentLengthField field, Deque<Integer> level) {}
    public void contentLocation(ContentLocationField field, Deque<Integer> level) {}
    public void contentMD5(ContentMD5Field field, Deque<Integer> level) {}
    public void contentTransferEncoding(ContentTransferEncodingField field, Deque<Integer> level) {}
    public void contentType(ContentTypeField field, Deque<Integer> level) {}
    public void dateTime(DateTimeField field, Deque<Integer> level) {}
    public void mailbox(MailboxField field, Deque<Integer> level) {}
    public void mailboxList(MailboxListField field, Deque<Integer> level) {}
    public void mimeVersion(MimeVersionField field, Deque<Integer> level) {}
    public void unstructured(UnstructuredField field, Deque<Integer> level) {}

    public void to(AddressListField field, Deque<Integer> level) {}
    public void cc(AddressListField field, Deque<Integer> level) {}
    public void bcc(AddressListField field, Deque<Integer> level) {}
    public void resentTo(AddressListField field, Deque<Integer> level) {}
    public void resentCc(AddressListField field, Deque<Integer> level) {}
    public void resentBcc(AddressListField field, Deque<Integer> level) {}
    public void replyTo(AddressListField field, Deque<Integer> level) {}

    public void sender(MailboxField field, Deque<Integer> level) {}
    public void resentSender(MailboxField field, Deque<Integer> level) {}
    
    public void from(MailboxListField field, Deque<Integer> level) {}
    public void resentFrom(MailboxListField field, Deque<Integer> level) {}

    public void date(DateTimeField field, Deque<Integer> level) {}
    public void resentDate(DateTimeField field, Deque<Integer> level) {}

    public void subject(UnstructuredField field, Deque<Integer> level) {}
    public void messageId(UnstructuredField field, Deque<Integer> level) {}

}
