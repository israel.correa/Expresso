<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'qCal' => array($baseDir . '/library/qCal/lib'),
    'Zend\\Http\\' => array($vendorDir . '/zendframework/zend-http'),
    'Zend' => array($baseDir . '/', $baseDir . '/library'),
    'TimeZoneConvert' => array($baseDir . '/library/TimeZoneConvert/lib'),
    'Syncroton' => array($baseDir . '/library/Syncroton/lib'),
    'Sabre\\VObject' => array($vendorDir . '/sabre/vobject/lib'),
    'Sabre\\HTTP' => array($vendorDir . '/sabre/dav/lib'),
    'Sabre\\DAVACL' => array($vendorDir . '/sabre/dav/lib'),
    'Sabre\\DAV' => array($vendorDir . '/sabre/dav/lib'),
    'Sabre\\CardDAV' => array($vendorDir . '/sabre/dav/lib'),
    'Sabre\\CalDAV' => array($vendorDir . '/sabre/dav/lib'),
    '' => array($baseDir . '/', $baseDir . '/library', $baseDir . '/../tests/tine20', $baseDir . '/../tests/setup'),
);
