﻿/*
 * Tine 2.0
 * 
 * @package     AppLauncher
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Alexander Stintzing <a.stintzing@metaways.de>
 * @copyright   Copyright (c) 2012 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */
 
Ext.namespace('Tine.AppLauncher');

/**
 * AppLauncherRecord grid panel
 * 
 * @namespace   Tine.AppLauncher
 * @class       Tine.AppLauncher.AppLauncherRecordGridPanel
 * @extends     Tine.widgets.grid.GridPanel
 * 
 * <p>AppLauncherRecord Grid Panel</p>
 * <p><pre>
 * </pre></p>
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Alexander Stintzing <a.stintzing@metaways.de>
 * 
 * @param       {Object} config
 * @constructor
 * Create a new Tine.AppLauncher.AppLauncherRecordGridPanel
 */
Tine.AppLauncher.AppLauncherRecordGridPanel = Ext.extend(Tine.widgets.grid.GridPanel, {

//    initComponent: function() {
//        this.initDetailsPanel();
//        Tine.AppLauncher.AppLauncherRecordGridPanel.superclass.initComponent.call(this);
//    },
    
    /**
     * @private
     */
    initComponent: function() {
//        this.recordProxy = Tine.Tasks.JsonBackend;
        
        //this.actionToolbarItems = this.getToolbarItems();
//        this.gridConfig.cm = this.getColumnModel();
        this.initFilterToolbar();
        
        this.plugins = this.plugins || [];
        this.plugins.push(/*this.action_showClosedToggle,*/ this.filterToolbar);
        
        Tine.AppLauncher.AppLauncherRecordGridPanel.superclass.initComponent.call(this);
        
        // the editGrids onEditComplete calls the focusCell after a edit operation
        // this leads to a 'flicker' effect we dont want!
        // mhh! but disabling this, breaks keynav 
        //this.grid.view.focusCell = Ext.emptyFn;
    },
    
    /**
     * initialises filter toolbar
     * @private
     */
    initFilterToolbar: function() {
        this.filterToolbar = new Tine.widgets.grid.FilterPanel({
            recordClass: Tine.Tasks.Model.Task,
            app: this.app,
            filterModels: Tine.Tasks.Model.Task.getFilterModel(),
            defaultFilter: 'query',
            filters: [
                {field: 'container_id', operator: 'equals', value: {path: Tine.Tinebase.container.getMyNodePath()}}
            ],
            plugins: [
                new Tine.widgets.grid.FilterToolbarQuickFilterPlugin()
            ]
        });
    },
    
    /**
     * @private
     */
    initDetailsPanel: function() {
        this.detailsPanel = new Tine.AppLauncher.AppLauncherRecordDetailsPanel({
            grid : this,
            app: this.app
        });
    }
});
