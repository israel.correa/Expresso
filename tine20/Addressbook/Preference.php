<?php
/**
 * Tine 2.0
 * 
 * @package     Addressbook
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2009 Metaways Infosystems GmbH (http://www.metaways.de)
 */


/**
 * backend for Addressbook preferences
 *
 * @package     Addressbook
 */
class Addressbook_Preference extends Tinebase_Preference_Abstract
{
    /**************************** application preferences/settings *****************/
    
    /**
     * default addressbook all newly created contacts are placed in
     */
    const DEFAULTADDRESSBOOK = 'defaultAddressbook';

    /**
     * default addressbook all newly created contacts are placed in
     */
    const AUTOSEARCHADDRESSBOOK = 'autosearchAddressbook';

    /**
     * have name of default favorite an a central palce
     * _("All contacts")
     */
    const DEFAULTPERSISTENTFILTER_NAME = "All contacts";

    /**
     * allow multiple contacts edition
     */
    const MULTIPLEEDIT = 'multipleEdit';

    /**
     * allow user to resolve duplicated contacts
     */
    const DUPLICATERESOLVALBLE = 'duplicateResolvable';
    
    /**
     * @var string application
     */
    protected $_application = 'Addressbook';
        
    /**************************** public functions *********************************/
    
    /**
     * get all possible application prefs
     *
     * @return  array   all application prefs
     */
    public function getAllApplicationPreferences()
    {
        $allPrefs = array(
            self::DEFAULTADDRESSBOOK,
            self::AUTOSEARCHADDRESSBOOK,
            self::DEFAULTPERSISTENTFILTER,
            self::MULTIPLEEDIT,
            self::DUPLICATERESOLVALBLE,
        );
        
        return $allPrefs;
    }
    
    /**
     * get translated right descriptions
     * 
     * @return  array with translated descriptions for this applications preferences
     */
    public function getTranslatedPreferences()
    {
        $translate = Tinebase_Translation::getTranslation($this->_application);

        $prefDescriptions = array(
            self::DEFAULTADDRESSBOOK  => array(
                'label'         => $translate->_('Default Addressbook'),
                'description'   => $translate->_('The default addressbook for new contacts'),
            ),
            self::AUTOSEARCHADDRESSBOOK  => array(
                'label'         => $translate->_('Auto search Addressbook'),
                'description'   => $translate->_('The adressbook that searchs are automaticaly made'),
            ),
            self::DEFAULTPERSISTENTFILTER  => array(
                'label'         => $translate->_('Default Favorite'),
                'description'   => $translate->_('The default favorite which is loaded on addressbook startup'),
            ),
            self::MULTIPLEEDIT  => array(
                'label'         => $translate->_('Enable multiple edit'),
                'description'   => $translate->_('Enable multiple contacts edition'),
            ),
            self::DUPLICATERESOLVALBLE  => array(
                'label'         => $translate->_('Enable resolve duplicates'),
                'description'   => $translate->_('Enable resolve duplicated contacts'),
            ),
        );
        
        return $prefDescriptions;
    }
    
    /**
     * get preference defaults if no default is found in the database
     *
     * @param string $_preferenceName
     * @param string|Tinebase_Model_User $_accountId
     * @param string $_accountType
     * @return Tinebase_Model_Preference
     */
    public function getApplicationPreferenceDefaults($_preferenceName, $_accountId = NULL, $_accountType = Tinebase_Acl_Rights::ACCOUNT_TYPE_USER)
    {
        $preference = $this->_getDefaultBasePreference($_preferenceName);
        
        switch($_preferenceName) {
            case self::DEFAULTADDRESSBOOK:
                $this->_getDefaultContainerPreferenceDefaults($preference, $_accountId);
                break;
            case self::AUTOSEARCHADDRESSBOOK:
                $this->_getAutomaticContainerPreferenceDefaults($preference, $_accountId);
                break;
            case self::DEFAULTPERSISTENTFILTER:
                $preference->value          = Tinebase_PersistentFilter::getPreferenceValues('Addressbook', $_accountId, self::DEFAULTPERSISTENTFILTER_NAME);
                break;
            case self::MULTIPLEEDIT:
            case self::DUPLICATERESOLVALBLE:
                $preference->value      = 0;
                $preference->type       = Tinebase_Model_Preference::TYPE_FORCED;
                $preference->options    = '<?xml version="1.0" encoding="UTF-8"?>
                    <options>
                        <special>' . Tinebase_Preference_Abstract::YES_NO_OPTIONS . '</special>
                    </options>';
                break;
            default:
                throw new Tinebase_Exception_NotFound('Default preference with name ' . $_preferenceName . ' not found.');
        }
        
        return $preference;
    }
    
    
     /**
     * adds defaults to default container pref
     * 
     * @param Tinebase_Model_Preference $_preference
     * @param string|Tinebase_Model_User $_accountId
     * @param string $_appName
     */
    protected function _getAutomaticContainerPreferenceDefaults(Tinebase_Model_Preference $_preference, $_accountId, $_appName = NULL, $_optionName = self::SHAREDCONTAINER_OPTIONS)
    {
        $appName = ($_appName !== NULL) ? $_appName : $this->_application;
        
        $accountId = ($_accountId) ? $_accountId : Tinebase_Core::getUser()->getId();
        $containers = Tinebase_Container::getInstance()->getSharedContainer(Tinebase_Core::getUser(), $appName, Tinebase_Model_Grants::GRANT_READ);
        
        $_preference->value  = $containers->getFirstRecord()->getId();
        $_preference->options = '<?xml version="1.0" encoding="UTF-8"?>
            <options>
                <special>' . $_optionName . '</special>
            </options>';
    }
}
