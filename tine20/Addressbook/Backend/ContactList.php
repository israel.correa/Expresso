<?php
/**
 * Tine 2.0
 *
 * @package     Addressbook
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cassiano Dal Pizzol <cassiano.dalpizzol@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2014 SERPRO <http://www.serpro.gov.br>
 *
 */

/**
 * sql backend class for the Contact List
 *
 * @package     Addressbook
 */
class Addressbook_Backend_ContactList extends Tinebase_Backend_Sql_Abstract
{
    /**
     * Table name without prefix
     *
     * @var string
     */
    protected $_tableName = 'addressbook_list_members';

    /**
     * Model name
     *
     * @var string
     */
    protected $_modelName = 'Addressbook_Model_ContactList';

    /**
     * key in $_validators/$_properties array for the filed which
     * represents the identifier
     *
     * @var string
     */
    protected $_identifier = 'list_id';

    /**
     * Return the contacts (name and email) of a list
     *
     * @param type $_listId
     * @param type $_pagination
     */
    public function searchListContacts($_listId, $_pagination)
    {
        $filter = new Addressbook_Model_ContactListFilter($_listId);
        if (!($_pagination instanceof Tinebase_Model_Pagination)){
            $_pagination = null;
        }
            $return = $this->search($filter, $_pagination, array('list_id', 'contact_id'));
            $arrResult = $return->toArray();
        return array('results' => $arrResult, 'totalcount' => count($arrResult));
    }

    /**
     * Search for records matching given filter
     *
     * @param  Tinebase_Model_Filter_FilterGroup    $_filter
     * @param  Tinebase_Model_Pagination            $_pagination
     * @param  array|string|boolean                 $_cols columns to get, * per default / use self::IDCOL or TRUE to get only ids
     * @return Tinebase_Record_RecordSet|array
     */
    public function search(Tinebase_Model_Filter_FilterGroup $_filter = NULL, Tinebase_Model_Pagination $_pagination = NULL, $_cols = '*')
    {
        if ($_pagination === NULL) {
            $_pagination = new Tinebase_Model_Pagination(NULL, TRUE);
        }

        // legacy: $_cols param was $_onlyIds (boolean) ...
        if ($_cols === TRUE) {
            $_cols = self::IDCOL;
        } else if ($_cols === FALSE) {
            $_cols = '*';
        }

        // (1) eventually get only ids or id/value pair
        list($colsToFetch, $getIdValuePair) = $this->_getColumnsToFetch($_cols, $_filter, $_pagination);

        $select = $this->_getSelect($_cols);

        if ($_filter !== NULL) {
            $this->_addFilter($select, $_filter);
        }

        $select->join(
                array('addressbook' => $this->_tablePrefix . 'addressbook'),
                $this->_db->quoteIdentifier('addressbook.id') . ' = ' . $this->_db->quoteIdentifier($this->_tableName . '.' . 'contact_id'),
                array(Addressbook_Model_ContactList::NAMEATTRIBUTE, Addressbook_Model_ContactList::EMAILATTRIBUTE)
            );

        $this->_addSecondarySort($_pagination);
        $_pagination->appendPaginationSql($select);
        Tinebase_Backend_Sql_Abstract::traitGroup($select);

        if ($getIdValuePair) {
            return $this->_fetch($select, self::FETCH_MODE_PAIR);
        } elseif($_cols === self::IDCOL) {
            return $this->_fetch($select);
        }

        $rows = $this->_fetch($select, self::FETCH_ALL);
        if (empty($rows)) {
            return new Tinebase_Record_RecordSet($this->_modelName);
        } else {
            return $this->_rawDataToRecordSet($rows);
        }

        // (2) get other columns and do joins
        $ids = $this->_fetch($select);
        if (empty($ids)) {
            return new Tinebase_Record_RecordSet($this->_modelName);
        }
        $select = $this->_getSelect($_cols);

        $this->_addWhereIdIn($select, $ids);
        $_pagination->appendSort($select);

        $rows = $this->_fetch($select, self::FETCH_ALL);

        return $this->_rawDataToRecordSet($rows);
    }
}
