/*
 * Tine 2.0
 *
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cassiano Dal Pizzol <cassiano.dalpizzol@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2014 SERPRO (http://www.serpro.gov.br)
 */

/**
 * list model
 */
Tine.Addressbook.Model.ListContact = Tine.Tinebase.data.Record.create([
   {name: 'list_id'},
   {name: 'contact_id'},
   {name: 'n_fn'},
   {name: 'email'}
], {
    appName: 'Addressbook',
    modelName: 'ListContact',
    idProperty: 'contact_id',
    titleProperty: 'n_fn',
    // ngettext('List', 'Lists', n); gettext('Lists');
    recordName: 'ListContact',
    recordsName: 'ListsContacts',
    //containerProperty: 'container_id',
    // ngettext('Addressbook', 'Addressbooks', n); gettext('Addressbooks');
    containerName: 'Addressbook',
    containersName: 'Addressbooks'
});


/**
 * get filtermodel of contact model
 *
 * @namespace Tine.Addressbook.Model
 * @static
 * @return {Array} filterModel definition
 */
Tine.Addressbook.Model.ListContact.getFilterModel = function() {
    var app = Tine.Tinebase.appMgr.get('Addressbook');

    var typeStore = [['contact', app.i18n._('Contact')], ['user', app.i18n._('User Account')]];

    return [
        {label: app.i18n._('Name'),     field: 'n_fn' },
        {label: app.i18n._('E-Mail'),   field: 'email'}
    ];
};

/**
 * default timesheets backend
 */
Tine.Addressbook.listContactBackend = new Tine.Tinebase.data.RecordProxy({
    appName: 'Addressbook',
    modelName: 'ListContact',
    recordClass: Tine.Addressbook.Model.ListContact
});
