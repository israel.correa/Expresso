<?php
/**
 * Tine 2.0
 * 
 * @package     Messenger
 * @subpackage  Model
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 *
 */

/**
 * PreferenceRecord filter Class
 * 
 * @package     Messenger
 * @subpackage  Model
 */
class Messenger_Model_PreferenceRecordFilter extends Tinebase_Model_Filter_FilterGroup 
{
    /**
     * @var string class name of this filter group
     *      this is needed to overcome the static late binding
     *      limitation in php < 5.3
     */
    protected $_className = 'Messenger_Model_PreferenceRecordFilter';
    
    /**
     * @var string application of this filter group
     */
    protected $_applicationName = 'Messenger';
    
    /**
     * @var string name of model this filter group is designed for
     */
    protected $_modelName = 'Messenger_Model_PreferenceRecord';
    
    protected $_defaultFilter = 'query';
    
    /**
     * @var array filter model fieldName => definition
     */
    protected $_filterModel = array(
       'query'  => array('filter' => 'Tinebase_Model_Filter_Query', 'options' => array('fields' => array('account_id', /*'...'*/))),
       'id'     => array('filter' => 'Tinebase_Model_Filter_Id'),
       'name'   => array('filter' => 'Tinebase_Model_Filter_Text'),
    );
}
