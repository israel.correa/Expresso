Ext.ns('Tine.Messenger');

Tine.Messenger.ChatTab = Ext.extend(Ext.Panel, {

    closable    : true,
    ctid        : '',   

    initComponent: function() {

        Tine.Messenger.ChatTab.superclass.initComponent.apply(this, arguments);

        this.addListener({
            beforeclose: {
                fn: this.onClose,
                scope: this
            }
        });
    },

    onClose: function(p) {
        var chatWindow = Ext.getCmp(this.ctid);
        if (chatWindow) {
            chatWindow.setMinimized(false);
            chatWindow.hide();
            Tine.Messenger.ChatHandler.downloadChatText(chatWindow);
        }
        return true;
    }

});

Ext.reg('chattab', Tine.Messenger.ChatTab);