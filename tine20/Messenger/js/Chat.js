Ext.ns('Tine.Messenger');

Tine.Messenger.Chat = Ext.extend(Ext.Window, {
    
    constructor: function () {
        var app = Tine.Tinebase.appMgr.get('Messenger');
        Ext.apply(this, {
            iconCls:        'messenger-icon',
            cls:            'messenger-chat-window',
            width:          460,
            minWidth:       400,
            height:         458,
            minHeight:      458,
            //closeAction: 'hide', //'close' - destroy the component
            collapsible:    false,
            plain:          true,
            layout:         'border',
            minimized:      false,
            hltIntId:       -1,
            tools: [
                {
                    id      : 'minimize',
                    scope   : this,
                    handler : function(event, obj) {
                        this.setMinimized(true);
                        this.hide();
                    }
                },
                {
                    id      : 'close',
                    scope   : this,
                    handler : function(event, obj) {
                        var chatTabBar = Ext.getCmp('chat-tab-bar'),
                            id = this.id.substr(MESSENGER_CHAT_ID_PREFIX.length),
                            jid = Tine.Messenger.Util.idToJid(id),
                            chatId = Tine.Messenger.ChatHandler.formatChatId(jid);
                        chatTabBar.items.each(function(item, index) {
                            if (item.ctid == chatId) {
                                chatTabBar.remove(item);
                            }
                        });
                        this.hide();
                    }
                }
            ],
            tbar: {
                items:[
                    {
                        xtype: 'button',
                        plugins: [new Tine.Messenger.BrowsePlugin({})],
                        itemId: 'messenger-chat-send',
                        icon: 'Messenger/res/page_go.png',
                        tooltip: app.i18n._('Send file'),
                        handler: function(filebrowser) { // has second argument: EventObject (optional)
                            var window_chat = filebrowser.component.ownerCt.ownerCt,
                                id = window_chat.id.substr(MESSENGER_CHAT_ID_PREFIX.length),
                                jid = Tine.Messenger.Util.idToJid(id);
                            if (!Tine.Messenger.RosterHandler.isContactUnavailable(jid)) {
                                Tine.Messenger.FileTransfer.sendRequest(jid, filebrowser);
                            } else {
                                Ext.MessageBox.show({
                                    title : app.i18n._('User unavailable!'),
                                    msg : app.i18n._('User unavailable!'),
                                    buttons : Ext.MessageBox.OK,
                                    icon : Ext.MessageBox.INFO
                                });
                            }
                        }
                    },
                    {
                        xtype: 'button',
                        itemId: 'messenger-chat-video',
                        icon: 'Messenger/res/webcam.png',
                        tooltip: app.i18n._('Start video chat'),
                        disabled: true,
                        handler: function() {
                            var window_chat = this.ownerCt.ownerCt,
                            id = window_chat.id.substr(MESSENGER_CHAT_ID_PREFIX.length),
                            jid = Tine.Messenger.Util.idToJid(id);

                            Tine.Messenger.VideoChat.startVideo(window_chat, id, jid);
                        }
                     },
                     {
                        xtype: 'button',
                        itemId: 'messenger-chat-emoticons',
                        icon: 'Messenger/res/emoticons/smile.png',
                        tooltip: app.i18n._('Choose a Emoticon'),
                        listeners: {
                            scope: this,
                            click: function() {
                                var emoticonWindow,
                                    mainChatWindow = this,
                                    emoticonsPath = 'Messenger/res/emoticons',
                                    check = [];

                                if (Ext.getCmp('emoticon-window-choose')) {
                                    emoticonWindow = Ext.getCmp('emoticon-window-choose');
                                } else {
                                    emoticonWindow = new Ext.Window({
                                        id: 'emoticon-window-choose',
                                        autoScroll: true,
                                        closeAction: 'hide',
                                        layout: {
                                            type: 'table',
                                            columns: 10
                                        },
                                        margins: {
                                            top: 5,
                                            left: 5
                                        },
                                        height: 175,
                                        width: 290,
                                        title: app.i18n._('Choose a Emoticon')
                                    });

                                    Ext.each(EMOTICON.emoticons, function (item, index) {
                                        if (check.indexOf(EMOTICON.translates[index]) < 0) {
                                            check.push(EMOTICON.translates[index]);
                                            emoticonWindow.add({
                                                xtype: 'button',
                                                icon: emoticonsPath + '/' + EMOTICON.translates[index] + '.png',
                                                cls: 'emoticon-button',
                                                tooltip: EMOTICON.emoticons[index],
                                                emoticon: item,
                                                handler: function () {
                                                    var textfield = mainChatWindow.getComponent(2).getComponent(0);
                                                    textfield.insertAtCursor('<img src="' + this.icon + '" />');
                                                    emoticonWindow.close();
                                                }
                                            });
                                        }
                                    });
                                }

                                emoticonWindow.show();
                            }
                        }
                     },
                     {
                         xtype: 'button',
                         itemId: 'messenger-history',
                         icon: 'Messenger/res/folder.png',
                         tooltip: app.i18n._('Contact Chat History'),
                         listeners: {
                             scope: this,
                             click: function (button) {
                                 var mainChatWindow = this,
                                     id = mainChatWindow.id.substr(MESSENGER_CHAT_ID_PREFIX.length),
                                     contact_jid = Tine.Messenger.Util.idToJid(id),
                                     jid = Strophe.getBareJidFromJid(Tine.Tinebase.appMgr.get('Messenger').getConnection().jid);

                                 var history_window = new Tine.Messenger.HistoryWindow({
                                     jid: jid,
                                     contact: contact_jid
                                 });

                                 history_window.show();
                             }
                         }
                     }
                ]
            },
            listeners: {
                beforerender: function(_box){
                    Tine.Messenger.AddItems(_box);
                },
                resize: function(_box, _width, _height){
                    Tine.Messenger.ChatHandler.adjustChatAreaHeight(_box.id, _width, _height);
                },
                show: function () {
                    this.setTextfieldFocus();
                    if (this.hltIntId != -1) {
                        window.clearInterval(this.hltIntId);
                        this.hltIntId = -1;
                    }
                },
                activate: function () {
                    this.setTextfieldFocus();
                },
                expand: function () {
                    this.setTextfieldFocus();
                },
                move: function(_box){
                    Tine.Messenger.Window._onMoveWindowAction(_box);
                },
                beforehide: function(_box){
                    // only if the chat being closed is the one using videochat
                    if(Tine.Messenger.VideoChat.jid != null && Tine.Messenger.VideoChat.getChatWindow(Tine.Messenger.VideoChat.jid).id == _box.id){
                        Tine.Messenger.VideoChat.hangup(_box);
                    }
                },
                beforecollapse: function(_box){
                    if(Tine.Messenger.VideoChat.state != VideoChatStates.IDLE){
                        return false;
                    }
                },
                beforedestroy: function(){
                    var chatTabBar = Ext.getCmp('chat-tab-bar'),
                        id = this.id.substr(MESSENGER_CHAT_ID_PREFIX.length),
                        jid = Tine.Messenger.Util.idToJid(id),
                        chatId = Tine.Messenger.ChatHandler.formatChatId(jid);

                    chatTabBar.items.each(function(item, index) {
                        if (item.ctid === chatId) {
                            // remove tab on destroy...
                            chatTabBar.remove(item);
                        }
                    });
                }
            }
        });
  
        Tine.Messenger.Chat.superclass.constructor.apply(this, arguments);

        //this.addTabToTaskBar();
    },
    
    addTabToTaskBar: function() {
        var chatTabBar = Ext.getCmp('chat-tab-bar'),
            id = this.id.substr(MESSENGER_CHAT_ID_PREFIX.length),
            jid = Tine.Messenger.Util.idToJid(id),
            contact = Tine.Messenger.RosterHandler.getContactElement(jid),
            chatId = Tine.Messenger.ChatHandler.formatChatId(jid),
            isTabActive = false;

        chatTabBar.items.each(function(item, index) {
            if (item.ctid === chatId) {
                // this window is already in tab chat bar...
                chatTabBar.setActiveTab(item);
                isTabActive = true;
            }
        });
        
        if (isTabActive) return;

        chatTabBar.add(
            {
                ctid            : chatId,
                xtype           : 'chattab',
                title           : contact.attributes.text
            }
        );

        var index = chatTabBar.items.getCount()-1;
        chatTabBar.setActiveTab(index);
        
        Ext.get(chatTabBar.getTabEl(index)).on('click',
            function(event) {
                var chatWindow = Ext.getCmp(chatId);
                if (chatWindow) {
                    chatWindow.setMinimized(false);
                    chatWindow.show();
               }
            }
        );
    },
    
    highlightTab: function(chatTabBar, chatId) {
        if (!chatTabBar) {
            chatTabBar = Ext.getCmp('chat-tab-bar');
            var id = this.id.substr(MESSENGER_CHAT_ID_PREFIX.length);
            var jid = Tine.Messenger.Util.idToJid(id);
            chatId = Tine.Messenger.ChatHandler.formatChatId(jid);
            if (this.hltIntId == -1) {
                var that = this;
                this.hltIntId = window.setInterval(function () {
                    that.highlightTab(chatTabBar, chatId);
                }, 2000);
            }
        }

        chatTabBar.items.each(function(item, index) {
            if (item.ctid === chatId) {
                Ext.fly(chatTabBar.getTabEl(index)).child('.x-tab-strip-text')
                    .fadeOut('t', {
                        easing: 'easeOut',
                        duration: 1
                    })
                    .fadeIn('t', {
                        easing: 'easeIn',
                        duration: 1
                    });
            }
        });
    },
            
    getMinimized: function() {
        return this.minimized;
    },
    
    setMinimized: function(status) {
        this.minimized = status;
    },
    
    setTextfieldFocus: function () {
        //this.getComponent('messenger-chat-textchat').getComponent(1).getComponent(0).focus(false, 200); // foco no textfield
	this.getComponent(2).getComponent(0).focus(false, 200);
        var chatTabBar = Ext.getCmp('chat-tab-bar'),
            id = this.id.substr(MESSENGER_CHAT_ID_PREFIX.length),
            jid = Tine.Messenger.Util.idToJid(id),
            chatId = Tine.Messenger.ChatHandler.formatChatId(jid);
        chatTabBar.items.each(function(item, index) {
            if (item.ctid === chatId) {
                chatTabBar.setActiveTab(item);
            }
        });
    },
    
    setFileTransferDisabled: function(jid, unavailable){
	unavailable = typeof unavailable !== 'undefined' ? unavailable : Tine.Messenger.RosterHandler.isContactUnavailable(jid);
	
	if(this.getTopToolbar !== undefined){
	    this.getTopToolbar().getComponent('messenger-chat-send').setDisabled(unavailable);
	}
    }
    
});