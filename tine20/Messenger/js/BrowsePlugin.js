/*
 * Tine 2.0
 * 
 * @license     New BSD License
 * @author      loeppky - based on the work done by MaximGB in Ext.ux.UploadDialog (http://extjs.com/forum/showthread.php?t=21558)
 *
 */
Ext.ns('Tine.Messenger');


/**
 * @namespace   Tine.Messenger
 * @class       Tine.Messenger.BrowsePlugin
 */
Tine.Messenger.BrowsePlugin = Ext.extend(Ext.ux.file.BrowsePlugin, {   

    /**
    * returns z-index
    * If the component has no z-index: 30000
    * If the component has a z-index: this z-index + 30000
    * 
    * Method overrided to return zIndex granter than the original (300), IE8 bug
    * 
    */
    getNextZindex: function() {
        var zElement = this.component.getEl().up('div[style*=z-index]');
        var zIndex = zElement === null ? 0 : parseInt(zElement.getStyle('z-index'));
        
        return zIndex + 30000; // IE8 in popup windows and menus needs zIndex greater than 300 to be visible
    }

});
