Ext.ns('Tine.Messenger');

Tine.Messenger.HtmlEditor = Ext.extend(Ext.form.HtmlEditor, {
    frame : true,
    height: 80,
    boxMinHeight: 50,
    autoScroll: true,
    enableSourceEdit: false,
    enableLinks: false,
    enableColors: false,
    enableAlignments: false,
    enableLists: false,
    enableFontSize: false,
    initComponent : function() {
        Tine.Messenger.HtmlEditor.superclass.initComponent.call(this);
        this.addEvents('submit');
    },
    initEditor : function(editor) {
        Tine.Messenger.HtmlEditor.superclass.initEditor.call(this);

        Ext.EventManager.on(this.iframe.contentWindow.document.body, 'keypress', this.keyPress, this);
        Ext.EventManager.on(this.iframe.contentWindow.document.body, 'keydown', this.keyPress, this);
        
        var i18n = Tine.Tinebase.appMgr.get('Messenger').i18n,
            title = '<b>' + i18n._('Send Message') + ' (ENTER)</b><br/>' + i18n._('Sends the formated message below') + '.';
        this.getToolbar().addButton([
            {
                xtype: 'tbseparator'
            },
            {
                icon: 'Messenger/res/send-message.png',
                scope: this,
                overflowText: i18n._('Send Message'),
                tooltip: title,
                handler: function (cmp, e) {
                    var chat_id = this.ownerCt.ownerCt.id,
                        type = this.ownerCt.ownerCt.type,
                        private_chat = this.ownerCt.ownerCt.privy,
                        old_id = this.ownerCt.ownerCt.initialConfig.id,
                        message = this.getValue();
                    
                    this.sendMessage(chat_id, type, private_chat, old_id, message, e);
                    this.setValue(this.defaultValue);
                }
            }
        ]);
        this.getToolbar().doLayout();
        
        this.focus();
        Tine.Messenger.Chat.alreadySentComposing = false;
        
        this.onFirstFocus();
    },
    getValue: function () {
        var value = this.superclass().getValue.call(this);

        value = value.replace(/<div.*?>(.+?)<\/div>/g, '<br>$1')
                     .replace(/<b>(.+)<\/b>/g, '<strong>$1</strong>')
                     .replace(/<i>(.+)<\/i>/g, '<em>$1</em>')
                     .replace(/<u>(.+)<\/u>/g, '<span style="text-decoration: underline;">$1</span>')
                     .replace(/<font (face="(.+?)")+>(.+?)<\/font>/g, '<span style="font-family: $2;">$3</span>')
                     .replace(/<br>$/, '');

        return value;
    },
    keyPress: function(e) {
        var chat_id = this.ownerCt.ownerCt.id,
            type = this.ownerCt.ownerCt.type,
            private_chat = this.ownerCt.ownerCt.privy,
            old_id = this.ownerCt.ownerCt.initialConfig.id,
            message = this.getValue();
            
        if (!e.shiftKey && e.getKey() == Ext.EventObject.ENTER) {
            if (this.isVoidMessage()) {
                this.setValue(this.defaultValue);
            } else {
                this.sendMessage(chat_id, type, private_chat, old_id, message, e);
            }
            this.setValue(this.defaultValue);
            e.stopEvent();
        } else {
            this.sendChatStatus(type, chat_id);
        }
    },
    sendChatStatus: function (type, id) {
        // Envia apenas na primeira quando começa a digitar
        if(type == 'chat' && !Tine.Messenger.Chat.alreadySentComposing) {
            Tine.Messenger.ChatHandler.sendState(id, 'composing');
            Tine.Messenger.Chat.alreadySentComposing = true;
        }
        if(type == 'groupchat' && !Tine.Messenger.Chat.alreadySentComposing) {
            try {
                Tine.Messenger.Groupie.sendState(id, 'composing');
            } catch(err) {
                Tine.Messenger.Log.error(err);
            }
            Tine.Messenger.Chat.alreadySentComposing = true;
        }
        // Verifica se parou de digitar
        if (Tine.Messenger.Chat.checkPauseInterval)
            window.clearTimeout(Tine.Messenger.Chat.checkPauseInterval);
        Tine.Messenger.Chat.checkPauseInterval = window.setTimeout(function () {
            Tine.Messenger.ChatHandler.sendState(id, 'paused');
            Tine.Messenger.Chat.alreadySentComposing = false;
        }, 2000);
    },
    sendMessage: function (chat_id, type, private_chat, old_id, message, ev) {
        if (!this.voidMessage(message)) {
            if(type == 'chat' || private_chat) {
                window.clearTimeout(Tine.Messenger.Chat.checkPauseInterval);
                Tine.Messenger.Chat.checkPauseInterval = null;
                Tine.Messenger.Chat.alreadySentComposing = false;
                if(type == 'chat')
                    Tine.Messenger.ChatHandler.sendMessage(message, chat_id);
                if(type == 'groupchat')
                    Tine.Messenger.Groupie.sendPrivMessage(message, chat_id, old_id);
            } else {
                this.sendMUCMessage(private_chat, chat_id, message);
            }
        }
        
        //this.setValue(this.defaultValue);
    },
    sendMUCMessage: function (private_chat, chat_id, message) {
        if(private_chat)
            Tine.Messenger.Groupie.sendPrivMessage(message, chat_id);
        else
            Tine.Messenger.Groupie.sendPublMessage(message, chat_id);
    },
    isVoidMessage: function () {
        return this.getValue() == '' || this.getValue() == '<br>' || this.getValue() == '<br/>';
    },
    voidMessage: function (message) {
        return message == '' || message == '<br>' || message == null;
    },

    /**
     * Inserts the passed text at the current cursor position. Note: the editor must be initialized and activated
     * to insert text.
     * @param {String} text
     */
    insertAtCursor : function(text){
        if(!this.activated){
            return;
        }

        if(this.isIE()){
            this.win.focus();
            var doc = this.getDoc(),
                r = doc.selection.createRange();
            if(r){
                r.pasteHTML(text);
                this.syncValue();
                this.deferFocus();
            }
        }else{
            this.win.focus();
            this.execCmd('InsertHTML', text);
            this.deferFocus();
        }
    },

    isIE: function() {
        return Ext.isIE | Ext.isIE7 | Ext.isIE8 | Ext.isIE9 | Ext.isIE10 | Ext.isIE6;
    }
});

Ext.reg('imhtmleditor', Tine.Messenger.HtmlEditor);