
Ext.ns('Tine.Messenger');

var VideoChatStates = {
    IDLE : 0,
    CALL_CALLING : 1,
    CALL_RINGING : 2,
    CALL_ESTABLISHED : 3
};

Tine.Messenger.VideoChat = {
    
    /**
     * Indicates if the video chat functionality is enabled.
     */
    enabled: false,
    
    /**
     * The RTMFP server url.
     */
    rtmfpServerUrl: '',
    
    /**
     * The videochat state.
     */
    state: VideoChatStates.IDLE,
    
    /**
     * Far user jid.
     */
    jid: null,
    
    /**
     * Cumulus connection id.
     */
    id:null,
    
    /**
     * Cumulus connection id of the user who is this videochat connected to.
     */
    farId:null,
    
    /**
     * Videochat invite message box.
     */
    invite: null,
    
    
    /**
     * Indicates if the videochat panel is hidden.
     *
     */
    hidden : true,
    
    originalChatWidth: 400,
    
    VIDEOCHAT_OBJECT_ID : 'messenger-chat-videochat-object',
    
    flashVideoWidth: 368,
    flashVideoHeight: 276,
    isCameraAvailable: false,
    isMicrophoneAvailable: false,
    videoVersion: '0.0.41',
    
    init: function(){
	var rtmfpServerUrl = Ext.util.Format.trim(Tine.Messenger.registry.get('rtmfpServerUrl'));
	Tine.Messenger.VideoChat.enabled = (rtmfpServerUrl != '' && rtmfpServerUrl !== null);
	Tine.Messenger.VideoChat.rtmfpServerUrl = rtmfpServerUrl;
        //Tine.Messenger.VideoChat.checkCameraAndMicrophone();
    },
    
    sendStartCall: function (jid, myId) {
	var to = typeof jid == 'string' ? jid : jid.node.attributes.jid;
                       
	var info = $msg({
	    'to': to + '/' + Tine.Messenger.registry.get('resource'),
	    'type': 'videochat'
	});
	
	info.c("startcall", {
	    'id': myId,
	    'user': Tine.Messenger.Util.getJidFromConfigNoResource()
	});

	Tine.Messenger.Application.connection.send(info);         
    },
    sendRejectCall: function(item){
	
	var to = typeof item == 'string' ? item : item.node.attributes.jid;
	               
	var info = $msg({
	    'to': to + '/' + Tine.Messenger.registry.get('resource'),
	    'type': 'videochat'
	});
	
	info.c("rejectcall", {
	    'user': Tine.Messenger.Util.getJidFromConfigNoResource()
	});
	
	Tine.Messenger.Application.connection.send(info);  
    },
    
    /**
     * Sends incapable response when this client does not have flash, does not have camera....
     *
     */
    sendIncapable: function(item){
	
	var to = typeof item == 'string' ? item : item.node.attributes.jid;
	               
	var info = $msg({
	    'to': to + '/' + Tine.Messenger.registry.get('resource'),
	    'type': 'videochat'
	});
	
	info.c("incapable", {
	    'user': Tine.Messenger.Util.getJidFromConfigNoResource()
	});
	
	Tine.Messenger.Application.connection.send(info);  
    },
    sendBusy: function(jid){
                       
	var info = $msg({
	    'to': jid + '/' + Tine.Messenger.registry.get('resource'),
	    'type': 'videochat'
	});
	
	info.c("busy", {
	    'user': Tine.Messenger.Util.getJidFromConfigNoResource()
	});
	
	Tine.Messenger.Application.connection.send(info);         
    },
    sendCancelCall: function(jid){
                       
	var info = $msg({
	    'to': jid + '/' + Tine.Messenger.registry.get('resource'),
	    'type': 'videochat'
	});
	
	info.c("cancelcall", {
	    'user': Tine.Messenger.Util.getJidFromConfigNoResource()
	});
	
	Tine.Messenger.Application.connection.send(info);         
    },
    
    onStartCall: function(msg){
	var startcall = $(msg).find('startcall'),
	id = startcall.attr('id'),
	user = startcall.attr('user');
	
	var app = Tine.Tinebase.appMgr.get('Messenger');
	
        if(!Tine.Messenger.VideoChat.isFlashEnabled()){
            Tine.Messenger.VideoChat.sendIncapable(user);
            return;
        }
        
	if(Tine.Messenger.VideoChat.state == VideoChatStates.IDLE){
	    Tine.Messenger.VideoChat.state = VideoChatStates.CALL_RINGING;
		
	    Tine.Messenger.VideoChat.invite = Ext.MessageBox.show({
		title: app.i18n._('Video chat'),
		msg: String.format(
		    app.i18n._('{0} is inviting you to a video chat. Do you Accept?'), 
		    user
		),
		
		buttons: Ext.Msg.YESNO, 
		icon: Ext.MessageBox.QUESTION,
		modal: false,
		fn: function(btn) {
		    if(btn == 'yes') {
			Tine.Messenger.VideoChat.farId = id;
			Tine.Messenger.VideoChat.jid = user;
			var chat = Tine.Messenger.ChatHandler.showChatWindow(user, '', 'chat', true);
			Tine.Messenger.VideoChat.loadVideoChat(chat);
		    }
		    else{
			Tine.Messenger.VideoChat.sendRejectCall(user);
			Tine.Messenger.VideoChat.state = VideoChatStates.IDLE;
		    }
		}
	    });
	}
	else{
	    Tine.Messenger.VideoChat.sendBusy(user);
	}
	    
    },
    onRejectCall: function(msg){
	var rejectcall = $(msg).find('rejectcall'),
	user = rejectcall.attr('user');
	    
	var chat = Tine.Messenger.VideoChat.getChatWindow(Tine.Messenger.VideoChat.jid);
	Tine.Messenger.VideoChat.unloadVideoChat(chat);
	Tine.Messenger.VideoChat.hideVideoChat(chat);
	
	var app = Tine.Tinebase.appMgr.get('Messenger');
	Tine.Messenger.ChatHandler.setChatMessage(
	    Tine.Messenger.VideoChat.jid, 
	    String.format(
		app.i18n._('{0} rejected your video chat call'), 
		user
	    ),
	    app.i18n._('Info'),
	    'messenger-notify'
	    );
	Tine.Messenger.VideoChat.state = VideoChatStates.IDLE;
	
    },
    onBusy: function(msg){
	var busy = $(msg).find('busy'),
	user = busy.attr('user');
	    
	var chat = Tine.Messenger.VideoChat.getChatWindow(Tine.Messenger.VideoChat.jid);
	Tine.Messenger.VideoChat.unloadVideoChat(chat);
	Tine.Messenger.VideoChat.hideVideoChat(chat);
	
	var app = Tine.Tinebase.appMgr.get('Messenger');
	Tine.Messenger.ChatHandler.setChatMessage(
	    Tine.Messenger.VideoChat.jid, 
	    String.format(
		app.i18n._('{0} is busy for video chat'), 
		user
	    ),
	    app.i18n._('Info'),
	    'messenger-notify'
	    );
	Tine.Messenger.VideoChat.state = VideoChatStates.IDLE;
	    
    },
    onCancelCall: function(msg){
	var cancelcall = $(msg).find('cancelcall'),
	user = cancelcall.attr('user');
	    
	if( Tine.Messenger.VideoChat.invite != null){
	    Tine.Messenger.VideoChat.invite.hide();
	    
	    var app = Tine.Tinebase.appMgr.get('Messenger');
	    var chat_id = Tine.Messenger.ChatHandler.formatChatId(user);
	    if(typeof(Ext.getCmp(chat_id)) != 'undefined' && Ext.getCmp(chat_id) != null){
	    
		Tine.Messenger.ChatHandler.setChatMessage(
		    user, 
		    String.format(
			app.i18n._('One video chat call missed from {0}'), 
			user
		    ),
		    app.i18n._('Info'),
		    'messenger-notify'
		);
	    }
	}
	
	
	Tine.Messenger.VideoChat.state = VideoChatStates.IDLE;
	    
    },
    
     onIncapable: function(msg){
	var incapable = $(msg).find('incapable'),
	user = incapable.attr('user');
	    
	var chat = Tine.Messenger.VideoChat.getChatWindow(Tine.Messenger.VideoChat.jid);
	Tine.Messenger.VideoChat.unloadVideoChat(chat);
	Tine.Messenger.VideoChat.hideVideoChat(chat);
	
	var app = Tine.Tinebase.appMgr.get('Messenger');
	Tine.Messenger.ChatHandler.setChatMessage(
	    Tine.Messenger.VideoChat.jid, 
	    String.format(
		app.i18n._('{0} is incapable to start a video call'), 
		user
	    ),
	    app.i18n._('Info'),
	    'messenger-notify'
	    );
	Tine.Messenger.VideoChat.state = VideoChatStates.IDLE;
	
    },
    
    onRequest: function (msg) {
	
	if($(msg).find('startcall').length)
	    Tine.Messenger.VideoChat.onStartCall(msg);
	else if($(msg).find('rejectcall').length)
	    Tine.Messenger.VideoChat.onRejectCall(msg);
	else if($(msg).find('busy').length)
	    Tine.Messenger.VideoChat.onBusy(msg);
	else if($(msg).find('cancelcall').length)
	    Tine.Messenger.VideoChat.onCancelCall(msg);
        else if($(msg).find('incapable').length)
	    Tine.Messenger.VideoChat.onIncapable(msg);
	    
	
	return true;

    },
   
   
   
   
    // -------------------------------------------------------------------   
   
   /**
    * This user (caller) is starting a video chat
    */
   
    startVideo: function (window_chat, id, jid){

	if(Tine.Messenger.VideoChat.state == VideoChatStates.IDLE){
	    Tine.Messenger.VideoChat.jid = jid;
	    Tine.Messenger.VideoChat.loadVideoChat(window_chat);
	    Tine.Messenger.VideoChat.showVideoChat(window_chat);
	    if(Tine.Messenger.VideoChat.isFlashEnabled()){
		Tine.Messenger.VideoChat.state = VideoChatStates.CALL_CALLING;
	    }
	}
	else{
	    if(Tine.Messenger.VideoChat.jid != jid){
		var app = Tine.Tinebase.appMgr.get('Messenger');
		Tine.Messenger.ChatHandler.setChatMessage(
		    jid, 
		    app.i18n._('You are already in a video chat'), 
		    app.i18n._('Info'),
		    'messenger-notify'
		);
	    }
	}
	
	return true;
    },
        
    /**
     * Called by flash.
     * The flash component has loaded.
     */
    appLoaded: function()
    {
	Tine.Messenger.VideoChat.startApp();
    },
    
    /**
     * After flash app has loaded, let's call startApp in flash connect to RTMP or RTMFP server.
     * 
     */
    startApp:function(){
	var movie = Tine.Messenger.VideoChat.getFlashMovie(Tine.Messenger.VideoChat.VIDEOCHAT_OBJECT_ID); 
	
	if(movie && typeof(movie.startApp) != "undefined"){
	    movie.startApp(
                '{"urls":["' + Tine.Messenger.VideoChat.rtmfpServerUrl + '"]}', 
                Tine.Messenger.Util.getJidFromConfigNoResource()
            );
	}

	return true;
    },
    /**
     * Called by flash after a successfull server connection.
     * Both sides of videochat call this function. The side is identified by the state.
     * After flash got connected to server, it return its id.
     */
    myId: function(id){

	Tine.Messenger.VideoChat.id = id;
	if(Tine.Messenger.VideoChat.state == VideoChatStates.CALL_CALLING){ // Caller
	    Tine.Messenger.VideoChat.acceptCallFrom(Tine.Messenger.VideoChat.jid);
	    Tine.Messenger.VideoChat.sendStartCall(Tine.Messenger.VideoChat.jid, Tine.Messenger.VideoChat.id);
	}
	else if(Tine.Messenger.VideoChat.state == VideoChatStates.CALL_RINGING){ // Callee
	    Tine.Messenger.VideoChat.placeCall(Tine.Messenger.VideoChat.farId);
	}

    },
    
    /**
     * Called by flash after an unsuccessfull server connection.
     */
    jsConnectionFailed: function(){
        
        var app = Tine.Tinebase.appMgr.get('Messenger');
        
        Tine.Messenger.ChatHandler.setChatMessage(
		    Tine.Messenger.VideoChat.jid, 
		    app.i18n._('Unable to establish connection for video call (Server)'), 
		    app.i18n._('Info'),
		    'messenger-notify'
        );
            
        if(Tine.Messenger.VideoChat.state == VideoChatStates.CALL_CALLING){ // Caller
	    Tine.Messenger.VideoChat.hangup(
                Tine.Messenger.VideoChat.getChatWindow(Tine.Messenger.VideoChat.jid)
            );
	}
	else if(Tine.Messenger.VideoChat.state == VideoChatStates.CALL_RINGING){ // Callee
	    Tine.Messenger.VideoChat.sendIncapable(Tine.Messenger.VideoChat.jid);
            Tine.Messenger.VideoChat.hangup(
                Tine.Messenger.VideoChat.getChatWindow(Tine.Messenger.VideoChat.jid)
            );
	}
        
    },
    
    /**
     * The caller needs to register in flash that he accepts call from the contact (callee)
     */
    acceptCallFrom:function(jid){
	var movie = Tine.Messenger.VideoChat.getFlashMovie(Tine.Messenger.VideoChat.VIDEOCHAT_OBJECT_ID); 
	if(movie && typeof(movie.acceptCallFrom) != "undefined"){
	    movie.acceptCallFrom(jid);
	}
    },
    
    /**
     * The callee after accept call, load app and start app, invoke flash placeCall that will connect (call) to caller
     */
    placeCall: function(farId){
	var movie = Tine.Messenger.VideoChat.getFlashMovie(Tine.Messenger.VideoChat.VIDEOCHAT_OBJECT_ID); 
	if(movie && typeof(movie.placeCall) != "undefined"){
	    movie.placeCall('', farId);
	}
    },
    
    /**
     * Called by flash. On both sides, after connection establishment, flash app tells that call is started.
     */
    callStarted: function(){
	
	var chat = Tine.Messenger.ChatHandler.showChatWindow(Tine.Messenger.VideoChat.jid, '', 'chat', true);
	Tine.Messenger.VideoChat.showVideoChat(chat);
	
	Tine.Messenger.VideoChat.state = VideoChatStates.CALL_ESTABLISHED;
    },
    
    /**
     * When one (caller or callee) ends the connection. Invoke hangup in flash.
     */
    hangup: function(_box){
	
	//if(Tine.Messenger.VideoChat.state != VideoChatStates.IDLE){
	    
	    Tine.Messenger.VideoChat.hideVideoChat(_box);
	    
	    if (Tine.Messenger.VideoChat.state == VideoChatStates.CALL_CALLING){
		Tine.Messenger.VideoChat.sendCancelCall(Tine.Messenger.VideoChat.jid);
		
		// sometimes hungup before connection establishment don't fire callEnded, so force to unload
		if(_box != null)
		    Tine.Messenger.VideoChat.unloadVideoChat(_box);
		Tine.Messenger.VideoChat.state = VideoChatStates.IDLE;
		Tine.Messenger.VideoChat.jid = null;
		
	    }
	    var movie = Tine.Messenger.VideoChat.getFlashMovie(Tine.Messenger.VideoChat.VIDEOCHAT_OBJECT_ID); 
	    
	    if(movie && typeof(movie.hangup) != "undefined"){
		movie.hangup();
		
		//workaround: two flash calls to callEnded doesn't work
		Tine.Messenger.VideoChat.callEnded();
	    }
	    
	//}
	
	
    },
    
    /**
     * Called by flash. On both sides, flash tells that it has finished the call.
     */
    callEnded: function(id){
	var _jid = Tine.Messenger.VideoChat.jid;
	Tine.Messenger.VideoChat.jid = null;
	if(_jid !== null){
	    
	    var chat = Tine.Messenger.VideoChat.getChatWindow(_jid);

	    Tine.Messenger.VideoChat.hideVideoChat(chat);
	    if(chat !== null){
		Tine.Messenger.VideoChat.unloadVideoChat(chat);
	    }
	    Tine.Messenger.VideoChat.state = VideoChatStates.IDLE;
	    
	}
    },
    
    unloadVideoChat: function(_box){
	
	_box.getComponent('messenger-chat-videochat').removeAll();
	_box.doLayout();

	
    },
    
    
    
    loadVideoChat : function(_box){
	var app = Tine.Tinebase.appMgr.get('Messenger'),
	    flash = null;
	    
	Ext.FlashComponent.EXPRESS_INSTALL_URL = "Messenger/flash/swfobject/expressInstall.swf";
	
	if(Tine.Messenger.VideoChat.isFlashEnabled()){
	    flash = new Ext.FlashComponent({
		url: "Messenger/flash/ExpressoVideoChat.swf?v="+ Tine.Messenger.VideoChat.videoVersion,
		wmode:"direct",
		flashParams:{
		    wmode:"direct"
		},
		flashVars:{
		    useExternalInterface: "true", 
		    extNamespace : "Tine.Messenger.VideoChat"
		},
		id: Tine.Messenger.VideoChat.VIDEOCHAT_OBJECT_ID, 
		allowScriptAccess:"always",
		flashVersion: "10.0.0",
		swfWidth: Tine.Messenger.VideoChat.flashVideoWidth,
		swfHeight: Tine.Messenger.VideoChat.flashVideoHeight,
		expressInstall:true

	    });
	}
	else{
	    flash = new Ext.Panel({
		id : Tine.Messenger.VideoChat.VIDEOCHAT_OBJECT_ID,
		width: Tine.Messenger.VideoChat.flashVideoWidth,
		height: Tine.Messenger.VideoChat.flashVideoHeight - 20,
		html: '<div>' +
			'<h1>' + app.i18n._('Flash Player is not installed') + '</h1>' +
			'<p><a href="http://www.adobe.com/go/getflashplayer" target="_blank"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>' +
		      '</div>'
	    });
	}
	    
	// create exit button and mute
	var options = new Ext.Panel({
	    itemId: 'messenger-chat-videochat-options',
	    height: 30,
	    width: Tine.Messenger.VideoChat.flashVideoWidth,
	    layout : {
		type  : 'hbox',
		pack  : 'center',
		align : 'middle'
	    },
	    border: false,
	    items: [
	    {
		xtype: 'button',
		//icon: 'Messenger/res/icon_cancel.png',
		text: app.i18n._('End'),
		tooltip: app.i18n._('End video chat'),
		handler : function() { 
		    Tine.Messenger.VideoChat.hangup(
			Tine.Messenger.VideoChat.getChatWindow(Tine.Messenger.VideoChat.jid)
		    );
		}
	    }
	    //,new Ext.Button({text:'Mute'})
	    ]
	});
	
	_box.getComponent('messenger-chat-videochat').removeAll();
	_box.getComponent('messenger-chat-videochat').add(flash);
	_box.getComponent('messenger-chat-videochat').add(options);

	_box.doLayout();
    
    },
    showVideoChat: function(_box){
	if(Tine.Messenger.VideoChat.hidden){
	    
	    _box.setWidth(Tine.Messenger.VideoChat.originalChatWidth + Tine.Messenger.VideoChat.flashVideoWidth);
	    
//	    Tine.Messenger.VideoChat.originalChatHeight = _box.getHeight();
//	    _box.setHeight(Tine.Messenger.VideoChat.flashVideoHeight + 200);
	    
	    
	    _box.getComponent('messenger-chat-videochat').getEl().setWidth(Tine.Messenger.VideoChat.flashVideoWidth);
	    
	    Tine.Messenger.VideoChat.setIconDisabled(_box, Tine.Messenger.VideoChat.jid, true); // disable
	    
	    _box.doLayout();
	    
	    

	    Tine.Messenger.VideoChat.hidden = false;
	}
	
    },
   
    hideVideoChat : function(_box){
	if(!Tine.Messenger.VideoChat.hidden){
	    _box.setWidth(Tine.Messenger.VideoChat.originalChatWidth);
	    //_box.setHeight(Tine.Messenger.VideoChat.originalChatHeight);
	    
	    _box.getComponent('messenger-chat-videochat').getEl().setWidth(0);
	    
	    _box.doLayout();

	    Tine.Messenger.VideoChat.hidden = true;
	    
	    Tine.Messenger.VideoChat.setIconDisabled(_box, Tine.Messenger.VideoChat.jid);
	}
	
    },
    
     
    getFlashMovie: function (movieId){
	if (navigator.appName.indexOf("Microsoft") != -1) {
	    return window[movieId];
	} else {
	    return document[movieId];
	}
    },
    getChatWindow: function(jid){
	var chat_id = Tine.Messenger.ChatHandler.formatChatId(jid);
	var chat = Ext.getCmp(chat_id) ? Ext.getCmp(chat_id) : null;
	return chat;
    },
    
    setIconDisabled: function(chat, jid, unavailable){
	unavailable = typeof unavailable !== 'undefined' ? unavailable : Tine.Messenger.RosterHandler.isContactUnavailable(jid);
	var disabled = !Tine.Messenger.VideoChat.enabled || unavailable || !Tine.Messenger.VideoChat.hidden;
	
	
	if(chat !== null && chat.getTopToolbar !== undefined){
	    chat.getTopToolbar().getComponent('messenger-chat-video').setDisabled(disabled);
	}
    },
    isFlashEnabled: function()
    {
	var hasFlash = false;
	try
	{
	    var fo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
	    if(fo) hasFlash = true;
	}
	catch(e)
	{
	    try { 
                var fo = (navigator.mimeTypes && navigator.mimeTypes['application/x-shockwave-flash']) ? 
                    navigator.mimeTypes['application/x-shockwave-flash'].enabledPlugin : 0; 
                if(fo) hasFlash = true; 
            }
            catch(e2){ 
                if(navigator.mimeTypes ['application/x-shockwave-flash'] != undefined) hasFlash = true; 
            }
	}
	return hasFlash;
        
        
        
    },
    
    /**
     * Start the component to check flash capabilities
     */
    checkCameraAndMicrophone: function(){
        if(Tine.Messenger.VideoChat.isFlashEnabled()){
            var checkFlash = new Ext.FlashComponent({
                    renderTo: 'ClientDialog',
                    url: "Messenger/flash/ExpressoVideoCapabilities.swf",
                    flashVars:{
                        useExternalInterface: "true", 
                        extNamespace : "Tine.Messenger.VideoChat"
                    },
                    id: 'videoChatCheck', 
                    allowScriptAccess:"always",
                    flashVersion: "10.0.0",
                    swfWidth: 1,
                    swfHeight: 1

                });
        }
        
    },
    /**
     * This function is invoked when flash component to check capabilities is loaded and ready
     */
    setCaps: function(result) {
        
        try {
            var movie = Tine.Messenger.VideoChat.getFlashMovie("videoChatCheck"); 
            Tine.Messenger.VideoChat.isCameraAvailable = movie.getCameraCap();
            Tine.Messenger.VideoChat.isMicrophoneAvailable = movie.getMicrophoneCap();
            
            if(Ext.get('videoChatCheck')){
                Ext.get('videoChatCheck').remove();
            }
            
        }
        catch(e){
            
        }
        
    }
    
    
};