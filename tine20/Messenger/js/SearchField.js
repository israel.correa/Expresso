Ext.ns('Tine.Messenger');

Tine.Messenger.ComboSearch = Ext.extend(Ext.form.ComboBox, {
    
    translation: null,
    jidField: null,
    gridStore: null,

    initComponent : function() {
        this.triggerClass = 'x-form-search-trigger';
        Tine.Messenger.ComboSearch.superclass.initComponent.call(this);
    },
    
    disableSearch: function(doDisable) {
        this.setDisabled(doDisable);
        this.getJidField().setDisabled(doDisable);
    },
    
    onTriggerClick : function() {
        this.disableSearch(true);
        this.getJidField().setValue('');
        Ext.Ajax.request({
            params: {
                method: 'Messenger.searchContacts',
                name:   this.getRawValue(),
                start:  0,
                limit:  10
            },
            scope: this,
            success: function(result, request) {
                var response = JSON.parse(result.responseText);
                if (this.gridStore) {
                    this.gridStore.loadData(response);
                    this.disableSearch(false);
                }
            },
            failure: function (err, details) {
                console.log(err);
                console.log(details);
                this.disableSearch(false);
            }
        });
    },
    
    getJidField: function() {
        if (this.jidField == null || this.jidField == 'undefined')
            this.jidField = Ext.getCmp('messenger-contact-add-jid');
        return this.jidField;
    },
    
    setData: function(account) {
        this.setValue(account['full_name']);
        this.setJidValue(account);
    },

    setJidValue: function(account) {
        var jid;
        if (Tine.Messenger.registry.get('format') == 'email') {
            jid = account['email'];
            jid = jid.slice(0, jid.indexOf('@'));
            jid = jid + '@' + Tine.Messenger.registry.get('domain');
            this.getJidField().setValue(jid);
        } else if (Tine.Messenger.registry.get('format') == 'login') {
            jid = account['login_name'];
            jid = jid + '@' + Tine.Messenger.registry.get('domain');
            this.getJidField().setValue(jid);
        } else {
            this.disableSearch(true);
            Ext.Ajax.request({
                params: {
                    method: 'Messenger.getCustomNameFromID',
                    accountID: account['id']
                },
                scope: this,
                success: function(result, request) {
                    var response = JSON.parse(result.responseText);
                    if (response.totalcount > 0) {
                        jid = response.results[0]['value'];
                    } else {
                        jid = '';
                    }
                    jid = jid + '@' + Tine.Messenger.registry.get('domain');
                    this.getJidField().setValue(jid);
                    this.disableSearch(false);
                },
                failure: function (err, details) {
                    console.log(err);
                    console.log(details);
                    this.disableSearch(false);
                }
            });
        }
    }
    
});

Ext.reg('imcombosearch', Tine.Messenger.ComboSearch);
