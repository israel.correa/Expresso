msgid ""
msgstr ""

#: Messenger/Preference.php:63
msgid "Show Notifications"
msgstr ""

#: Messenger/Preference.php:64
msgid "Show or don't show Messenger notifications."
msgstr ""

#: Messenger/Preference.php:67
msgid "Chat History"
msgstr ""

#: Messenger/Preference.php:68
msgid "How to get chat history."
msgstr ""

#: Messenger/Preference.php:71
msgid "Custon name"
msgstr ""

#: Messenger/Preference.php:72
msgid "If you have custom Messenger, what's your name (login)."
msgstr ""

#: Messenger/Preference.php:75
msgid "Messenger Start"
msgstr ""

#: Messenger/Preference.php:76
msgid "How Messenger should start at Expresso loading."
msgstr ""

#: Messenger/Preference.php:79
msgid "Offline Contacts"
msgstr ""

#: Messenger/Preference.php:80
msgid "Show/Hide offline contacts."
msgstr ""

#: Messenger/Preference.php:113
msgid "Don't save history chat"
msgstr ""

#: Messenger/Preference.php:117
msgid "Download history chat"
msgstr ""

#: Messenger/Preference.php:133
msgid "Auto start Messenger when logged into Tine 2.0"
msgstr ""

#: Messenger/Preference.php:136
msgid "Start Messenger clicking at Messenger icon at toolbar"
msgstr ""

#: Messenger/Preference.php:140
msgid "Start Messenger pressing Login button at Messenger Window"
msgstr ""

#: Messenger/Preference.php:151
msgid "Show offline contacts"
msgstr ""

#: Messenger/Preference.php:155
msgid "Don't show offline contacts"
msgstr ""

#: Messenger/Frontend/Http.php:53
msgid "ME"
msgstr ""

#: Messenger/js/Groupie.js:250
msgid " has joined the room as a "
msgstr ""

#: Messenger/js/Groupie.js:259
msgid " is now known as "
msgstr ""

#: Messenger/js/Groupie.js:262
msgid " has left the room."
msgstr ""

#: Messenger/js/Groupie.js:362
#: Messenger/js/Groupie.js:540
#: Messenger/js/RosterTree.js:236
#: Messenger/js/RosterTree.js:311
#: Messenger/js/RosterTree.js:515
msgid "Status"
msgstr ""

#: Messenger/js/Groupie.js:363
#: Messenger/js/Groupie.js:541
msgid "Role"
msgstr ""

#: Messenger/js/Priority.js:7
msgid "Priority settings"
msgstr ""

#: Messenger/js/Priority.js:16
msgid "Priority"
msgstr ""

#: Messenger/js/Priority.js:35
msgid "Negative values do not receive messages"
msgstr ""

#: Messenger/js/Priority.js:38
msgid "Pidgin priority"
msgstr ""

#: Messenger/js/Priority.js:41
msgid "PSI priority"
msgstr ""

#: Messenger/js/Priority.js:44
msgid "Gaijm priority"
msgstr ""

#: Messenger/js/Priority.js:54
msgid "The highest the priority more likely this client to receive the message"
msgstr ""

#: Messenger/js/Priority.js:58
msgid "Examples: PSI priority = 5. Pidgin priority = 1"
msgstr ""

#: Messenger/js/Priority.js:64
msgid "Change"
msgstr ""

#: Messenger/js/Priority.js:76
#: Messenger/js/ChatHandler.js:621
#: Messenger/js/ConfigWindow.js:92
msgid "Cancel"
msgstr ""

#: Messenger/js/LogHandler.js:81
#: Messenger/js/ChatHandler.js:83
msgid "is unavailable"
msgstr ""

#: Messenger/js/LogHandler.js:90
msgid "Status text"
msgstr ""

#: Messenger/js/LogHandler.js:92
msgid "is away"
msgstr ""

#: Messenger/js/LogHandler.js:95
msgid "is busy"
msgstr ""

#: Messenger/js/LogHandler.js:98
msgid "auto status (idle)"
msgstr ""

#: Messenger/js/LogHandler.js:104
msgid "is on-line"
msgstr ""

#: Messenger/js/LogHandler.js:127
msgid "The intended recipient is temporarily unavailable."
msgstr ""

#: Messenger/js/LogHandler.js:130
msgid "The remote server does not exist or could not be reached."
msgstr ""

#: Messenger/js/LogHandler.js:133
msgid "Communication with the remote server has been interrupted."
msgstr ""

#: Messenger/js/LogHandler.js:139
#: Messenger/js/LogHandler.js:245
msgid "SERVER ERROR"
msgstr ""

#: Messenger/js/LogHandler.js:158
msgid "Accept your subscription"
msgstr ""

#: Messenger/js/LogHandler.js:163
msgid "Allow"
msgstr ""

#: Messenger/js/LogHandler.js:164
msgid "Deny"
msgstr ""

#: Messenger/js/LogHandler.js:166
msgid "Subscription Approval"
msgstr ""

#: Messenger/js/LogHandler.js:167
msgid "wants to subscribe you."
msgstr ""

#: Messenger/js/LogHandler.js:186
msgid "Denied/Removed your subscription"
msgstr ""

#: Messenger/js/LogHandler.js:187
msgid "Not authorized!"
msgstr ""

#: Messenger/js/LogHandler.js:190
msgid "Your subscription status changed to: "
msgstr ""

#: Messenger/js/LogHandler.js:217
msgid "Error sending: "
msgstr ""

#: Messenger/js/LogHandler.js:217
#: Messenger/js/Messenger.js:148
#: Messenger/js/Messenger.js:298
#: Messenger/js/Messenger.js:379
#: Messenger/js/Messenger.js:397
#: Messenger/js/FileTransfer.js:32
msgid "Error"
msgstr ""

#: Messenger/js/LogHandler.js:219
#: Messenger/js/LogHandler.js:246
msgid "Error number "
msgstr ""

#: Messenger/js/LogHandler.js:231
msgid "Cancel 404: The item was not found."
msgstr ""

#: Messenger/js/LogHandler.js:234
msgid "Cancel 501: The feature was not implemented."
msgstr ""

#: Messenger/js/LogHandler.js:237
msgid "Wait 500: Internal server error."
msgstr ""

#: Messenger/js/LogHandler.js:240
msgid "Cancel 503: Service unavailable."
msgstr ""

#: Messenger/js/LogHandler.js:258
#: Messenger/js/ChatHandler.js:83
#: Messenger/js/ChatHandler.js:84
#: Messenger/js/VideoChat.js:173
#: Messenger/js/VideoChat.js:194
#: Messenger/js/VideoChat.js:214
#: Messenger/js/VideoChat.js:260
msgid "Info"
msgstr ""

#: Messenger/js/ChatHandler.js:18
msgid "Group chat in room"
msgstr ""

#: Messenger/js/ChatHandler.js:20
msgid "Chat with"
msgstr ""

#: Messenger/js/ChatHandler.js:84
msgid "Your messages will be sent offline"
msgstr ""

#: Messenger/js/ChatHandler.js:129
msgid "You chose to download the every chat history"
msgstr ""

#: Messenger/js/ChatHandler.js:130
msgid "Do you want to download this chat"
msgstr ""

#: Messenger/js/ChatHandler.js:150
#: Messenger/js/FileTransfer.js:15
#: Messenger/js/FileTransfer.js:39
#: Messenger/js/FileTransfer.js:127
#: Messenger/js/FileTransfer.js:182
#: Messenger/js/FileTransfer.js:249
msgid "File Transfer"
msgstr ""

#: Messenger/js/ChatHandler.js:151
msgid "Chat has no content"
msgstr ""

#: Messenger/js/ChatHandler.js:158
#: Messenger/js/FileTransfer.js:152
msgid "File Transfer Error"
msgstr ""

#: Messenger/js/ChatHandler.js:159
msgid "Error creating chat history file"
msgstr ""

#: Messenger/js/ChatHandler.js:169
#: Messenger/js/FileTransfer.js:87
#: Messenger/js/FileTransfer.js:94
msgid "Temporary files not deleted"
msgstr ""

#: Messenger/js/ChatHandler.js:287
msgid " is typing..."
msgstr ""

#: Messenger/js/ChatHandler.js:289
msgid " stopped typing!"
msgstr ""

#: Messenger/js/ChatHandler.js:542
msgid "Group Message"
msgstr ""

#: Messenger/js/ChatHandler.js:552
#: Messenger/js/ChatHandler.js:578
msgid "Choose a Emoticon"
msgstr ""

#: Messenger/js/ChatHandler.js:607
msgid "Send"
msgstr ""

#: Messenger/js/ChatHandler.js:635
msgid "Send message to:"
msgstr ""

#: Messenger/js/Window.js:18
msgid "Group created successfully"
msgstr ""

#: Messenger/js/Window.js:21
#: Messenger/js/Layout.js:43
#: Messenger/js/Layout.js:440
msgid "Add Group"
msgstr ""

#: Messenger/js/Window.js:22
msgid "The group already exists"
msgstr ""

#: Messenger/js/Window.js:54
#: Messenger/js/Window.js:59
msgid "Added successfuly"
msgstr ""

#: Messenger/js/Window.js:61
msgid "Add Buddy"
msgstr ""

#: Messenger/js/Window.js:62
msgid "Buddy not added"
msgstr ""

#: Messenger/js/Messenger.js:45
msgid "Available"
msgstr ""

#: Messenger/js/Messenger.js:46
#: Messenger/js/Layout.js:187
msgid "Unavailable"
msgstr ""

#: Messenger/js/Messenger.js:47
#: Messenger/js/Layout.js:185
msgid "Away"
msgstr ""

#: Messenger/js/Messenger.js:48
msgid "Auto Status (idle)"
msgstr ""

#: Messenger/js/Messenger.js:49
#: Messenger/js/Layout.js:186
msgid "Do Not Disturb"
msgstr ""

#: Messenger/js/Messenger.js:58
msgid "none"
msgstr ""

#: Messenger/js/Messenger.js:59
msgid "from"
msgstr ""

#: Messenger/js/Messenger.js:60
msgid "both"
msgstr ""

#: Messenger/js/Messenger.js:61
msgid "to"
msgstr ""

#: Messenger/js/Messenger.js:62
msgid "waiting"
msgstr ""

#: Messenger/js/Messenger.js:63
msgid "subscribe"
msgstr ""

#: Messenger/js/Messenger.js:64
msgid "subscribed"
msgstr ""

#: Messenger/js/Messenger.js:65
msgid "unsubscribe"
msgstr ""

#: Messenger/js/Messenger.js:66
msgid "unsubscribed"
msgstr ""

#: Messenger/js/Messenger.js:135
msgid "Access to server is forbidden"
msgstr ""

#: Messenger/js/Messenger.js:136
msgid "Server does not exist"
msgstr ""

#: Messenger/js/Messenger.js:137
msgid "Server error"
msgstr ""

#: Messenger/js/Messenger.js:138
msgid "Server does not support the method"
msgstr ""

#: Messenger/js/Messenger.js:139
msgid "Server received invalid response from proxy"
msgstr ""

#: Messenger/js/Messenger.js:140
msgid "Server is unavailable"
msgstr ""

#: Messenger/js/Messenger.js:161
msgid "Messenger"
msgid_plural "Messengers"
msgstr[0] ""
msgstr[1] ""

#: Messenger/js/Messenger.js:176
msgid "IM Message"
msgstr ""

#: Messenger/js/Messenger.js:289
msgid "Connecting"
msgstr ""

#: Messenger/js/Messenger.js:299
msgid "Can't connect to server"
msgstr ""

#: Messenger/js/Messenger.js:360
msgid "Leave page"
msgstr ""

#: Messenger/js/Messenger.js:365
msgid "Close window"
msgstr ""

#: Messenger/js/Messenger.js:380
msgid "Authentication failed"
msgstr ""

#: Messenger/js/Messenger.js:398
msgid "Unknown error"
msgstr ""

#: Messenger/js/Messenger.js:485
#: Messenger/js/Messenger.js:502
#: Messenger/js/Layout.js:54
msgid "Hide offline contacts"
msgstr ""

#: Messenger/js/Messenger.js:524
#: Messenger/js/Layout.js:66
msgid "Collapse groups"
msgstr ""

#: Messenger/js/Messenger.js:529
msgid "Expand groups"
msgstr ""

#: Messenger/js/HtmlEditor.js:29
#: Messenger/js/HtmlEditor.js:37
msgid "Send Message"
msgstr ""

#: Messenger/js/HtmlEditor.js:29
msgid "Sends the formated message below"
msgstr ""

#: Messenger/js/Layout.js:8
msgid "Expresso Messenger"
msgstr ""

#: Messenger/js/Layout.js:34
#: Messenger/js/Layout.js:364
msgid "Add Contact"
msgstr ""

#: Messenger/js/Layout.js:77
msgid "Priority setting"
msgstr ""

#: Messenger/js/Layout.js:424
msgid "User"
msgstr ""

#: Messenger/js/Layout.js:450
msgid "no results"
msgstr ""

#: Messenger/js/Layout.js:96
#: Messenger/js/ConfigWindow.js:75
msgid "Login"
msgstr ""

#: Messenger/js/Layout.js:153
msgid "Type your Status"
msgstr ""

#: Messenger/js/Layout.js:154
msgid "press ENTER after"
msgstr ""

#: Messenger/js/Layout.js:184
msgid "Online"
msgstr ""

#: Messenger/js/Layout.js:382
msgid "JID"
msgstr ""

#: Messenger/js/Layout.js:389
#: Messenger/js/Layout.js:448
msgid "Name"
msgstr ""

#: Messenger/js/Layout.js:394
msgid "Group"
msgstr ""

#: Messenger/js/Layout.js:400
msgid "Select a group"
msgstr ""

#: Messenger/js/Layout.js:410
#: Messenger/js/Layout.js:453
msgid "Add"
msgstr ""

#: Messenger/js/Layout.js:486
msgid "Join Groupchat"
msgstr ""

#: Messenger/js/Layout.js:495
msgid "Identity"
msgstr ""

#: Messenger/js/Layout.js:501
msgid "Host"
msgstr ""

#: Messenger/js/Layout.js:506
msgid "Room"
msgstr ""

#: Messenger/js/Layout.js:511
msgid "Nickname"
msgstr ""

#: Messenger/js/Layout.js:517
msgid "Password"
msgstr ""

#: Messenger/js/Layout.js:521
msgid "Join"
msgstr ""

#: Messenger/js/FileTransfer.js:16
msgid "You must choose a resource"
msgstr ""

#: Messenger/js/FileTransfer.js:25
#: Messenger/js/FileTransfer.js:54
msgid "Sending file"
msgstr ""

#: Messenger/js/FileTransfer.js:29
msgid "Maximum file size: 200MB"
msgstr ""

#: Messenger/js/FileTransfer.js:92
msgid "Temporary files deleted"
msgstr ""

#: Messenger/js/FileTransfer.js:111
#: Messenger/js/FileTransfer.js:118
#: Messenger/js/FileTransfer.js:128
msgid "File sent"
msgstr ""

#: Messenger/js/FileTransfer.js:141
msgid "Error sending file"
msgstr ""

#: Messenger/js/FileTransfer.js:153
msgid "Error uploading file"
msgstr ""

#: Messenger/js/FileTransfer.js:186
msgid "wants to send you a file:"
msgstr ""

#: Messenger/js/FileTransfer.js:191
msgid "Do you allow"
msgstr ""

#: Messenger/js/FileTransfer.js:195
#: Messenger/js/RosterTree.js:94
#: Messenger/js/RosterTree.js:197
msgid "Yes"
msgstr ""

#: Messenger/js/FileTransfer.js:197
msgid "File received"
msgstr ""

#: Messenger/js/FileTransfer.js:207
#: Messenger/js/RosterTree.js:95
#: Messenger/js/RosterTree.js:198
msgid "No"
msgstr ""

#: Messenger/js/FileTransfer.js:209
msgid "File refused"
msgstr ""

#: Messenger/js/FileTransfer.js:257
msgid " has more than one resource. Choose one!"
msgstr ""

#: Messenger/js/RosterTree.js:5
msgid "(no group)"
msgstr ""

#: Messenger/js/RosterTree.js:18
#: Messenger/js/RosterTree.js:152
msgid "Rename"
msgstr ""

#: Messenger/js/RosterTree.js:22
#: Messenger/js/RosterTree.js:156
msgid "Remove"
msgstr ""

#: Messenger/js/RosterTree.js:29
msgid "Subscribe"
msgstr ""

#: Messenger/js/RosterTree.js:33
msgid "Move to"
msgstr ""

#: Messenger/js/RosterTree.js:44
msgid "Send file"
msgstr ""

#: Messenger/js/RosterTree.js:55
msgid "Show contact history"
msgstr ""

#: Messenger/js/RosterTree.js:58
msgid "User unavailable!"
msgstr ""

#: Messenger/js/RosterTree.js:77
#: Messenger/js/RosterTree.js:174
msgid "A name is required"
msgstr ""

#: Messenger/js/RosterTree.js:97
msgid "Delete Contact"
msgstr ""

#: Messenger/js/RosterTree.js:98
#: Messenger/js/RosterTree.js:201
msgid "Are you sure you want to delete "
msgstr ""

#: Messenger/js/RosterTree.js:133
msgid "Empty"
msgstr ""

#: Messenger/js/RosterTree.js:160
msgid "Send group message"
msgstr ""

#: Messenger/js/RosterTree.js:185
msgid "Rename Group"
msgstr ""

#: Messenger/js/RosterTree.js:186
msgid "The group already exists!"
msgstr ""

#: Messenger/js/RosterTree.js:200
msgid "Delete Group"
msgstr ""

#: Messenger/js/RosterTree.js:238
#: Messenger/js/RosterTree.js:312
#: Messenger/js/RosterTree.js:517
msgid "Subscription"
msgstr ""

#: Messenger/js/ConfigWindow.js:8
msgid "Preferences"
msgstr ""

#: Messenger/js/ConfigWindow.js:22
msgid "Notifications"
msgstr ""

#: Messenger/js/ConfigWindow.js:28
msgid "Show notifications"
msgstr "Show Notifications"

#: Messenger/js/ConfigWindow.js:39
msgid "History"
msgstr ""

#: Messenger/js/ConfigWindow.js:59
msgid "Send history chat to e-mail"
msgstr ""

#: Messenger/js/ConfigWindow.js:69
msgid "Custom Name"
msgstr ""

#: Messenger/js/ConfigWindow.js:88
msgid "Save"
msgstr ""

#: Messenger/js/VideoChat.js:132
msgid "{0} is inviting you to a video chat. Do you Accept?"
msgstr ""

#: Messenger/js/VideoChat.js:170
msgid "{0} rejected your video chat call"
msgstr ""

#: Messenger/js/VideoChat.js:191
msgid "{0} is busy for video chat"
msgstr ""

#: Messenger/js/VideoChat.js:266
msgid "{0} is incapable to start a video call"
msgstr ""

#: Messenger/js/VideoChat.js:211
msgid "One video chat call missed from {0}"
msgstr ""

#: Messenger/js/VideoChat.js:259
msgid "You are already in a video chat"
msgstr ""

#: Messenger/js/VideoChat.js:408
msgid "End"
msgstr ""

#: Messenger/js/VideoChat.js:409
msgid "End video chat"
msgstr ""

#: Messenger/js/RosterHandler.js:56
#: Messenger/js/RosterHandler.js:371
msgid "was successfully removed"
msgstr ""

#: Messenger/js/RosterHandler.js:312
#: Messenger/js/RosterHandler.js:369
msgid "Successful"
msgstr ""

#: Messenger/js/RosterHandler.js:313
#: Messenger/js/RosterHandler.js:370
#: Messenger/js/RosterHandler.js:375
msgid "The group"
msgstr ""

#: Messenger/js/RosterHandler.js:314
msgid "was successfully renamed to"
msgstr ""

#: Messenger/js/RosterHandler.js:319
msgid " already exist. It was not renamed."
msgstr ""

#: Messenger/js/RosterHandler.js:376
msgid "was not removed"
msgstr ""

#: Messenger/js/History.js:7
msgid "History Chat with"
msgstr ""

#: Messenger/js/History.js:16
msgid "Download"
msgstr ""

#: Messenger/js/Chat.js:40
msgid "Start video chat"
msgstr ""

#: Messenger/js/Chat.js:111
msgid "Contact Chat History"
msgstr ""

#: Messenger/js/VideoChat.js:464
msgid "Flash Player is not installed"
msgstr ""