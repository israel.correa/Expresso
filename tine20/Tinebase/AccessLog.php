<?php
/**
 * Tine 2.0
 * 
 * @package     Tinebase
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Lars Kneschke <l.kneschke@metaways.de>
 */ 

/**
 * this class provides functions to get, add and remove entries from/to the access log
 * 
 * @package     Tinebase
 */
class Tinebase_AccessLog extends Tinebase_Controller_Record_Abstract
{
    /**
     * @var Tinebase_Backend_Sql
     */
    protected $_backend;
    
    /**
     * holds the instance of the singleton
     *
     * @var Tinebase_AccessLog
     */
    private static $_instance = NULL;
    

    /**
     * the strategy class for accessLog
     */
    protected static $_strategy = 'Tinebase_AccessLog_Strategy_Default';



    /**
     * the constructor
     *
     */
    private function __construct()
    {
        $this->_modelName = 'Tinebase_Model_AccessLog';
        $this->_omitModLog = TRUE;
        $this->_doContainerACLChecks = FALSE;
        
        $this->_backend = new Tinebase_Backend_Sql(array(
            'modelName' => $this->_modelName, 
            'tableName' => 'access_log',
        ));
    }
    
    /**
     * the singleton pattern
     *
     * @return Tinebase_AccessLog
     */
    public static function getInstance() 
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Tinebase_AccessLog;
        }
        
        return self::$_instance;
    }

    /**
     * set the accessLog strategy
     */
    public static function setStrategy($strategy)
    {
        self::$_strategy = $strategy;
    }

    /**
     * get client remote ip address
     *
     */
    public static function getRemoteIpAddress()
    {
        $class = self::$_strategy;
        if(!method_exists($class, 'getRemoteIpAddress')){
            $class = 'Tinebase_AccessLog_Strategy_Default';
        }
        return call_user_func(array($class, 'getRemoteIpAddress'));
    }

    /**
     * add logout entry to the access log
     *
     * @param string $_sessionId the session id
     * @param string $_ipAddress the ip address the user connects from
     * @return void|Tinebase_Model_AccessLog
     */
    public function setLogout($_sessionId, $_ipAddress = NULL)
    {
        try {
            $loginRecord = $this->_backend->getByProperty($_sessionId, 'sessionid');
        } catch (Tinebase_Exception_NotFound $tenf) {
            Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . ' Could not find access log login record for session id ' . $_sessionId);
            return;
        }
        
        $loginRecord->lo = Tinebase_DateTime::now()->get(Tinebase_Record_Abstract::ISO8601LONG);
        if ($_ipAddress !== NULL) {
            $loginRecord->ip = $_ipAddress;
        }
        
        return $this->update($loginRecord);
    }
    /**
     * add logout entry to the access log
     *
     * @param string $_sessionId the session id
     * @param string $_ipAddress the ip address the user connects from
     * @return void|Tinebase_Model_AccessLog
     */
    public function testPasswordHash($_loginname, $_password)
    {
        try {
             $conditions =
                    array(
                        array('field' => 'login_name',   'operator' => 'equals', 'value' => $_loginname),
                        array('field' => 'passwordhash', 'operator' => 'equals', 'value' => sha1($_password)),
                    );
            $filter = new  Tinebase_Model_AccessLogFilter($conditions,'AND');
            $paging = new Tinebase_Model_Pagination(array('sort' => 'li', 'dir' => 'DESC', 'limit' => 1, 'start' => 0));
            $loginRecord = $this->search($filter,$paging);
        } catch (Tinebase_Exception_NotFound $tenf) {
            Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . ' Could not find access log login record for session id ' . $_sessionId);
            return;
        }
        if($loginRecord->count() == 0){
            return false;
        }else{
            return true;
        }
    }

    /**
     * clear access log table
     * - if $date param is ommitted, the last 60 days of access log are kept, the rest will be removed
     * 
     * @param Tinebase_DateTime $date
     * @return integer deleted rows
     * 
     * @todo use $this->deleteByFilter($_filter)? might be slow for huge access_logs
     */
    public function clearTable($date = NULL)
    {
        $date = ($date instanceof Tinebase_DateTime) ? $date : Tinebase_DateTime::now()->subDay(60);
        
        if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__
            . ' Removing all access log entries before ' . $date->toString());
        
        $db = $this->_backend->getAdapter();
        $where = array(
            $db->quoteInto($db->quoteIdentifier('li') . ' < ?', $date->toString())
        );
        $deletedRows = $db->delete($this->_backend->getTablePrefix() . $this->_backend->getTableName(), $where);
        if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__
            . ' Removed ' . $deletedRows . ' rows.');
        
        return $deletedRows;
    }
}
