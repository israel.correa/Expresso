/*
 * Tine 2.0
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Fernando Alberto Reuter Wendt <fernando-alberto.wendt@serpro.gov.br>
 * @copyright   Copyright (c) 2013 SERPRO - Serviço Federal de Processamento de Dados (http://www.serpro.gov.br)
 *
 */
 
 Ext.ns('Tine', 'Tine.Tinebase');
 
 /**
  * @namespace  Tine.Tinebase
  * @class      Tine.Tinebase.PasswordChangeDialog
  * @extends    Ext.Window
  */
Tine.Tinebase.ExpiredPasswordChangeDialog = Ext.extend(Ext.Window, {
    
    id: 'changeExpiredPassword_window',
    closeAction: 'close',
    modal: true,
    width: 350,
    height: 230,
    minWidth: 350,
    minHeight: 230,
    layout: 'fit',
    plain: true,
    title: null,

    initComponent: function() {
        if (!Tine.loginPanel){
            Ext.Msg.alert(_('Warning'), _('This action is only available in the login screen!'));
            return;
        }      
        
        this.currentAccount = Tine.loginPanel.getLoginPanel().getForm().findField("username").getValue();
        this.currentPassword = Tine.loginPanel.getLoginPanel().getForm().findField("password").getValue();
        
        if ((Ext.isEmpty(this.currentAccount)) || (Ext.isEmpty(this.currentPassword))){
            Ext.Msg.alert(_('Warning'), _('Check the username and/or password entered in the login screen!'));
            return;            
        }

        this.title = (this.title !== null) ? this.title : String.format(_('Change Password For "{0}"'), this.currentAccount);
        
        this.items = new Ext.FormPanel({
            bodyStyle: 'padding:5px;',
            buttonAlign: 'right',
            labelAlign: 'top',
            anchor:'100%',
            id: 'changeExpiredPasswordPanel',
            defaults: {
                xtype: 'textfield',
                inputType: 'password',
                anchor: '100%',
                allowBlank: false
            },
            items: [{
                id: 'oldPassword',
                fieldLabel: _('Old Password'), 
                value: this.currentPassword,
                name:'oldPassword'
            },{
                id: 'newPassword',
                fieldLabel: _('New Password'), 
                name:'newPassword'
            },{
                id: 'newPasswordSecondTime',
                fieldLabel: _('Repeat new Password'), 
                name:'newPasswordSecondTime'
            },{
                id: 'accountId',
                name:'accountId',
                inputType: 'hidden',
                value: this.currentAccount
            }],
            buttons: [{
                text: _('Cancel'),
                iconCls: 'action_cancel',
                handler: function() {
                    Ext.getCmp('changeExpiredPassword_window').close();
                }
            }, {
                text: _('Ok'),
                iconCls: 'action_saveAndClose',
                handler: function() {
                    var form = Ext.getCmp('changeExpiredPasswordPanel').getForm();
                    var values;
                    if (form.isValid()) {
                        values = form.getValues();
                        if (values.newPassword == values.newPasswordSecondTime) {
                            Ext.MessageBox.wait(_('Updating your new password...'), _('Please wait'));
                            
                            Ext.Ajax.request({
                                waitTitle: _('Please Wait!'),
                                waitMsg: _('changing password...'),
                                params: {
                                    method: 'Tinebase.changeExpiredPassword',
                                    userName: values.accountId,
                                    oldPassword: values.oldPassword,
                                    newPassword: values.newPassword
                                },
                                success: function(_result, _request){
                                    var response = Ext.util.JSON.decode(_result.responseText);
                                    if (response.success) {
                                        Ext.getCmp('changeExpiredPassword_window').close();                                       
                                        
                                        
                                        Ext.MessageBox.show({
                                            title: _('Success'),
                                            msg: _(response.successMessage),
                                            buttons: Ext.MessageBox.OK,
                                            icon: Ext.MessageBox.INFO,
                                            fn: function(){
                                                    if(Tine.loginPanel){
                                                        Tine.loginPanel.getLoginPanel().getForm().findField('password').setValue("");
                                                    }
                                            }
                                        });
                                    } else {
                                        var errorMessage = response.errorMessage;
                                        var title  = errorMessage.substr(0, (errorMessage.indexOf(':')+1));
                                        var errorFull = errorMessage.substr((errorMessage.indexOf(':') + 1));
                                        var errorTitle = errorFull.substr(0, (errorFull.indexOf(':') + 1));
                                        var errors = errorFull.substr((errorFull.indexOf(':') + 1));
                                        Ext.MessageBox.show({
                                            title: title,
                                            msg: errorTitle + '<br /><br />' + errors.replace(/, /g, '<br />'),
                                            buttons: Ext.MessageBox.OK,
                                            icon: Ext.MessageBox.ERROR
                                        });
                                    }
                                },
                                failure: function(response, opts) {
                                        Ext.MessageBox.show({
                                            title: _('Failure'),
                                            msg: _('Something wrong happened while setting your new password. You can try again, or contact your system administrator'),
                                            buttons: Ext.MessageBox.OK,
                                            icon: Ext.MessageBox.INFO
                                        });
                                }
                            });
                        } else {
                            Ext.MessageBox.show({
                                title: _('Failure'),
                                msg: _('The new passwords mismatch, please correct them.'),
                                buttons: Ext.MessageBox.OK,
                                icon: Ext.MessageBox.ERROR 
                            });
                        }
                    }
                }                    
            }]
        });
        
        Tine.Tinebase.ExpiredPasswordChangeDialog.superclass.initComponent.call(this);
    }
});