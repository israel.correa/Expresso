<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  PluginManager
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 * @copyright   Copyright (c) 2014 Serpro (http://www.serpro.gov.br)
 *
 */
/**
 * class to store plugin configuration
 *
 * @package     Tinebase
 * @subpackage  PluginManager
 */

final class Tinebase_PluginManager
{
    /**
     *
     * @var array
     */
    private static $_pluginConfigItems = array();

    /**
     *
     * @var array
     */
    private static $_globalPluginConfigItems = array();

    /**
     * @param string $name
     * @param string $xtype
     * @param string $fieldLabel
     * @param mixed $value
     */
    public static function addPluginConfigItem($name, $xtype, $fieldLabel, $value = '')
    {
        $plugin = array();
        $plugin['name'] = $name;
        $plugin['xtype'] = $xtype;
        $plugin['fieldLabel'] = $fieldLabel;
        $plugin['value'] = $value;
        self::$_pluginConfigItems[] = $plugin;
    }

    /**
     * @param string $name
     * @param string $xtype
     * @param string $fieldLabel
     * @param mixed $value
     */
    public static function addGlobalPluginConfigItem($name, $xtype, $fieldLabel, $value = '')
    {
        $plugin = array();
        $plugin['name'] = $name;
        $plugin['xtype'] = $xtype;
        $plugin['fieldLabel'] = $fieldLabel;
        $plugin['value'] = $value;
        self::$_globalPluginConfigItems[] = $plugin;
    }

    /**
     *
     * @return array
     */
    public static function getPluginConfigItems()
    {
        return array('plugins' => self::$_pluginConfigItems);
    }

    /**
     *
     * @return array
     */
    public static function getGlobalPluginConfigItems()
    {
        return array('global' => array('plugins' => self::$_globalPluginConfigItems));
    }
}
