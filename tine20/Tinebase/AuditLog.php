<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  AuditLog
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Lucas Alberto Santos <lucas-alberto.santos@serpro.gov.br>
 * @copyright   Copyright (c) 2014 Serpro (http://www.serpro.gov.br)
 */

/**
 * Audit Log controll class
 *
 * @package     Tinebase
 * @subpackage  AuditLog
 */
class Tinebase_AuditLog
{
    /**
     * Log type Read
     */
    const READ = 0;

    /**
     * Log type Create e/or Update
     */
    const CREATE_UPDATE = 1;

    /**
     * Log type Delete
     */
    const DELETE = 2;

    /**
     * Array de tipos de log
     *
     * @var array
     */
    protected $_auditTypes = array();

    /**
     * holds the return of the openlog function
     *
     * @var boolean
     */
    protected $_active = null;

    /**
     * holds the instance of the singleton
     *
     * @var Audit_Log
     */
    private static $_instance = NULL;

    /**
     * the singleton pattern
     *
     * @return Tinebase_AccessLog
     */
    public static function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Tinebase_AuditLog();
        }
        return self::$_instance;
    }

    /**
     * Class constructor. Create a new logger
     *
     */
    private function __construct()
    {
        $this->_auditTypes = array(
            Tinebase_AuditLog::READ => 'READ',
            Tinebase_AuditLog::CREATE_UPDATE => 'CREATE_UPDATE',
            Tinebase_AuditLog::DELETE => 'DELETE'
        );
        // open syslog, include the process ID and also send the log to standard error, and use a user defined
        // logging mechanism-
        $_active = openlog("Expressomail_AuditLog", LOG_PID | LOG_PERROR, LOG_USER);
        $this->setActive($_active);
    }

    /**
     * Class destructor.
     *
     * @return void
     */
    public function __destruct()
    {
        closelog();
        $this->setActive(false);
    }

    /**
     * Sets the autid log Active
     *
     * @param type $active
     */
    public function setActive($active)
    {
        $this->_active = $active;
    }

    /**
     * Sets the audit log Inactive
     *
     * @return type
     */
    public function isActive()
    {
        if ((isset(Tinebase_Core::getConfig()->auditLogActive))) {
            $auditLogActive = Tinebase_Core::getConfig()->auditLogActive;
            return $this->_active && $auditLogActive;
        } else {
            return $this->_active;
        }
    }

    /**
     * Try to log the info, if succeds return the data sent. if not, return NULL
     *
     * @param string $type one of the $this->_auditTypes
     * @param string $module
     * @param string $method
     * @param mixed $parameters
     * @param string $message
     * @return mixed|array
     */
    public function logCall($type, $module, $method, $parameters, $message)
    {
        $write = $this->log($type, $module, $method, $parameters, $message, NULL);

        if(! $write) {
            return NULL;
        }

        return array($type, $module, $method, $parameters, $message);
    }

    /**
     * Ends the log
     *
     * @param array $callData
     * @param mixed $return
     * @param string $message
     * @return boolean
     */
    public function logReturn($callData, $return, $message = NULL)
    {
        if(! is_null($callData)) {
            if(! is_null($message)) {
                return $this->log($callData[0], $callData[1], $callData[2], $callData[3], $message, $return);
            } else {
                return $this->log($callData[0], $callData[1], $callData[2], $callData[3], $callData[4], $return);
            }
        }
    }

    /**
     * Log a message in audit log
     *
     * @param string $type one of the $this->_auditTypes
     * @param string $module
     * @param string $method
     * @param mixed $parameters
     * @param string $message
     * @return boolean
     *
     * @throws Zend_Log_Exception
     */
    public function log($type, $module, $method, $parameters, $message, $return = NULL)
    {
        try {
            if(! $this->isActive())
                return false;

            $type = $this->_auditTypes[$type];
            $module = strtolower($module);
            $timestamp = date('c');
            $sessionid = Tinebase_Core::get(Tinebase_Session::SESSIONID);
            $user = json_encode($this->_getUserDataToLog(Tinebase_Core::getUser()));
            $parameters = json_encode($parameters);

            $log = ';\''.$timestamp.'\';\''.$type.'\';\''.$sessionid.'\';\''.$module.'\';\''.$user.'\';\''.$method.'\';\''.
                    $parameters.'\';\''.$message.'\'';

            if(! is_null($return)) {
                $return = json_encode($return);
                $log = $log.';RETURN \''.$return.'\'';
            }

            $write = syslog(LOG_INFO, $log);

        } catch (Exception $e) {
            Tinebase_Core::getLogger()->warn(__METHOD__ . '::' . __LINE__ . ' Tinebase_AuditLog unable to write to syslog. ' .
                $e->getMessage());
        }

        if(! isset($write) || ! $write) {
            throw new Zend_Log_Exception("Tinebase_AuditLog unable to write to syslog.");
        }

        return true;
    }

    /**
     * Get the core properties of the user to write in log
     *
     * @param Tinebase_Model_FullUser $user
     * @return array of user properties to log
     */
    public function _getUserDataToLog(Tinebase_Model_FullUser $user)
    {
        return array('accountDisplayName' => $user['accountDisplayName'], 'accountLoginName' => $user['accountLoginName'],
                'accountEmailAddress' => $user['accountEmailAddress'], 'accountStatus' => $user['accountStatus'],
                'accountPrimaryGroup' => $user['accountPrimaryGroup']);
    }
}
