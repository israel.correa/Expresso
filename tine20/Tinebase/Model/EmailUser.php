<?php
/**
 * Tine 2.0
 * 
 * @package     Tinebase
 * @subpackage  LDAP
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2009 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Philipp Schuele <p.schuele@metaways.de>
 * 
 * @todo        make default quota configurable
 */

/**
 * class Tinebase_Model_EmailUser
 * 
 * - this class contains all email specific user settings like quota, forwards, ...
 * 
 * @package     Tinebase
 * @subpackage  LDAP
 */
class Tinebase_Model_EmailUser extends Tinebase_Record_Abstract 
{
   
    protected $_identifier = 'emailUID';
    
    /**
     * application the record belongs to
     *
     * @var string
     */
    protected $_application = 'Tinebase';
    
    /**
     * validators / fields
     *
     * @var array
     */
    protected $_validators = array(
        'emailUID'          => array('allowEmpty' => true),
        'emailGID'          => array('allowEmpty' => true),
        'emailMailQuota'    => array('allowEmpty' => true, 'Digits'),
        'emailMailSize'     => array('allowEmpty' => true),
        'emailMailRemoteArchiveQuota'    => array('allowEmpty' => true, 'Digits'),
        'emailMailRemoteArchiveSize'     => array('allowEmpty' => true),
        'emailSieveQuota'   => array('allowEmpty' => true, 'Digits'),
        'emailSieveSize'    => array('allowEmpty' => true),
        'emailUserId'       => array('allowEmpty' => true),
        'emailLastLogin'    => array('allowEmpty' => true),
        'emailPassword'     => array('allowEmpty' => true),
        'emailForwards'     => array('allowEmpty' => true, Zend_Filter_Input::DEFAULT_VALUE => array()),
        'emailForwardOnly'  => array('allowEmpty' => true, Zend_Filter_Input::DEFAULT_VALUE => 0),
        'emailAliases'      => array('allowEmpty' => true, Zend_Filter_Input::DEFAULT_VALUE => array()),
        'emailAddress'      => array('allowEmpty' => true),
    // dbmail username (tine username + dbmail domain)
        'emailUsername'     => array('allowEmpty' => true),
    );
    
    /**
     * datetime fields
     *
     * @var array
     */
    protected $_datetimeFields = array(
        'emailLastLogin'
    );
    
    /**
     * overwrite constructor to add more filters
     *
     * @param mixed $_data
     * @param bool $_bypassFilters
     * @param mixed $_convertDates
     * @return void
     */
    public function __construct($_data = NULL, $_bypassFilters = false, $_convertDates = true)
    {
        $this->_filters['emailForwardOnly'] = new Zend_Filter_Empty(false);
        $this->_filters['emailMailSize'] = new Zend_Filter_Empty(0);
        $this->_filters['emailMailQuota'] = new Zend_Filter_Empty(0);
        $this->_filters['emailForwards'] = new Zend_Filter_Empty(array());
        $this->_filters['emailAliases'] = new Zend_Filter_Empty(array());
        
        return parent::__construct($_data, $_bypassFilters, $_convertDates);
    }
    
    /**
     * sets the record related properties from user generated input.
     * 
     * Input-filtering and validation by Zend_Filter_Input can enabled and disabled
     *
     * @param array $_data            the new data to set
     */
    public function setFromArray(array $_data)
    {
        foreach (array('emailForwards', 'emailAliases') as $arrayField) {
            if (isset($_data[$arrayField]) && ! is_array($_data[$arrayField])) {
                $_data[$arrayField] = explode(',', preg_replace('/ /', '', $_data[$arrayField]));
            }
        }
        
        parent::setFromArray($_data);
    }
    
    /**
     * Additional validation
     *
     * @param $_throwExceptionOnInvalidData
     * @return bool
     * @throws Tinebase_Exception_Record_Validation
     * @see Tinebase_Record_Abstract::isValid()
     */
    function validateAliases($_throwExceptionOnInvalidData = false) {

        $errors = array();
        if($this->__get('emailAliases') !== NULL) {
            $smtpConfig = Tinebase_Config::getInstance()->get(Tinebase_Config::SMTP, new Tinebase_Config_Struct())->toArray();
            $obligatorydomains = explode(',', str_replace(' ', '', $smtpConfig['obligatorydomains']));
            $secondaryDomains = explode(',', str_replace(' ', '', $smtpConfig['secondarydomains']));
            $allDomains = array_merge(strlen($smtpConfig['secondarydomains']) ? $secondaryDomains : array(),
                    strlen($smtpConfig['obligatorydomains']) ? $obligatorydomains : array());
            array_push($allDomains, substr($this->__get('emailAddress'), strpos($this->__get('emailAddress'),
            '@')+1, strlen($this->__get('emailAddress'))));

            $valid = true;
            foreach ($this->__get('emailAliases') as $email) {
                $pos = strpos($email, '@');
                $domain = substr($email, $pos+1, strlen($email));
                if(! in_array($domain, $allDomains)) {
                    $valid = false;
                    //TODO fix translation
                    $message = sprintf(Tinebase_Translation::getTranslation('Tinebase')->_('The email alias (%s) has not a allowed domain.'), $email);
                    array_push($errors, array('id' => 'emailAliases', 'msg' => $message));
                    if (Tinebase_Core::isLogLevel(Zend_Log::ERR)) Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . $message);
                }
            }

            if (!$valid && $_throwExceptionOnInvalidData) {
                $e = new Tinebase_Exception_Record_Validation($message);
                Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . ":\n" . print_r($errors, true). $e);
                throw $e;
            }
        }

        return $errors;
    }

    public function validateExistentAliases(){
        $errors = array();
        $checks = Admin_Controller_User::getInstance()->checkEmail($this->__get('emailAliases'));
        if($checks['status'] == 'failure') {
            foreach ($checks['data'] as $check) {
                if(count($check['failures']) > 0) {
                    //TODO fix translation
                    $message = sprintf(Tinebase_Translation::getTranslation('Tinebase')->_('The email alias (%s) is already in system. Please choose another alias.'), $check['mail']);
                    $errors[] = $check['mail'];
                }
            }
        }
        return $errors;
    }
} 
