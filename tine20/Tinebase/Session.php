<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Session
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 * @copyright   Copyright (c) 2009-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/**
 * class for Session and Session Namespaces in Core
 *
 * @package     Tinebase
 * @subpackage  Session
 */
class Tinebase_Session extends Tinebase_Session_Abstract
{

    /**
     * Session Validator IpAddress Class
     */
    protected static $_IpAddressValidator = 'Zend_Session_Validator_IpAddress';

    /**
     * Set Session Validator IpAddress Class
     */
    public static function setIpAddressValidator($ipAddressValidator)
    {
        self::$_IpAddressValidator = $ipAddressValidator;
    }

    /**
     * Register Validator for Http User Agent
     */
    public static function registerValidatorHttpUserAgent()
    {
        Zend_Session::registerValidator(new Zend_Session_Validator_HttpUserAgent());
    }

    /**
     * Register Validator for Ip Address
     */
    public static function registerValidatorIpAddress()
    {
        $config = Tinebase_Config::getInstance()->get(Tinebase_Config::SESSIONIPVALIDATION, TRUE);
        if ($config === TRUE || (!is_bool($config) && $config->active === TRUE )) {
            $ipAddressValidator = self::$_IpAddressValidator;
            if (class_exists($ipAddressValidator)) {
                Zend_Session::registerValidator(new $ipAddressValidator());
            } else {
                Tinebase_Core::getLogger()->err(__METHOD__ . '::' . __LINE__ . ' Failed to load ValidatorIpAddress Class: ' . $ipAddressValidator . '. Loading default: Zend_Session_Validator_IpAddress');
                Zend_Session::registerValidator(new Zend_Session_Validator_IpAddress());
            }
        }
    }
}
