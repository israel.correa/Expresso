<?php
/**
 * Tine 2.0
 *
 * @package     Shard
 * @subpackage  LoginName
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 *
 */

/**
 * Implements Strategy that retuns LoginName as Shardkey
 *
 * @package     Shard
 */
class Tinebase_Shard_Shardkey_Strategy_LoginName implements Tinebase_Shard_Shardkey_Strategy_Interface
{
    /**
     * Get ShardKey from selected strategy
     *
     * @param  string $_database
     * @return string || NULL
     */
    public function getShardKey($_database)
    {
        $shardConfig = Tinebase_Shard_Manager::getShardConfig($_database);

        $user = Tinebase_Core::getUser();

        return $user->accountEmailAddress;
    }

    /**
     * Get All shardKeys associated with a virtualshard
     *
     * @param  string $_database
     * @param  string $_virtualShard
     * @return array || NULL || FALSE
     */
    public function getShardKeysByVirtualShard($_database, $_virtualShard)
    {
        $shardKeys = NULL;
        $shardConfig = Tinebase_Shard_Manager::getShardConfig($_database);

        $users = Tinebase_User::getInstance()->getFullUsers();
        $associationVirtualshardShardkey = new Tinebase_Shard_Association_Virtualshard_Shardkey_Strategy_Context();
        $result = $associationVirtualshardShardkey->setStrategy($shardConfig->associationVirtualshardShardkeyStrategy);

        if (! isset($result)) {
            throw new Tinebase_Exception_NotFound ('Not implemented association between Virtualshard and Shardkey Strategy '
                                                   . $shardConfig->associationVirtualshardShardkeyStrategy);
            return FALSE;
        }

        foreach($users as $user) {

            if (! isset($user->accountEmailAddress)) {
                continue;
            }

            $userVirtualShard = $associationVirtualshardShardkey->getVirtualShard($_database, $user->accountEmailAddress);

            if (! isset($userVirtualShard)) {
                 throw new Tinebase_Exception_NotFound ('Unable to get Virtual Shard of shardKey' . $user->accountEmailAddress . '  ');
                 return FALSE;
            }

            if ($_virtualShard == $userVirtualShard) {
                $shardKeys[] = $user->accountEmailAddress;
                Tinebase_Shard_Manager::getLogger()->info($user->accountEmailAddress);
            }
        }

        return $shardKeys;
    }

    /**
     * Get All shardKeys from selected strategy
     *
     * @param  string $_database
     * @return array || NULL
     */
    public function getAllShardKeys($_database)
    {
        $shardKeys = NULL;
        $shardConfig = Tinebase_Shard_Manager::getShardConfig($_database);

        $users = Tinebase_User::getInstance()->getFullUsers();

        foreach($users as $user) {
            if (! isset($user->accountEmailAddress)) {
                continue;
            }

            $shardKeys[] = $user->accountEmailAddress;
        }

        return $shardKeys;
    }

    /**
     * Get All Filters for Shard Key
     *
     * @param  string $_shardKey
     * @return array
     */
    public function getFilters($_shardKey) {
        $userFilters == array();

        $userId = Tinebase_User::getInstance()->getUserByEmailAddress($_shardKey)->getId();
        $userFilters['accounts']['id'] = $userId;
        $userFilters['acsync_device']['owner_id'] = $userId;

        $applications = Tinebase_Application::getInstance()->getApplications();
        foreach($applications as $application) {
            $appName = $application->__toString();
            $containers = Tinebase_Container::getInstance()->getContainerByACL($userId, $appName, 'read', FALSE, FALSE);
            foreach($containers as $container) {
                if ($container->isPersonalOf($userId)) {
                    $containerIds[] = $container->getId();
                }
            }
        }

        $userFilters['cal_events']['container_id'] = $containerIds;
        $userFilters['tasks']['container_id'] = $containerIds;

        return $userFilters;
    }
}
