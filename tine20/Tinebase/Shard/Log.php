<?php
/**
 * Tine 2.0
 *
 * @package     Shard
 * @subpackage  Shard_Log
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 *
 */

/**
 * class Tinebase_Shard_Log
 *
 * @package Shard
 * @subpackage Log
 */
class Tinebase_Shard_Log extends Zend_Log
{
    /**
     * add new log writer defined by a config object/array
     *
     * @param Tinebase_Config_Struct|Zend_Config|array $_loggerConfig
     *
     * @throws Tinebase_Exception_NotFound
     */
    public function addWriterByConfig($_loggerConfig)
    {
        $_loggerConfig = ($_loggerConfig instanceof Tinebase_Config_Struct || $_loggerConfig instanceof Zend_Config)
            ? $_loggerConfig : new Tinebase_Config_Struct($_loggerConfig);

        if (empty($_loggerConfig->filename)) {
            throw new Tinebase_Exception_NotFound('filename missing in logger config');
        }

        $filename = $_loggerConfig->filename;
        $writer = new Zend_Log_Writer_Stream($filename);

        $formatter = new Tinebase_Log_Formatter();
        $formatter->setReplacements();
        $writer->setFormatter($formatter);

        $priority = ($_loggerConfig->priority) ? (int)$_loggerConfig->priority : Zend_Log::EMERG;
        $filter = new Zend_Log_Filter_Priority($priority);
        $writer->addFilter($filter);

        // add more filters here
        if (isset($_loggerConfig->filter->user)) {
            $writer->addFilter(new Tinebase_Log_Filter_User($_loggerConfig->filter->user));
        }
        if (isset($_loggerConfig->filter->message)) {
            $writer->addFilter(new Zend_Log_Filter_Message($_loggerConfig->filter->message));
        }

        $this->addWriter($writer);
    }

    /**
     * get max log priority
     *
     * @param Tinebase_Config_Struct|Zend_Config $_loggerConfig
     * @return integer
     */
    public static function getMaxLogLevel($_loggerConfig)
    {
        $logLevel = $_loggerConfig && $_loggerConfig->priority ? (int)$_loggerConfig->priority : Zend_Log::EMERG;
        if ($_loggerConfig && $_loggerConfig->additionalWriters) {
            foreach ($_loggerConfig->additionalWriters as $writerConfig) {
                $writerConfig = ($writerConfig instanceof Tinebase_Config_Struct || $writerConfig instanceof Zend_Config)
                    ? $writerConfig : new Tinebase_Config_Struct($writerConfig);
                if ($writerConfig->priority && $writerConfig->priority > $logLevel) {
                    $logLevel = (int) $writerConfig->priority;
                }
            }
        }
        return $logLevel;
    }
}