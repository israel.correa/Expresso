<?php
/**
 * // Association Virtual Shard => Connection Config key
 *
 * @package Tinebase
 * @subpackage Shard
 * @license http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 * @copyright Copyright (c); 2015 Metaways Infosystems GmbH (http://www.metaways.de);
 *
 */

/**
 * interface Association Virtual Shard => Connection Config key
 *
 * @package Tinebase
 * @subpackage Shard
 */
interface Tinebase_Shard_Association_Virtualshard_Connectionconfigkey_Interface
{
   /**
    * the singleton pattern
    *
    * @param array $_options
    * @param string $_database
    * @return Tinebase_Shard_Association_Virtualshard_Connectionconfigkey_Interface
    */
    public static function getInstance(array $_options, $_database);

   /**
    * get Connectionconfig key associated with given Virtual Shard
    *
    * @param integer $_virtualShard
    * @return string | FALSE
    */
    public function getAssociation($_virtualShard);

   /**
    * set Virtual Shard X Connectionconfig key association
    *
    * @param integer $_virtualShard
    * @param string $_key
    * @return boolean
    */
    public function setAssociation($_virtualShard, $_key);

    /**
    * remove Virtual Shard X Connectionconfig key association
    *
    * @param integer $_virtualShard
    * @return boolean
    */
    public function removeAssociation($_virtualShard);

    /**
    * clear all associations
    *
    */
    public function clearAllAssociations();
}