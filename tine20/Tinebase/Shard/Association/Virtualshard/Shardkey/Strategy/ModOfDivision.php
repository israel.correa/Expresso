<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase_Shard_Association_Virtualshard_Shardkey_Strategy
 * @subpackage  ModOfDivision
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 *
 */

/**
 * Implements the Mod of Division strategy to associate Users and Virtual Shards
 *
 * @package     Tinebase_Shard
 */
class Tinebase_Shard_Association_Virtualshard_Shardkey_Strategy_ModOfDivision implements Tinebase_Shard_Association_Virtualshard_Shardkey_Strategy_Interface
{
    /**
     * Get VirtualShard number associated with shardKey from selected strategy
     *
     * @param  string $_database
     * @param  string $_shardKey
     * @return integer || NULL
     */
    public function getVirtualShard($_database, $_shardKey)
    {
        $shardConfig = Tinebase_Shard_Manager::getShardConfig($_database);

        if (isset($shardConfig) && isset($_shardKey)
            && isset($shardConfig->numberOfVirtualShards)
            && ($shardConfig->numberOfVirtualShards > 0)
        ) {
           return (hexdec(substr(hash('md5', $_shardKey), 0, 6)) % $shardConfig->numberOfVirtualShards);
        }

        return NULL;
    }
}