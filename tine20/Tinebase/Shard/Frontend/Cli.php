<?php
/**
 * Tine 2.0
 * @package     Shard
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 * @copyright   Copyright (c) 2015 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * Cli frontend for Shard
 *
 * This class handles cli requests for the Shard
 *
 * @package     Tinebase_Shard
 */
class Tinebase_Shard_Frontend_Cli extends Tinebase_Frontend_Cli_Abstract
{
    /**
     * the internal name of the application
     *
     * @var string
     */
    protected $_applicationName = 'Tinebase';

    /**
     * Set/Update/Delete Shard Connection Config
     *
     * @param $_opts
     * @return boolean success
     */
    public function connectionConfig($_opts)
    {
        if (! $this->_checkAdminRight()) {
            return FALSE;
        }

        $args = $this->_parseArgs($_opts, array('database', 'operation', 'values', 'outputlogfile'));

        switch ($args['operation']) {
            case 'set':
                $valuesJson = str_replace(array(";","'"),array(',','"'),$args['values']);
                $valuesArray = json_decode($valuesJson, TRUE);
                $result = Tinebase_Shard_Manager::getInstance()->setConnectionConfig($args['database'], $valuesArray, $args['outputlogfile']);
                break;
            case 'remove':
                $result = Tinebase_Shard_Manager::getInstance()->removeConnectionConfig($args['database'], $args['values'], $args['outputlogfile']);
                break;
            default:
                echo 'Syntax error: operation parameter must be set or remove.';
                $result = FALSE;
        }

        return $result;
    }

    /**
     * Reshard Virtual Shard
     *
     * @param $_opts
     * @return boolean success
     */
    public function reshardVirtualShard($_opts)
    {
        if (! $this->_checkAdminRight()) {
            return FALSE;
        }

        $args = $this->_parseArgs($_opts, array('database', 'virtualshard', 'key', 'outputlogfile'));

        ini_set('memory_limit', '1024M');
        ini_set('max_execution_time', -1);

        echo "Resharding can take a long time. You can monitor the execution process looking outputlogfile.\n";
        $result = Tinebase_Shard_Manager::getInstance()->reshardVirtualShard($args['database'], $args['virtualshard'], $args['key'], $args['outputlogfile']);

        return $result;
    }

    /**
     * Get associations of Shardkey
     *
     * @param $_opts
     * @return boolean success
     */
    public function getAssociationOfShardKey($_opts)
    {
        if (! $this->_checkAdminRight()) {
            return FALSE;
        }

        $args = $this->_parseArgs($_opts, array('database','shardkey','outputlogfile'));

        $shardkey = ($args['shardkey'] == strtolower('all')) ? NULL : $args['shardkey'];

        $result = Tinebase_Shard_Manager::getInstance()->getAssociationOfShardKey($args['database'], $shardkey, $args['outputlogfile']);

        return $result;
    }

    /**
     * Set associations between VirtualShards and Connection Config Keys
     *
     * @param $_opts
     * @return boolean success
     */
    public function associationVirtualshardConnectionconfigkey($_opts)
    {
        if (! $this->_checkAdminRight()) {
            return FALSE;
        }

        $args = $this->_parseArgs($_opts, array('database', 'key','fromvirtualshard','tovirtualshard','outputlogfile'));
        $result = Tinebase_Shard_Manager::getInstance()->associationVirtualshardConnectionconfigkey($args['database'], $args['key'], $args['fromvirtualshard'], $args['tovirtualshard'], $args['outputlogfile']);
    }
}
