<?php
/**
 * Abstract class for Redistributer
 *
 * @package     Shard
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Emerson Faria Nobre <emerson-faria.nobre@serpro.gov.br>
 * @copyright   Copyright (c); 2015 Metaways Infosystems GmbH (http://www.metaways.de);
 *
 */

/**
 * Abstract class for Redistributer
 *
 * @package     Shard
 * @subpackage  Redistributer
 */
abstract class Tinebase_Shard_Redistributer_Abstract implements Tinebase_Shard_Redistributer_Interface
{
    /**
     * constant to define the number of registries added per insert statement
     */
    const SIZE_OF_INSERT_BUCKET = 200;

    /**
     * constant to define the number of registries removed per delete statement
     */
    const SIZE_OF_DELETE_BUCKET = 1000;

    /**
     * database tables prefix
     *
     * @var string
     */
    protected $_tablePrefix;

    /**
     * Shard metadata obtained from setup.xml files
     *
     * @var array
     */
    protected $_shardMetadata = NULL;

    /**
     * Path to all related tables required to build join clause of sql statement
     *
     * @var array
     */
    protected $_pathToAllTables;

    /**
     * Path to root table required to build join clause of sql statement
     *
     * @var array
     */
    protected $_pathToRootTable;

    /**
     * Array of created relations
     *
     * @var array
     */
    protected $_createdRelations = NULL;

    /**
     * Tables required to build where clause of sql statement
     *
     * @var array
     */
    protected $_whereTables;

    /**
     * Array of filters to build where clause of sql statement
     *
     * @var array
     */
    protected $_filters = array();

    /**
     * Array of sql select statements
     *
     * @var array
     */
    protected $_selectStatements;

    /**
     * Array of sql delete statements
     *
     * @var array
     */
    protected $_deleteStatements;

    /**
     * Database connection config key
     *
     * @var string
     */
    protected $_database;

     /**
     * Constructor
     *
     * @param  string    $_database
     */
    function __construct($_database)
    {
        $this->_database = $_database;

        // Initialize Shard Metadata
        $applicationsSetupXml = $this->_getApplicationsSetupXml();

        $this->_shardMetadata = $this->_getShardMetadata($applicationsSetupXml);
    }

     /**
     * Get applications setup.xml
     *
     * @return SimpleXMLElement
     */
    private function _getApplicationsSetupXml()
    {
        $setupController = Setup_Controller::getInstance();
        $applications = $setupController->searchApplications();

        foreach ($applications['results'] as $properties) {
            if ($properties['install_status'] === 'uptodate') {
                $applicationsSetupXml[$properties['name']] = $setupController->getSetupXml($properties['name']);
            } else if ($properties['install_status'] === 'updatable') {
                Tinebase_Shard_Manager::getLogger()->debug('Applications must be updated.');
                throw new Setup_Exception('Applications must be updated.');
            }
        }

        return $applicationsSetupXml;
    }

    /**
     * Get Shard TAGS from applications setup.xml
     *
     * @param  SimpleXMLElement  $_applicationsSetupXml
     * @return array
     */
    private function _getShardMetadata($_applicationsSetupXml)
    {
        foreach ($_applicationsSetupXml as $applicationName => $xml) {
            foreach ($xml->tables->table as $tableProperties) {
                $isShardTable = FALSE;
                $tableName = (string) $tableProperties->name;

                foreach($tableProperties->declaration->shard as $shard) {
                    if ($shard->backend == $this->_database) {
                        $isShardTable = TRUE;
                    } else {
                        $isShardTable = FALSE;
                        continue;
                    }
                    $shardTable = (isset($shard->shardtable)) ? (string) $shard->shardtable : TRUE;
                    $singleKeyTable = isset($shard->keytable) ? TRUE : FALSE;
                    if ($singleKeyTable) {
                        $keyTable = (string) $shard->keytable;
                        $shardMetadata['TABLES'][$keyTable][$tableName] = $shardTable;
                    }
                    foreach($shard->relation as $shardRelation) {
                        if (! $singleKeyTable) {
                            $keyTable = (string) $shardRelation->keytable;
                            $shardMetadata['TABLES'][$keyTable][$tableName] = $shardTable;
                        }

                        $relationTable1 = array($tableName => (string) $shardRelation->field);
                        $relationTable2 = array((string) $shardRelation->reference->table
                                                => (string) $shardRelation->reference->field);

                        $relationCondition = array();
                        foreach ($shardRelation->condition as $condition) {
                            $fieldType = (string) reset($tableProperties->xpath('.//declaration/field[name="'
                                                                                . (string) $condition->field . '"]/type'));
                            $relationCondition['condition'][$tableName][] =
                                    array('field' => (string) $condition->field, 'operator'
                                          => (string) $condition->operator, 'dataType' => $fieldType, 'value'
                                          => (string) $condition->value);
                        }

                        $shardMetadata['TABLE_RELATIONS'][$keyTable][] = array_merge($relationTable1
                                                                                     , $relationTable2, $relationCondition);
                    }
                    foreach($shard->filter as $shardFilter) {
                        if (! $singleKeyTable) {
                            $keyTable = (string) $shardFilter->keytable;
                        }
                        $fieldType = (string) reset($tableProperties->xpath('.//declaration/field[name="'
                                                                             . (string) $shardFilter->field . '"]/type'));
                        $shardMetadata['TABLE_FILTERS'][$keyTable][$tableName][] =
                                array('field' => (string) $shardFilter->field, 'operator'
                                      => (string) $shardFilter->operator, 'dataType' => $fieldType, 'value'
                                      => (string) $shardFilter->value);
                    }
                }

                if ($isShardTable) {

                    foreach($tableProperties->declaration->index as $indexProperties){
                        if (isset($indexProperties->primary) && $indexProperties->primary == 'true') {
                            foreach($indexProperties->field as $indexField) {
                                $shardMetadata['TABLE_PK'][$tableName][] = (string) $indexField->name;
                            }
                        }
                    }

                    foreach($tableProperties->declaration->field as $field) {
                        if (isset($field->autoincrement) && $field->autoincrement == 'true') {
                            $shardMetadata['TABLE_AUTOINCREMENT'][$tableName][] = (string) $field->name;
                        }
                    }
                }
            }
        }

        return $shardMetadata;
    }

    /**
     * Relation between tables already was created
     *
     * @param  array $_relation
     * @return boolean
     */
    private function _isRelationCreated($_relation)
    {
        if (is_array($this->_createdRelations)) {
            foreach ($this->_createdRelations as $createdRelation) {
                if (array_values($createdRelation) == array_values($_relation)
                        && array_key_exists(key(array_slice($_relation,0,1)), $createdRelation)
                        && array_key_exists(key(array_slice($_relation,1,1)), $createdRelation)) {
                        return TRUE;
                }
            }
        }

        return FALSE;
    }

    /**
     * Get conditional relations between tables
     *
     * @param  string $_keyTable
     * @param  string $_table
     * @return array
     */
    private function _getConditionalRelations($_keyTable, $_table)
    {
        $conditionalRelations = array();
        foreach($this->_shardMetadata['TABLE_RELATIONS'][$_keyTable] as $relation) {
            if(array_key_exists('condition', $relation) && (array_key_exists($_table, $relation['condition']))) {
                $conditionalRelations[] = $relation;
            }
        }
        return $conditionalRelations;
    }

    /**
     * Add JOIN statement between tables to $_joinStatement
     *
     * @param  string $_keyTable
     * @param  string $_fromTable
     * @param  string $_joinTable
     * @param  array $_joinStatement
     * @return array
     */
    private function _buildJoins($_keyTable, $_fromTable, $_joinTable, $_joinStatement)
    {
        if ($_joinTable === $_keyTable) {
            $this->_pathToRootTable[] = $_joinTable;
            $this->_pathToAllTables[] = $_joinTable;
            $this->_whereTables[$_keyTable][$_joinTable] = $this->_shardMetadata['TABLE_FILTERS'][$_keyTable][$_joinTable];
            $_joinStatement['depth'] = 0;
            return $_joinStatement;
        }

        if ((count($this->_getConditionalRelations($_keyTable, $_joinTable)) > 1) && ($_joinTable !== $_fromTable)) {
            return $_joinStatement;
        }

        foreach($this->_shardMetadata['TABLE_RELATIONS'][$_keyTable] as $relation) {

            if ($this->_isRelationCreated($relation)) {
                continue;
            }

            if (array_key_exists($_joinTable, $relation)){
                $table1 = key(array_slice($relation,0,1));
                $field1 = $relation[$table1];
                $table2 = key(array_slice($relation,1,1));
                $field2 = $relation[$table2];
                if ($table1 == $_joinTable) {
                    $referenceTable = $table2;
                    $referenceField = $field2;
                    $foreignTable = $table1;
                    $foreignField = $field1;
                } else {
                    $referenceTable = $table1;
                    $referenceField = $field1;
                    $foreignTable = $table2;
                    $foreignField = $field2;
                }

                $this->_createdRelations[] = $relation;

                $_joinStatement = $this->_buildJoins($_keyTable, $_fromTable, $referenceTable, $_joinStatement);

                // If conditional JOIN
                foreach ($relation['condition'] as $conditionTable => $conditions) {

                    if ((count($this->_getConditionalRelations($_keyTable, $conditionTable)) < 2)
                       || ($conditionTable === $_fromTable)) {
                        if (! in_array($conditionTable, $this->_pathToAllTables)) {
                            $this->_pathToAllTables[] = $conditionTable;
                        }

                        if (! in_array($conditionTable, $this->_whereTables[$_keyTable])) {
                            $this->_whereTables[$_keyTable][$conditionTable] = $relation['condition'][$conditionTable];
                        }
                    }
                }

                foreach ($this->_shardMetadata['TABLE_FILTERS'][$_keyTable] as $whereTable => $whereProperties)  {
                    foreach ($whereProperties as $whereField => $whereComparation)  {
                        if ($whereTable == $referenceTable) {
                            if (! in_array($referenceTable, $this->_pathToAllTables)) {
                                $this->_pathToAllTables[] = $referenceTable;
                            }

                            if ($referenceTable !== $_keyTable) {
                                if (! in_array($referenceTable, $this->_whereTables[$_keyTable])) {
                                    $this->_whereTables[$_keyTable][$referenceTable] =
                                            $this->_shardMetadata['TABLE_FILTERS'][$_keyTable][$referenceTable];
                                }
                            }
                        } else if ($whereTable == $foreignTable) {
                            if (! in_array($foreignTable, $this->_pathToAllTables)) {
                                $this->_pathToAllTables[] = $foreignTable;
                            }

                            if (! in_array($foreignTable, $this->_whereTables[$_keyTable])) {
                                $this->_whereTables[$_keyTable][$foreignTable] =
                                        $this->_shardMetadata['TABLE_FILTERS'][$_keyTable][$foreignTable];
                            }
                        }
                    }
                }

                if (in_array($referenceTable, $this->_pathToAllTables)) {
                    if (! in_array($foreignTable, $this->_pathToAllTables)) {
                        $this->_pathToAllTables[] = $foreignTable;
                    }
                }

                if (in_array($referenceTable, $this->_pathToAllTables)) {
                    $_joinStatement['query'] = ' JOIN ' . $this->_tablePrefix . $referenceTable
                                             . ' ON (' . $this->_tablePrefix . $foreignTable . '.'
                                             . $foreignField . ' = ' . $this->_tablePrefix . $referenceTable . '.'
                                             . $referenceField . ')' . $_joinStatement['query'];
                }

                if (in_array($referenceTable, $this->_pathToRootTable)) {
                    $this->_pathToRootTable[] = $foreignTable;
                    $isPkRelation = FALSE;
                    foreach($this->_shardMetadata['TABLE_PK'][$referenceTable] as $pkField) {
                        if ($pkField != $referenceField) {
                            $isPkRelation = TRUE;
                        }
                    }
                    ($isPkRelation) ? $_joinStatement['depth']-- : $_joinStatement['depth']++;
                }
            }
        }
        return $_joinStatement;
    }

    /**
     * Parse WHERE parameters datatype
     *
     * @param  string $_dataType
     * @param  string $_value
     * @return string|integer
     */
    private function _parseParameter($_dataType, $_value)
    {
        switch ($_dataType) {
            case 'text':
                $return = "'$_value'";
                break;
            default:
                $return = $_value;
        }

        return $return;
    }

    /**
     * Build WHERE statement
     *
     * @param  string $_keyTable
     * @param  array $_parameters
     * @return array
     */
    private function _buildWhere($_keyTable, $_parameters)
    {
        $where = ' WHERE';
        $and = FALSE;
        foreach ($this->_whereTables[$_keyTable] as $whereTable => $filters) {
            foreach($filters as $filter) {
                $filterField = $filter['field'];
                $filterOperator = $filter['operator'];
                $filterDataType = $filter['dataType'];
                $where .= ($and === TRUE) ? " AND" : "";
                if ($filter['value'] == '?') {
                    if ($filterOperator == 'in') {
                        $isFirstValue = TRUE;
                        $filterValue = '(';
                        foreach ($_parameters[$whereTable][$filterField] as $value) {
                            $value = $this->_parseParameter($filterDataType, $value);
                            $filterValue .= ($isFirstValue) ? $value : ',' . $value;
                            $isFirstValue = FALSE;
                        }
                        $filterValue .= ')';
                    } else {
                        $filterValue = $this->_parseParameter($filterDataType, $_parameters[$whereTable][$filterField]);
                    }
                } else {
                    $filterValue = $this->_parseParameter($filterDataType, $filter['value']);
                }
                $where .= " (" . $this->_tablePrefix . $whereTable . "." . $filterField . " "
                        . $filterOperator . " " . $filterValue. ")";
                $and = TRUE;
            }
        }
        return $where;
    }

    /**
     * Build SELECT statements in $this->_selectStatements
     *
     * @param  array $_parameters
     */
    private function _buildSelectStatements($_parameters)
    {
        if (isset($this->_selectStatements)){
            return;
        }
        foreach($this->_shardMetadata['TABLES'] as $keyTable => $fromTables) {
            $depth = NULL;
            foreach($fromTables as $fromTable => $shardThisTable) {

                if ($shardThisTable === TRUE ) {
                    $isFirstPkField = TRUE;
                    $pkFields = '';
                    foreach($this->_shardMetadata['TABLE_PK'][$fromTable] as $pkField) {
                        $pkFields .= ($isFirstPkField) ? $pkField : ',' . $pkField;
                        $isFirstPkField = FALSE;
                    }

                    $conditionalRelations = $this->_getConditionalRelations($keyTable, $fromTable);

                    if (count($conditionalRelations) > 1) {
                        $addUnion = FALSE;
                        $select[$keyTable][$fromTable] = '';
                        foreach ($conditionalRelations as $key => $value) {
                           $this->_pathToRootTable = NULL;
                           $this->_pathToAllTables = NULL;
                           $this->_whereTables = NULL;
                           $tmp =  $conditionalRelations;
                           unset($tmp[$key]);
                           $this->_createdRelations = $tmp;
                           unset($tmp);
                           $joins = $this->_buildJoins($keyTable, $fromTable, $fromTable, NULL);
                           $select[$keyTable][$fromTable] .= ($addUnion) ? ' UNION ' : '';
                           $select[$keyTable][$fromTable] .= 'SELECT DISTINCT ' . $this->_tablePrefix . $fromTable
                                                           . '.* FROM ' . $this->_tablePrefix . $fromTable;
                           $select[$keyTable][$fromTable] .= isset($joins['query']) ? $joins['query'] : '';
                           $select[$keyTable][$fromTable] .= $this->_buildWhere($keyTable, $_parameters);
                           $select[$keyTable][$fromTable] = $joins['depth'];
                           $addUnion = TRUE;
                        }
                    } else {
                        $this->_createdRelations = NULL;
                        $this->_pathToRootTable = NULL;
                        $this->_pathToAllTables = NULL;
                        $this->_whereTables = NULL;
                        $joins = $this->_buildJoins($keyTable, $fromTable, $fromTable, NULL);
                        $select[$keyTable][$fromTable] = 'SELECT DISTINCT ' . $this->_tablePrefix . $fromTable . '.* FROM '
                                                       . $this->_tablePrefix . $fromTable;
                        $select[$keyTable][$fromTable] .= isset($joins['query']) ? $joins['query'] : '';
                        $select[$keyTable][$fromTable] .= $this->_buildWhere($keyTable, $_parameters);
                        $depth[$fromTable] = $joins['depth'];
                    }
                $select[$keyTable][$fromTable] .= ' ORDER BY ' . $this->_tablePrefix . $fromTable . '.' .$pkFields;
                $select[$keyTable][$fromTable] .= ';';
                }
            }

            asort($depth);

            $this->_selectStatements[$keyTable] = NULL;
            foreach($depth as $fromTable => $order) {
                $this->_selectStatements[$keyTable][$fromTable] = $select[$keyTable][$fromTable];
            }
        }
    }

    /**
     * Remove Autoincrement fields
     * @param array $_rows
     * @return array
     */
    private function _removeAutoincrementeFields(&$_rows)
    {
        $memory = (memory_get_usage() / 1000);
        foreach ($_rows as $table => $values) {
            foreach ($_rows[$table] as $key => $value) {
                if (isset($this->_shardMetadata['TABLE_AUTOINCREMENT'][$table])) {
                    foreach($this->_shardMetadata['TABLE_AUTOINCREMENT'][$table] as $autoIncrementField) {
                        unset($_rows[$table][$key][$autoIncrementField]);
                    }
                }
            }
        }
        $memory = (memory_get_usage() / 1000);
    }

    /**
     * Fetch all Shard Key data
     *
     * @param string $_shardKey
     * @param string $_backend_connection_config
     * @return array
     */
    public function &fetchAllShardKeyData($_shardKey, $_backend_connection_config)
    {
        $this->_tablePrefix = $_backend_connection_config->tableprefix;

        if (empty($this->_filters[$_shardKey])) {
            $shardConfig = Tinebase_Shard_Manager::getShardConfig($this->_database);

            $shardkeyStrategy = new Tinebase_Shard_Shardkey_Strategy_Context();

            $resultShardKeyStrategy = $shardkeyStrategy->setStrategy($shardConfig->shardKeyStrategy);

            $this->_filters[$_shardKey] = $shardkeyStrategy->getFilters($_shardKey);
        }

        $this->_buildSelectStatements($this->_filters[$_shardKey]);

        $db = TineBase_Core::getDb($_backend_connection_config);

        $rows = array();
        try {
            $numberOfRecords = 0;
            foreach($this->_selectStatements as $keyTable => $selectStatement) {
                foreach($selectStatement as $fromTable => $select) {
                    $result = $db->fetchAll($select);
                    $numberOfRecords += count($result);
                    $rows[$fromTable] = isset($rows[$fromTable]) ? array_merge($rows[$fromTable], $result) : $result;
                }
            }
        } catch (Exception $ex) {
            throw $ex;
            return FALSE;
        }

        Tinebase_Shard_Manager::getLogger()->info("Fetched " . $numberOfRecords . " records");
        return $rows;
    }


    /**
     * Delete all Shard Key data
     *
     * @param array $_rows
     * @param string $_backend_connection_config
     * @return integer | boolean
     */
    public function deleteAllShardKeyData(&$_rows, $_backend_connection_config)
    {
        $tablePrefix = $_backend_connection_config->tableprefix;

        $db = TineBase_Core::getDb($_backend_connection_config);

        $rows = array_reverse($_rows, TRUE);
        try {
            $numberOfRecords = 0;
            $db->beginTransaction();
            foreach ($rows as $table => $registries) {
                if (empty($registries)) {
                    continue;
                }
                $totalValues = count($registries);
                $deleteWhere = '';
                $bindPos = 0;
                $countRegistries = 0;
                foreach ($registries as $registry) {
                    $countRegistries++;
                    $deleteWhere .= ($bindPos == 0) ? '' : ' OR ';
                    $and = FALSE;
                    $deleteWhere .= '(';
                    foreach($this->_shardMetadata['TABLE_PK'][$table] as $pkField) {
                        $deleteWhere .= ($and === TRUE) ? ' AND ' : '';
                        $deleteWhere .= $pkField . ' = ' . $db->quote($registry[$pkField]);
                        $and = TRUE;
                    }
                    $deleteWhere .= ')';

                    if (($bindPos + 1) == self::SIZE_OF_DELETE_BUCKET || $totalValues == $countRegistries) {
                        $numberOfRecords += $db->delete($tablePrefix . $table, $deleteWhere);
                        $bindPos = 0;
                        $deleteWhere = '';
                    } else {
                        $bindPos++;
                    }
                }
            }
            $db->commit();
        } catch (Exception $ex) {
            $db->rollBack();
            throw $ex;
            return FALSE;
        }
        Tinebase_Shard_Manager::getLogger()->info("Deleted " . $numberOfRecords . " records");
        return $numberOfRecords;
    }

    /**
     * Insert all Shard Key data
     *
     * @param array $_rows
     * @param string $_backend_connection_config
     * @return array | boolean
     */
    public function insertAllShardKeyData(&$_rows, $_backend_connection_config)
    {
        $tablePrefix = $_backend_connection_config->tableprefix;

        $db = TineBase_Core::getDb($_backend_connection_config);

        $this->_removeAutoincrementeFields($_rows);

        $numberOfRecords = 0;
        try {
            $db->beginTransaction();

            foreach ($_rows as $table => $values) {
                $numOfCols = 0;

                if (count($values) > 0) {
                    $colNames = array();
                    foreach($values[0] as $colName => $value) {
                        $colNames[] = $db->quoteIdentifier($colName);
                        $numOfCols++;
                    }
                } else {
                    continue;
                }

                $queryCols = '(' . implode(',', $colNames) . ')';
                $query = 'INSERT INTO ' . $db->quoteIdentifier($this->_tablePrefix . $table) . ' ' . $queryCols . ' VALUES ';

                $bindPos = 1;
                $registryPos = 0;
                $totalValues = count($values);

                do {
                    $leftValues = ($totalValues - $registryPos);

                    $bucketSize = ($leftValues  < self::SIZE_OF_INSERT_BUCKET) ? $leftValues : self::SIZE_OF_INSERT_BUCKET;
                    $hasMore = ($leftValues  > self::SIZE_OF_INSERT_BUCKET ) ? TRUE : FALSE;

                    $allPlaces = implode(',', array_fill(0, $bucketSize, '('.str_pad('', ($numOfCols*2)-1, '?,').')'));
                    $queryInsert = $query . $allPlaces;

                    unset($stmt);
                    $stmt = $db->prepare($queryInsert);

                    $bindPos = 1;
                    for ($count = 1; $count <= $bucketSize; $count++) {
                        foreach($values[$registryPos] as $value) {
                            $stmt->bindValue($bindPos, $value);
                            $bindPos++;
                        }
                        $registryPos++;
                    }

                    $stmt->execute();

                    $numberOfRecords += $stmt->rowCount();
                } while ($hasMore);
            }
            $db->commit();

            Tinebase_Shard_Manager::getLogger()->info("Inserted " . $numberOfRecords . " records");
            return $numberOfRecords;

        } catch (Exception $ex) {
            $db->rollBack();
            throw $ex;
            return FALSE;
        }
    }

    /**
     * Compare Shard Key data
     *
     * @param array $_rowsOrigin
     * @param string $_shardKey
     * @param string $_backend_connection_config
     * @return boolean
     */
    public function compareAllShardKeyData(&$_rowsOrigin, $_shardKey, $_backend_connection_config)
    {
        $db = TineBase_Core::getDb($_backend_connection_config);
        $this->_removeAutoincrementeFields($_rowsOrigin);
        $md5Origin = md5(serialize($_rowsOrigin));
        $_rowsOrigin = NULL;
        $rowsDestination = $this->fetchAllShardKeyData($_shardKey, $_backend_connection_config);
        $this->_removeAutoincrementeFields($rowsDestination);
        $md5Destination = md5(serialize($rowsDestination));
        $rowsDestination = NULL;

        return ($md5Origin == $md5Destination) ? TRUE : FALSE;
    }
}
