<?php
/**
 * Tine 2.0
 *
 * @package     Tinebase
 * @subpackage  Exception
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2008 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Flávio Gomes da Silva Lisboa <flavio.lisboa@serpro.gov.br>
 *
 */

/**
 * Update exception
 *
 * @package     Tinebase
 * @subpackage  Exception
 */
class Tinebase_Exception_Update extends Tinebase_Exception
{
    /**
     *
     * @param string $_message
     * @param number $_code
     */
    public function __construct($_message, $_code=503)
    {
        parent::__construct($_message, $_code);
    }
}
