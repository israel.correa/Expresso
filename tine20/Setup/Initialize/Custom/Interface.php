<?php
/**
 * Tine 2.0
  * 
 * @package     Setup
 * @subpackage  Custom
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Guilherme Striquer Bisotto <guilherme.bisotto@serpro.gov.br>
 * @copyright   Copyright (c) 2008-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * Interface for plugins of setup initialize
 * 
 * @package     Setup
 * @subpackage  Custom
 */
interface Setup_Initialize_Custom_Interface 
{    
    /**
     * Run customization for initialize application
     */
    public static function applyCustomizations();    
}
