<?php
/**
 * Tine 2.0
 *
 * @package     Setup
 * @subpackage  Server
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2011 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Philipp Schuele <p.schuele@metaways.de>
 *
 */

/**
 * dispatcher and initialisation class (functions are static)
 * - dispatchRequest() function
 * - initXYZ() functions
 * - has registry and config
 *
 * @package     Setup
 */
class Setup_Core extends Tinebase_Core
{
    /**
     * constant for config registry index
     *
     */
    const CHECKDB = 'checkDB';

    /**
     *
     * @var path for global config file to be used
     */
    private static $globalConfigFilePath = NULL;

    /**
     *
     * @var path for domain config file to be used
     */
    private static $configFilePath = NULL;

    /**
     * init setup framework
     */
    public static function initFramework()
    {
        Setup_Core::setupConfig();
        
        Setup_Core::setupTempDir();

        Setup_Core::setupStreamWrapper();
        
        //Cache must be setup before User Locale because otherwise Zend_Locale tries to setup
        //its own cache handler which might result in a open_basedir restriction depending on the php.ini settings
        Setup_Core::setupCache();
        
        Setup_Core::setupBuildConstants();
        
        // setup a temporary user locale/timezone. This will be overwritten later but we
        // need to handle exceptions during initialisation process such as seesion timeout
        Setup_Core::set('locale', new Zend_Locale('en_US'));
        Setup_Core::set(Tinebase_Core::USERTIMEZONE, 'UTC');
        
        Setup_Core::setupUserLocale();
        
        header('X-API: http://www.tine20.org/apidocs/tine20/');
    }
    
    public static function startSetupSession()
    {
        Tinebase_Session::setSessionBackend();

        Zend_Session::start();

        $setupSession = Setup_Session::getSessionNamespace();

        if (isset($setupSession->setupuser)) {
            self::set(self::USER, $setupSession->setupuser);
        }

        if (!isset($setupSession->jsonKey)) {
            $setupSession->jsonKey = Tinebase_Record_Abstract::generateUID();
        }
        self::set('jsonKey', $setupSession->jsonKey);
    }
    
    /**
     * dispatch request
     *
     */
    public static function dispatchRequest()
    {
        $request = new \Zend\Http\PhpEnvironment\Request();
        self::set(self::REQUEST, $request);

        // ensures that domain info be read from config.inc.php of root folder
        Tinebase_Config_Manager::getInstance()->detectDomain();

        $server = NULL;
        
        /**************************** JSON API *****************************/

        if ( (isset($_SERVER['HTTP_X_TINE20_REQUEST_TYPE']) && $_SERVER['HTTP_X_TINE20_REQUEST_TYPE'] == 'JSON')  ||
             (isset($_POST['requestType']) && $_POST['requestType'] == 'JSON')
           ) {
            $server = new Setup_Server_Json();

        /**************************** CLI API *****************************/
        
        } elseif (php_sapi_name() == 'cli') {
            // ensures that save_handler for session is files because value 'cluster' for CLI causes an error
            ini_set('session.save_handler', 'files');
            $server = new Setup_Server_Cli();
            
        /**************************** HTTP API ****************************/
        
        } else {
            $server = new Setup_Server_Http();
        }
        
        $server->handle();
    }
    
    /**
     * checks if global config file exists
     *
     * @return bool
     */
    public static function configFileExists()
    {
        return file_exists(Tinebase_Config::getInstance()->getGlobalConfigFileNamePath());
    }

    /**
     * checks if global config file or tine root is writable
     *
     * @return bool
     */
    public static function configFileWritable()
    {
        if (self::configFileExists()) {
            $configFilePath = Tinebase_Config::getInstance()->getGlobalConfigFileNamePath();
            return is_writable($configFilePath);
        } else {
            $path = dirname(dirname(__FILE__));
            $testfilename = $path . DIRECTORY_SEPARATOR . uniqid(mt_rand()).'.tmp';
            if (!($f = @fopen($testfilename, 'w'))) {
                error_log(__METHOD__ . '::' . __LINE__ . ' Your tine root dir ' . $path . ' is not writable for the webserver! Config file can\'t be created.');
                return false;
            }
            fclose($f);
            unlink($testfilename);
            return true;
        }
    }
    
    /**
     * initializes the database connection
     *
     * @param Tinebase_Config_Struct|NULL $_dbConfig
     * @return boolean
     *
     * @todo try to write to db, if it fails: self::set(Setup_Core::CHECKDB, FALSE);
     */
    public static function setupDatabaseConnection($_dbConfig = NULL)
    {
        try {
            parent::setupDatabaseConnection($_dbConfig);
        } catch (Zend_Db_Adapter_Exception $zae) {
            Setup_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' ' . $zae->getMessage());
        } catch (Zend_Db_Exception $zde) {
            Setup_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . ' ' . $zde->getMessage());
        }
        
        $dbCheck = self::get(self::CHECKDB);

        return $dbCheck;
    }
    
    /**
     * setups the logger
     *
     * NOTE: if no logger is configured, we write to stderr in setup
     *
     * @param $_defaultWriter Zend_Log_Writer_Abstract default log writer
     */
    public static function setupLogger(Zend_Log_Writer_Abstract $_defaultWriter = NULL)
    {
        $writer = new Zend_Log_Writer_Stream('php://stderr');
        parent::setupLogger($writer);
    }

    /**
     * initializes the build constants like buildtype, package information, ...
     */
    public static function setupBuildConstants()
    {
        $config = self::getConfig();
        define('TINE20_BUILDTYPE',           strtoupper($config->get('buildtype', 'DEVELOPMENT')));
        define('TINE20SETUP_CODENAME',       Tinebase_Helper::getDevelopmentRevision());
        define('TINE20SETUP_PACKAGESTRING', 'none');
        define('TINE20SETUP_RELEASETIME',   'none');
    }
    
    /**
     * setup the cache and add it to zend registry
     *
     * Ignores {@param $_enabled} and always sets it to false
     *
     */
    public static function setupCache($_enabled = true)
    {
        // disable caching for setup
        parent::setupCache(false);
    }
}
