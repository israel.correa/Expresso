<?php

$phrase1 = sprintf($this->translate->_('The user %s is inviting you to a Webconference'), $this->accountFullName) . "\n";

echo <<<EOT
<div style="font-family:arial, helvetica, sans-serif;">
	{$phrase1} 
	<br/>
	<br/>
	<p>
	    <b>{$this->translate->_('Room Title')} </b>: {$this->roomTitle}
	</p>
	<p>
	    <b>{$this->translate->_('Type')} </b>: {$this->type}
	</p>
	<br/>
        <div>
            <span class="{$this->url}"></span>
            <span class="tinebase-webconference-link">
                <a href="{$this->url}" target="_brank">
                    {$this->translate->_('Join the webconference')}
                </a>
            </span>
        </div>
        
</div>
EOT;
