<?php
/**
 * @package     Webconference
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2011 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * attendee role record
 * 
 * @package     Webconference
 */
class Webconference_Model_AttendeeRole extends Tinebase_Config_KeyFieldRecord
{
    
}