<?php
/**
 * @package     Webconference
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2011 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * room status record
 * 
 * @package     Webconference
 */
class Webconference_Model_RoomStatus extends Tinebase_Config_KeyFieldRecord
{
    
}