<?php
/**
 * class to Filter Record data
 * 
 * @package     Webconference
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>, Marcelo Teixeira <marcelo.teixeira@serpro.gov.br>
 * @copyright   Copyright (c) 2012 Metaways Infosystems GmbH (http://www.metaways.de)
 * 
 */


/**
 * Room filter Class
 * 
 * @package     Sales
 * @subpackage  Filter
 */
class Webconference_Model_RoomFilter extends Tinebase_Model_Filter_FilterGroup
{
    /**
     * @var string class name of this filter group
     *      this is needed to overcome the static late binding
     *      limitation in php < 5.3
     */
    protected $_className = 'Webconference_Model_RoomFilter';
    
    /**
     * @var string application of this filter group
     */
    protected $_applicationName = 'Webconference';
    
    /**
     * @var string name of model this filter group is designed for
     */
    protected $_modelName = 'Webconference_Model_Room';
    
    protected $_defaultFilter = 'query';
    
    /**
     * @var array filter model fieldName => definition
     */
    protected $_filterModel = array(
        'id'                    => array('filter' => 'Tinebase_Model_Filter_Id', 'options' => array('modelName' => 'Webconference_Model_Room')),
        'query'                 => array(
            'filter' => 'Tinebase_Model_Filter_Query', 
            'options' => array('fields' => array('status', 'room_name', 'title', 'wconf_config_id'))
        ),
        'status'		=> array('filter' => 'Tinebase_Model_Filter_Text'),
        'room_name'		=> array('filter' => 'Tinebase_Model_Filter_Text'),
	'title'			=> array('filter' => 'Tinebase_Model_Filter_Text'),
	'container_id'          => array('filter' => 'Webconference_Model_WebconferenceFilter', 'options' => array('applicationName' => 'Webconference')),
        'attender'              => array('filter' => 'Webconference_Model_AttenderFilter'),
        'attender_role'         => array('filter' => 'Webconference_Model_AttenderRoleFilter'),
        'organizer'             => array('filter' => 'Addressbook_Model_ContactIdFilter', 'options' => array('modelName' => 'Addressbook_Model_Contact')),
	//'dtstart'               => array('filter' => 'Tinebase_Model_Filter_DateTime'),
	'creation_time'         => array('filter' => 'Tinebase_Model_Filter_DateTime'),
	'grants'                => array('filter' => 'Webconference_Model_GrantFilter'),
    );
}