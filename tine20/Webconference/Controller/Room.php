<?php

/**
 * Room controller for Webconference application
 * 
 * @package     Webconference
 * @subpackage  Controller
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@sepro.gov.br>
 * @copyright   Copyright (c) 2012 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */

/**
 * WebconferenceConfig controller class for Room record
 * 
 * @package     Webconference
 * @subpackage  Controller
 */
class Webconference_Controller_Room extends Tinebase_Controller_Record_Abstract {

    /**
     * the constructor
     *
     * don't use the constructor. use the singleton 
     */
    private function __construct() 
    {
        $this->_purgeRecords = FALSE;
	$this->_doContainerACLChecks = FALSE;
        $this->_resolveCustomFields = TRUE;
	
	$this->_applicationName = 'Webconference';
        $this->_modelName	= 'Webconference_Model_Room';
        $this->_backend         = new Webconference_Backend_Sql();
        $this->_currentAccount  = Tinebase_Core::getUser();
    }

    /**
     * holds the instance of the singleton
     *
     * @var Webconference_Controller_Room
     */
    private static $_instance = NULL;

    /**
     * the singleton pattern
     *
     * @return Webconference_Controller_Room
     */
    public static function getInstance() 
    {
        if (self::$_instance === NULL) 
	{
            self::$_instance = new Webconference_Controller_Room();
        }
        return self::$_instance;
    }
    
    private function _updateRoomStatus()
    {
	$data = array("status"=>"A");
	$filter = new Webconference_Model_RoomFilter($data);
	$rooms = parent::search($filter, NULL, FALSE);
	foreach ($rooms as $room) {
	    if (Webconference_Controller_BigBlueButton::getInstance()->isMeetingActive($room->room_name, $room->wconf_config_id) == false){
		$room->status = "E";
		$this->update($room);
	    }
	}
    }
    
    public function search(Tinebase_Model_Filter_FilterGroup $_filter = NULL, Tinebase_Record_Interface $_pagination = NULL, $_getRelations = FALSE, $_onlyIds = FALSE, $_action = 'get') 
    {
	$this->_updateRoomStatus();
        $result = parent::search($_filter, $_pagination, $_getRelations, $_onlyIds, $_action);
        return $result;
    }
    
    public function create(Tinebase_Record_Interface $_record, $_duplicateCheck = TRUE, $_getOnReturn = TRUE)
    {
	try {
            $db = $this->_backend->getAdapter();
            $transactionId = Tinebase_TransactionManager::getInstance()->startTransaction($db);
            $this->_inspectRoom($_record);
	    	    
	    $meetingID = Tinebase_Core::getUser()->accountLoginName.'_'.time();
	    $userName = Tinebase_Core::getUser()->accountFullName;
	    $title = $_record->title;
	    $protocol = $_record->protocol;
	    
	    $_record->wconf_config_id = Webconference_Controller_BigBlueButton::getInstance()->createMeeting($userName, $meetingID, $title, $protocol);
	    $_record->room_name = $meetingID;
            // we need to resolve groupmembers before free/busy checking
            Webconference_Model_Attender::resolveGroupMembers($_record->attendee);
            $room = parent::create($_record);
            Tinebase_TransactionManager::getInstance()->commitTransaction($transactionId);
        } catch (Exception $e) 
	{
            Tinebase_TransactionManager::getInstance()->rollBack();
            throw $e;
        }
        $createdRoom = $this->get($room->getId());
        return $createdRoom;
    }
    
    /**
     * inspect before create/update
     * 
     * @TODO move stuff from other places here
     * @param   Webconference_Model_Room $_record      the record to inspect
     */
    protected function _inspectRoom($_record)
    {
        $_record->organizer = $_record->organizer ? $_record->organizer : Tinebase_Core::getUser()->contact_id;
        
        if (! $_record->resolveOrganizer()->account_id && count($_record->attendee) > 1) 
	{
            $ownAttendee = Webconference_Model_Attender::getOwnAttender($_record->attendee);
            $_record->attendee = new Tinebase_Record_RecordSet('Webconference_Model_Attender', $ownAttendee ? array($ownAttendee) : array());
        }
    } 
    
    /**
     * inspect creation of one record (after create)
     *
     * @param   Tinebase_Record_Interface $_createdRecord
     * @param   Tinebase_Record_Interface $_record
     * @return  void
     */
    protected function _inspectAfterCreate($_createdRecord, Tinebase_Record_Interface $_record)
    {
        $this->_saveAttendee($_record, $_createdRecord);
    }

    /**
     * inspect update of one record (after update)
     *
     * @param   Tinebase_Record_Interface $updatedRecord   the just updated record
     * @param   Tinebase_Record_Interface $record          the update record
     * @param   Tinebase_Record_Interface $currentRecord   the current record (before update)
     * @return  void
     */
    protected function _inspectAfterUpdate($updatedRecord, $record, $currentRecord)
    {
        $this->_saveAttendee($record, $currentRecord);
        $updatedRecord->attendee = clone($record->attendee);
    }  
    
    /* 
     * @param Webconference_Model_Room $_room
     * @param Webconference_Model_Event $_currentEvent
     * @param bool                 $_isRescheduled event got rescheduled reset all attendee status
     */
    protected function _saveAttendee($_room, $_currentRoom = NULL)
    {
        if (! $_room->attendee instanceof Tinebase_Record_RecordSet) 
	{
            $_room->attendee = new Tinebase_Record_RecordSet('Webconference_Model_Attender');
        }
        Webconference_Model_Attender::resolveEmailOnlyAttendee($_room);
        $_room->attendee->wconf_room_id = $_room->getId();
        Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__ . " About to save attendee for room {$_room->id} ");        
        $currentAttendee = $_currentRoom->attendee;
        $diff = $currentAttendee->getMigration($_room->attendee->getArrayOfIds());
        $webconference = Tinebase_Container::getInstance()->getContainerById($_room->container_id);
        // delete attendee
        $this->_backend->deleteAttendee($diff['toDeleteIds']);
        foreach ($diff['toDeleteIds'] as $deleteAttenderId) 
	{
            $idx = $currentAttendee->getIndexById($deleteAttenderId);
            if ($idx !== FALSE) 
	    {
                $currentAttenderToDelete = $currentAttendee[$idx];
                $this->_increaseDisplayContainerContentSequence($currentAttenderToDelete, $_room, Tinebase_Model_ContainerContent::ACTION_DELETE);
            }
        }
	$meetingID = $_room->room_name;
	$configId = $_room->wconf_config_id;
	$title = $_room->title;
	
	// create/update attendee
        foreach ($_room->attendee as $attender) 
	{
	    $userName = $attender->getName();
	    $attender->room_url = Webconference_Controller_BigBlueButton::getInstance()->joinURL($meetingID, $userName,  $contact, $attender->role, $configId);

	    $attenderId = $attender->getId();
            $idx = ($attenderId) ? $currentAttendee->getIndexById($attenderId) : FALSE;
            
            if ($idx !== FALSE) 
	    {
                $currentAttender = $currentAttendee[$idx];
                $this->_updateAttender($attender, $currentAttender, $_room, $webconference);
            } else {
                $this->_createAttender($attender, $_room, $webconference);
            }
	    if (($attender->user_id != Tinebase_Core::getUser()->contact_id) && ($_room->status != 'E')) {
	        $this->_sendEmailInvite($title, $attender->room_url, $attender->user_id, $attender->role);
	    }
        }
    }    

    private function _sendEmailInvite( $_roomTitle, $_roomUrl, $_userId, $_role)
    {
	$translate = Tinebase_Translation::getTranslation('Webconference');
	$contact = $_userId;
	try {
            $fullUser = Tinebase_Core::getUser();
            $messageSubject = $translate->_("Webconference Invite");
	    $view = new Zend_View();
	    $view->setScriptPath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'views');
	    $view->translate  = $translate;
	    $view->url        = $_roomUrl;
	    $view->accountFullName   = $fullUser->accountFullName;
	    $view->roomTitle = $_roomTitle;
	    $type = ($_role === 'MODERATOR' ? 'Moderator' : 'Attendee');
	    $view->type = $translate->_($type);
	    $messageBody = $view->render('eventNotification.php');
	    $attachments = null;
	    $sender = $fullUser;
	    $result = Tinebase_Notification::getInstance()->send($sender, array($contact), $messageSubject, null, $messageBody, $attachments);
	} catch (Exception $e) {
            Tinebase_Core::getLogger()->WARN(__METHOD__ . '::' . __LINE__ . " could not send notification :" . $e);
            return array(
                'message' => $e->getMessage(),
                'result' => $translate->_('An error has occured inviting users') 
                );
        }
	return array(
            'success'   => TRUE,
            'message' => $translate->_('Users invited successfully').'!'
            );
    }
      
    /**
     * creates a new attender
     * 
     * @param Webconference_Model_Attender  $attender
     * @param Webconference_Model_Room $room
     * @param Tinebase_Model_Container $webconference
     */
    protected function _createAttender(Webconference_Model_Attender $attender, Webconference_Model_Room $room, Tinebase_Model_Container $webconference = NULL)
    {
        $attender->user_type = isset($attender->user_type) ? $attender->user_type : Webconference_Model_Attender::USERTYPE_USER;
        $webconference = ($webconference) ? $webconference : Tinebase_Container::getInstance()->getContainerById($room->container_id);
        $userAccountId = $attender->getUserAccountId();
        // attach to display webconference if attender has/is a useraccount
        if ($userAccountId) 
	{
            if ($webconference->type == Tinebase_Model_Container::TYPE_PERSONAL && Tinebase_Container::getInstance()->hasGrant($userAccountId, $webconference, Tinebase_Model_Grants::GRANT_ADMIN)) 
	    {
                // if attender has admin grant to personal physical container, this phys. cal also gets displ. cal
                $attender->displaycontainer_id = $webconference->getId();
            } else if ($attender->displaycontainer_id && $userAccountId == Tinebase_Core::getUser()->getId() && Tinebase_Container::getInstance()->hasGrant($userAccountId, $attender->displaycontainer_id, Tinebase_Model_Grants::GRANT_ADMIN)) 
	    {
                // allow user to set his own displ. cal
                $attender->displaycontainer_id = $attender->displaycontainer_id;
            } else {
                $displayConfId = self::getDefaultDisplayContainerId($userAccountId);
                $attender->displaycontainer_id = $displayConfId;
            }
        }
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) 
	    Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . " New attender: " . print_r($attender->toArray(), TRUE));
        
        Tinebase_Timemachine_ModificationLog::getInstance()->setRecordMetaData($attender, 'create');
        $this->_backend->createAttendee($attender);
        $this->_increaseDisplayContainerContentSequence($attender, $room, Tinebase_Model_ContainerContent::ACTION_CREATE);
    }
    
    /**
     * returns default displayContainer id of given attendee
     *
     * @param string $userAccountId
     */
    public static function getDefaultDisplayContainerId($userAccountId)
    {
        $userAccountId = Tinebase_Model_User::convertUserIdToInt($userAccountId);
        $displayConfId = Tinebase_Core::getPreference('Webconference')->getValueForUser(Webconference_Preference::DEFAULTCONFERENCE, $userAccountId);
        try {
            // assert that displaycal is of type personal
            $container = Tinebase_Container::getInstance()->getContainerById($displayConfId);
            if ($container->type != Tinebase_Model_Container::TYPE_PERSONAL) {
                $displayConfId = NULL;
            }
        } catch (Exception $e) {
            $displayConfId = NULL;
        }
        
        if (! isset($displayConfId)) 
	{
            $containers = Tinebase_Container::getInstance()->getPersonalContainer($userAccountId, 'Webconference_Model_Room', $userAccountId, 0, true);
            if ($containers->count() > 0) {
                $displayConfId = $containers->getFirstRecord()->getId();
            }
        }
        return $displayConfId;
    }
    
    /**
     * increases content sequence of attender display container
     * 
     * @param Webconference_Model_Attender $attender
     * @param Webconference_Model_room $room
     * @param string $action
     */
    protected function _increaseDisplayContainerContentSequence($attender, $room, $action = Tinebase_Model_ContainerContent::ACTION_UPDATE)
    {
        if ($room->container_id === $attender->displaycontainer_id || empty($attender->displaycontainer_id)) {
            // no need to increase sequence
            return;
        }
        Tinebase_Container::getInstance()->increaseContentSequence($attender->displaycontainer_id, $action, $room->getId());
    }
    
    /**
     * updates an attender
     * 
     * @param Webconference_Model_Attender  $attender
     * @param Webconference_Model_Attender  $currentAttender
     * @param Webconference_Model_Event     $room
     * @param Tinebase_Model_Container $webconference
     */
    protected function _updateAttender($attender, $currentAttender, $room, $webconference = NULL)
    {
        $userAccountId = $currentAttender->getUserAccountId();
        if (Tinebase_Core::isLogLevel(Zend_Log::TRACE)) Tinebase_Core::getLogger()->trace(__METHOD__ . '::' . __LINE__ . " Updating attender: " . print_r($attender->toArray(), TRUE));
        // update display webconference if attender has/is a useraccount
        if ($userAccountId) {
            if ($webconference->type == Tinebase_Model_Container::TYPE_PERSONAL && Tinebase_Container::getInstance()->hasGrant($userAccountId, $webconference, Tinebase_Model_Grants::GRANT_ADMIN)) 
	    {
                // if attender has admin grant to personal physical container, this phys. cal also gets displ. cal
                $attender->displaycontainer_id = $webconference->getId();
            } else if ($userAccountId == Tinebase_Core::getUser()->getId() && Tinebase_Container::getInstance()->hasGrant($userAccountId, $attender->displaycontainer_id, Tinebase_Model_Grants::GRANT_ADMIN)) 
	    {
                // allow user to set his own displ. cal
                $attender->displaycontainer_id = $attender->displaycontainer_id;
            } else {
                $attender->displaycontainer_id = $currentAttender->displaycontainer_id;
            }
        }
        
        Tinebase_Timemachine_ModificationLog::getInstance()->setRecordMetaData($attender, 'update', $currentAttender);
        Tinebase_Timemachine_ModificationLog::getInstance()->writeModLog($attender, $currentAttender, get_class($attender), $this->_getBackendType(), $attender->getId());
        $this->_backend->updateAttendee($attender);   
        if ($attender->displaycontainer_id !== $currentAttender->displaycontainer_id) 
	{
            $this->_increaseDisplayContainerContentSequence($currentAttender, $room, Tinebase_Model_ContainerContent::ACTION_DELETE);
            $this->_increaseDisplayContainerContentSequence($attender, $room, Tinebase_Model_ContainerContent::ACTION_CREATE);
        } else {
            $this->_increaseDisplayContainerContentSequence($attender, $room);
        }
    }

    /**
     * redefine required grants for get actions
     * 
     * @param Tinebase_Model_Filter_FilterGroup $_filter
     * @param string $_action get|update
     */
    public function checkFilterACL(Tinebase_Model_Filter_FilterGroup $_filter, $_action = 'get')
    {
        $hasGrantsFilter = FALSE;
        foreach($_filter->getAclFilters() as $aclFilter) {
            if ($aclFilter instanceof Webconference_Model_GrantFilter) {
                $hasGrantsFilter = TRUE;
                break;
            }
        }
        
        if (! $hasGrantsFilter) {
            // force a grant filter
            // NOTE: actual grants are set via setRequiredGrants later
            $grantsFilter = $_filter->createFilter('grants', 'in', '@setRequiredGrants');
            $_filter->addFilter($grantsFilter);
        }
        
        parent::checkFilterACL($_filter, $_action);
        
        if ($_action == 'get') {
            $_filter->setRequiredGrants(array(
                Tinebase_Model_Grants::GRANT_READ,
                Tinebase_Model_Grants::GRANT_ADMIN,
            ));
        }
    }
    
    /**
     * check grant for action (CRUD)
     *
     * @param Tinebase_Record_Interface $_record
     * @param string $_action
     * @param boolean $_throw
     * @param string $_errorMessage
     * @param Tinebase_Record_Interface $_oldRecord
     * @return boolean
     * @throws Tinebase_Exception_AccessDenied
     * 
     * @todo use this function in other create + update functions
     * @todo invent concept for simple adding of grants (plugins?) 
     */
    protected function _checkGrant($_record, $_action, $_throw = TRUE, $_errorMessage = 'No Permission.', $_oldRecord = NULL)
    {
        $key = __METHOD__ . (string) $_record . $action . $_errorMessage;
        if (Tinebase_Session_Storage_Acl::getInstance()->has($key)){
            return Tinebase_Session_Storage_Acl::getInstance()->get($key);
        }

        if (    !$this->_doContainerACLChecks 
            // admin grant includes all others
            ||  ($_record->container_id && Tinebase_Core::getUser()->hasGrant($_record->container_id, Tinebase_Model_Grants::GRANT_ADMIN))) {
            return TRUE;
        }

        switch ($_action) {
            case 'get':
                // NOTE: free/busy is not a read grant!
                $hasGrant = $_record->hasGrant(Tinebase_Model_Grants::GRANT_READ);
                break;
            case 'create':
                $hasGrant = Tinebase_Core::getUser()->hasGrant($_record->container_id, Tinebase_Model_Grants::GRANT_ADD);
                break;
            case 'update':
                $hasGrant = (bool) $_oldRecord->hasGrant(Tinebase_Model_Grants::GRANT_EDIT);
                
                if ($_oldRecord->container_id != $_record->container_id) {
                    $hasGrant &= Tinebase_Core::getUser()->hasGrant($_record->container_id, Tinebase_Model_Grants::GRANT_ADD)
                                 && $_oldRecord->hasGrant(Tinebase_Model_Grants::GRANT_DELETE);
                }
                break;
            case 'delete':
                $hasGrant = (bool) $_record->hasGrant(Tinebase_Model_Grants::GRANT_DELETE);
                break;
            case 'sync':
                $hasGrant = (bool) $_record->hasGrant(Tinebase_Model_Grants::GRANT_SYNC);
                break;
            case 'export':
                $hasGrant = (bool) $_record->hasGrant(Tinebase_Model_Grants::GRANT_EXPORT);
                break;
        }
        
        if (!$hasGrant) {
            if ($_throw) {
                throw new Tinebase_Exception_AccessDenied($_errorMessage);
            } else {
                if (Tinebase_Core::isLogLevel(Zend_Log::DEBUG)) Tinebase_Core::getLogger()->debug(__METHOD__ . '::' . __LINE__ . 'No permissions to ' . $_action . ' in container ' . $_record->container_id);
            }
        }
        
        Tinebase_Session_Storage_Acl::getInstance()->set($key, $hasGrant);

        return $hasGrant;
    }
    
    /**
     * touches (sets seq and last_modified_time) given event
     * 
     * @param  $_event
     * @return void
     */
    protected function _touch($_event, $_setModifier = FALSE)
    {
        $_event->last_modified_time = Tinebase_DateTime::now();
        //$_event->last_modified_time = Tinebase_DateTime::now()->get(Tinebase_Record_Abstract::ISO8601LONG);
        $_event->seq = (int)$_event->seq + 1;
        if ($_setModifier) {
            $_event->last_modified_by = Tinebase_Core::getUser()->getId();
        }
        
        
        $this->_backend->update($_event);
    }

    /**
     * Get IDs from Room Name
     * @param String Room Name
     * @return array of ids from room
     */
    public function getRoomName($roomName)
    {
        return $this->_backend->getRoomFromName($roomName);
    }

}
