<?php
/**
 * Sql Webconference 
 * 
 * @package     Webconference
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Cornelius Weiss <c.weiss@metaways.de>
 * @copyright   Copyright (c) 2009 Metaways Infosystems GmbH (http://www.metaways.de)
 */

/**
 * native tine 2.0 events sql backend attendee class
 *
 * @package Webconference
 */
class Webconference_Backend_Sql_Attendee extends Tinebase_Backend_Sql_Abstract
{
    /**
     * event foreign key column
     */
    const FOREIGNKEY_ROOM = 'wconf_room_id';
    
    /**
     * Table name without prefix
     *
     * @var string
     */
    protected $_tableName = 'wconf_attendee';
    
    /**
     * Model name
     *
     * @var string
     */
    protected $_modelName = 'Webconference_Model_Attender';
    
    /**
     * if modlog is active, we add 'is_deleted = 0' to select object in _getSelect()
     *
     * @var boolean
     */
    protected $_modlogActive = TRUE;
}
