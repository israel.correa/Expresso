/*
 * Tine 2.0
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @copyright   Copyright (c) 2007-2010 Metaways Infosystems GmbH (http://www.metaways.de)
 */
Ext.ns('Tine.Webconference.Model');



/**
 * @namespace   Tine.Webconference.Model
 * @class       Tine.Webconference.Model.Invite
 * @extends     Tine.Tinebase.data.Record
 * Invite Record Definition
 */
Tine.Webconference.Model.Invite = Tine.Tinebase.data.Record.create([
    {name: 'id'},
    {name: 'url'},
    {name: 'roomName'},
    {name: 'roomId'},
    {name: 'roomTitle'},
    {name: 'moderator'},
    {name: 'createdBy'},
    {name: 'to'}
    
], {
    appName: 'Webconference',
    modelName: 'Invite',
    idProperty: 'id'
});

/**
 * @namespace   Tine.Webconference.Model
 * @class       Tine.Webconference.Model.Config
 * @extends     Tine.Tinebase.data.Record
 * Config Record Definition
 */
Tine.Webconference.Model.Config = Tine.Tinebase.data.Record.create(Tine.Tinebase.Model.genericFields.concat([
    { name: 'id' },
    { name: 'url' },
    { name: 'salt' },
    { name: 'limit_room' },
    { name: 'description' }
]), {
    appName: 'Webconference',
    modelName: 'Config',
    idProperty: 'id',
    titleProperty: 'description',
    
    recordName: 'server',
    recordsName: 'servers',
    
    containerName: 'server list',
    containersName: 'server lists'
});

Tine.Webconference.Model.Config.getFilterModel = function() {
    var app = Tine.Tinebase.appMgr.get('Webconference');
    
    return [
        {filtertype: 'tine.widget.container.filtermodel', app: app, recordClass: Tine.Webconference.Model.Config}
    ];
};

Tine.Webconference.Model.Config.getDefaultData = function() { 
    return {};
};

/**
 * Config record backend
 */
Tine.Webconference.configRecordBackend = new Tine.Tinebase.data.RecordProxy({
    appName: 'Webconference',
    modelName: 'Config',
    recordClass: Tine.Webconference.Model.Config
});


/**
 * @namespace   Tine.Webconference.Model
 * @class       Tine.Webconference.Model.Room
 * @extends     Tine.Tinebase.data.Record
 * Room Record Definition
 */
Tine.Webconference.Model.Room = Tine.Tinebase.data.Record.create(Tine.Tinebase.Model.genericFields.concat([
    { name: 'id' },
    { name: 'title' },
    { name: 'room_name' },
    { name: 'status', type: 'keyField', keyFieldConfigName: 'roomStatus'},
    { name: 'wconf_config_id'},
    { name: 'organizer' },
    { name: 'attendee' },
    { name: 'notes'},
    {name: 'readGrant'   , type: 'bool'},
    {name: 'editGrant'   , type: 'bool'},
    {name: 'deleteGrant' , type: 'bool'},
    {name: 'editGrant'   , type: 'bool'}
]), {
    appName: 'Webconference',
    modelName: 'Room',
    idProperty: 'id',
    titleProperty: 'title',
    
    recordName: 'Room',
    recordsName: 'Rooms',
    containerProperty: 'container_id',

    containerName: 'Webconference',
    containersName: 'Webconferences',
    
    /**
     * returns displaycontainer with orignialcontainer as fallback
     * 
     * @return {Array}
     */
    getDisplayContainer: function() {
        var displayContainer = this.get('container_id');
        var currentAccountId = Tine.Tinebase.registry.get('currentAccount').accountId;
        
        var attendeeStore = this.getAttendeeStore();
        attendeeStore.each(function(attender) {
            var userAccountId = attender.getUserAccountId();
            if (userAccountId == currentAccountId) {
                var container = attender.get('displaycontainer_id');
                if (container) {
                    displayContainer = container;
                }
                return false;
            }
        }, this);
        
        return displayContainer;
    },
    /**
     * returns store of attender objects
     * 
     * @param  {Array} attendeeData
     * @return {Ext.data.Store}
     */
    getAttendeeStore: function() {
        return Tine.Webconference.Model.Attender.getAttendeeStore(this.get('attendee'));
    },
    
    /**
     * returns attender record of current account if exists, else false
     */
    getMyAttenderRecord: function() {
        var attendeeStore = this.getAttendeeStore();
        return Tine.Webconference.Model.Attender.getAttendeeStore.getMyAttenderRecord(attendeeStore);
    }    
});

// register webconference filters in addressbook
Tine.widgets.grid.ForeignRecordFilter.OperatorRegistry.register('Addressbook', 'Contact', {
    foreignRecordClass: 'Webconference.Room',
    linkType: 'foreignId', 
    filterName: 'ContactAttendeeFilter',
    label: 'Room (as attendee)'
});

Tine.widgets.grid.ForeignRecordFilter.OperatorRegistry.register('Addressbook', 'Contact', {
    foreignRecordClass: 'Webconference.Room',
    linkType: 'foreignId', 
    filterName: 'ContactOrganizerFilter',
    label: 'Room (as organizer)'
});


/**
 * Room record backend
 */
Tine.Webconference.roomRecordBackend = new Tine.Tinebase.data.RecordProxy({
    appName: 'Webconference',
    modelName: 'Room',
    recordClass: Tine.Webconference.Model.Room,
    /**
     * exception handler for this proxy
     *
     * @param {Tine.Exception} exception
     */
    handleRequestException: function(exception) {
        Tine.Webconference.handleRequestException(exception);
    }
});

Tine.Webconference.Model.Room.getFilterModel = function() {
    var app = Tine.Tinebase.appMgr.get('Webconference');
    return [
        {label: _('Quick Search'), field: 'query', operators: ['contains']},
        {label: app.i18n._('title'), field: 'title' },
        {label: app.i18n._('creation_time'), field: 'creation_time', valueType: 'date', operators: ['within', 'before', 'after']},
    {
            label: app.i18n._('status'),
            field: 'status',
            filtertype: 'tine.widget.keyfield.filter',
            app: app, 
            keyfieldName: 'roomStatus'
        },
        {filtertype: 'tine.widget.container.filtermodel', app: app, recordClass: Tine.Webconference.Model.Room, defaultValue: {path: Tine.Tinebase.container.getMyNodePath()}},
        {filtertype: 'webconference.attendee'},
        {
            label: app.i18n._('Attendee Role'),
            field: 'attender_role',
            filtertype: 'tine.widget.keyfield.filter', 
            app: app, 
            keyfieldName: 'wconfRoles'
        },
        {filtertype: 'addressbook.contact', field: 'organizer', label: app.i18n._('Organizer')},
    ];
};

/**
 * @namespace Tine.Webconference.Model
 * @class Tine.Webconference.Model.Attender
 * @extends Tine.Tinebase.data.Record
 * Attender Record Definition
 */
Tine.Webconference.Model.Attender = Tine.Tinebase.data.Record.create([
    {name: 'id'},
    {name: 'wconf_room_id'},
    {name: 'room_url'},
    {name: 'user_id', sortType: Tine.Tinebase.common.accountSortType },
    {name: 'user_type'},
    {name: 'role', type: 'keyField', keyFieldConfigName: 'wconfRoles'},
    {name: 'call_date', type: 'date', dateFormat: Date.patterns.ISO8601Long},
    {name: 'displaycontainer_id'}
], {
    appName: 'Webconference',
    modelName: 'Attender',
    idProperty: 'id',
    titleProperty: 'name',
    recordName: 'Attender',
    recordsName: 'Attendee',
    containerProperty: 'wconf_room_id',
    containerName: 'Room',
    containersName: 'Rooms',
    
    /**
     * returns account_id if attender is/has a user account
     * 
     * @return {String}
     */
    getUserAccountId: function() {
        var user_type = this.get('user_type');
        if (user_type == 'user' || user_type == 'groupmember') {
            var user_id = this.get('user_id');
            if (! user_id) {
                return null;
            }
            
            // we expect user_id to be a user or contact object or record
            if (typeof user_id.get == 'function') {
                if (user_id.get('contact_id')) {
                    // user_id is a account record
                    return user_id.get('accountId');
                } else {
                    // user_id is a contact record
                    return user_id.get('account_id');
                }
            } else if (user_id.hasOwnProperty('contact_id')) {
                // user_id contains account data
                return user_id.accountId;
            } else if (user_id.hasOwnProperty('account_id')) {
                // user_id contains contact data
                return user_id.account_id;
            }
            
            // this might happen if contact resolved, due to right restrictions
            return user_id;
        }
        return null;
    },
    
    /**
     * returns id of attender of any kind
     */
    getUserId: function() {
        var user_id = this.get('user_id');
        if (! user_id) {
            return null;
        }
        var userData = (typeof user_id.get == 'function') ? user_id.data : user_id;
        if (!userData) {
            return null;
        }
        if (typeof userData != 'object') {
            return userData;
        }
        
        switch (this.get('user_type')) {
            case 'user':
            case 'groupmember':
            case 'memberOf':
                if (userData.hasOwnProperty('contact_id')) {
                    // userData contains account
                    return userData.contact_id;
                } else if (userData.hasOwnProperty('account_id')) {
                    // userData contains contact
                    return userData.id;
                }
                break;
            default:
                return userData.id
                break;
        }
    }
});
/**
 * @namespace Tine.Webconference.Model
 * 
 * get default data for a new attender
 *  
 * @return {Object} default data
 * @static
 */ 
Tine.Webconference.Model.Attender.getDefaultData = function() {
    return {
        user_type: 'user',
        role: 'ATTENDEE',
        quantity: 1
    };
};

/**
 * @namespace Tine.Webconference.Model
 * 
 * creates store of attender objects
 * 
 * @param  {Array} attendeeData
 * @return {Ext.data.Store}
 * @static
 */ 
Tine.Webconference.Model.Attender.getAttendeeStore = function(attendeeData) {
    var attendeeStore = new Ext.data.SimpleStore({
        fields: Tine.Webconference.Model.Attender.getFieldDefinitions(),
        sortInfo: {field: 'user_id', direction: 'ASC'}
    });
    
    Ext.each(attendeeData, function(attender) {
        if (attender) {
            var record = new Tine.Webconference.Model.Attender(attender, attender.id && Ext.isString(attender.id) ? attender.id : Ext.id());
            attendeeStore.addSorted(record);
        }
    });
    return attendeeStore;
};

/**
 * returns attender record of current account if exists, else false
 * @static
 */
Tine.Webconference.Model.Attender.getAttendeeStore.getMyAttenderRecord = function(attendeeStore) {
    var currentAccountId = Tine.Tinebase.registry.get('currentAccount').accountId;
    var myRecord = false;
    attendeeStore.each(function(attender) {
        var userAccountId = attender.getUserAccountId();
        if (userAccountId == currentAccountId) {
            myRecord = attender;
            return false;
        }
    }, this);

    return myRecord;
}

/**
 * returns attendee record of given attendee if exists, else false
 * @static
 */
Tine.Webconference.Model.Attender.getAttendeeStore.getAttenderRecord = function(attendeeStore, attendee) {
    var attendeeRecord = false;
    attendeeStore.each(function(r) {
        if (r.get('user_type') == attendee.get('user_type') && r.getUserId() == attendee.getUserId()) {
            attendeeRecord = r;
            return false;
        }
    }, this);
    return attendeeRecord;
}
    
/**
 * @namespace Tine.Webconference.Model
 * 
 * get default data for a new room
 * @todo: attendee according to webconference selection
 *  
 * @return {Object} default data
 * @static
 */ 
Tine.Webconference.Model.Room.getDefaultData = function() {
    var app = Tine.Tinebase.appMgr.get('Webconference'),
        container = app.getMainScreen().getWestPanel().getContainerTreePanel().getDefaultContainer(),
        userContact = Tine.Tinebase.registry.get('userContact'),
        containerOwner = container.owner,
        defaultAttender = containerOwner ? ( (userContact.accountId !== containerOwner.accountId) ? containerOwner : userContact ) : userContact;
    var data = {
        room_name: 'room_name',
        status: 'A',
        protocol: 'https',
        container_id: container,
        editGrant: true,
        organizer: Tine.Tinebase.registry.get('userContact'),
        attendee: [
            Ext.apply(Tine.Webconference.Model.Attender.getDefaultData(), {
                user_type: 'user',
                user_id: defaultAttender,
                role: 'MODERATOR'
            })
        ]
    };
    return data;
};

Tine.widgets.grid.ForeignRecordFilter.OperatorRegistry.register('Addressbook', 'Contact', {
    foreignRecordClass: 'Webconference.Room',
    linkType: 'foreignId', 
    filterName: 'ContactOrganizerFilter',
    label: 'Room (as organizer)'
});

