/*
 * Tine 2.0
 * 
 * @package     Webconference
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2009 Metaways Infosystems GmbH (http://www.metaways.de)
 *
 */
 
Ext.namespace('Tine.Webconference');

/**
 * @namespace   Tine.Webconference
 * @class       Tine.Webconference.RoomEditDialog
 * @extends     Tine.widgets.dialog.EditDialog
 * 
 * <p>Webconference Edit Dialog</p>
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar De Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2009 Metaways Infosystems GmbH (http://www.metaways.de)
 * 
 * @param       {Object} config
 * @constructor
 * Create a new Tine.Webconference.RoomEditDialog
 */
 Tine.Webconference.RoomEditDialog = Ext.extend(Tine.widgets.dialog.EditDialog, {
     
    /**
     * @private
     */
    containerId: -1,
    
    labelAlign: 'side',

    windowNamePrefix: 'RoomEditWindow_',
    appName: 'Webconference',
    recordClass: Tine.Webconference.Model.Room,
    recordProxy: Tine.Webconference.roomRecordBackend,
    evalGrants: false,
    showContainerSelector: false,   
    mode: 'local',   
    
    onResize: function() {
        Tine.Webconference.RoomEditDialog.superclass.onResize.apply(this, arguments);
        this.setTabHeight.defer(100, this);
    },
    
    setTabHeight: function() {
        var roomTab = this.items.first().items.first();
        var centerPanel = roomTab.items.first();
        var tabPanel = centerPanel.items.last();
        tabPanel.setHeight(centerPanel.getEl().getBottom() - tabPanel.getEl().getTop());
    },
    	
    /**
     * returns dialog
     * 
     * NOTE: when this method gets called, all initalisation is done.
     * @return {Object} components this.itmes definition
     */
    getFormItems: function() {       
        return {
            xtype: 'tabpanel',
            border: false,
            plugins: [{
                ptype : 'ux.tabpanelkeyplugin'
            }],
            plain:true,
            activeTab: 0,
            border: false,
            items:[{
                title: this.app.i18n.n_('Room', 'Rooms', 1),
                border: false,
                frame: true,
                layout: 'border',
                items: [{
                    region: 'center',
                    layout: 'hfit',
                    border: false,
                    items: [{
                        layout: 'hbox',
                        items: [{
                            margins: '5',
                            width: 100,
                            xtype: 'label',
                            text: this.app.i18n._('Title')
                        }, {
                            flex: 1,
                            xtype:'textfield',
                            name: 'title',
			
                            listeners: {render: function(field){field.focus(false, 250);}},
                            allowBlank: false,
                            requiredGrant: 'editGrant',
                            maxLength: 50
                        }]
                    },{
                        layout: 'hbox',
                        items: [{
                            margins: '5',
                            width: 100,
                            xtype: 'label',
                            text: this.app.i18n._('View')
                        }, Ext.apply(this.perspectiveCombo, {
                            flex: 1
                        })]
                    },{
                        layout: 'hbox',
                        height: 40,
                        layoutConfig: {
                            align : 'stretch',
                            pack  : 'start'
                        },
                        items: [{
                            flex: 1,
                            xtype: 'fieldset',
                            layout: 'hfit',
                            margins: '0 5 0 0',
                            title: this.app.i18n._('Details'),
                            items: [{
                                xtype: 'columnform',
                                labelAlign: 'side',
                                labelWidth: 100,
                                formDefaults: {
                                    xtype:'textfield',
                                    anchor: '100%',
                                    labelSeparator: '',
                                    columnWidth: .7
                                },
                                items: [
				  [ this.containerSelectCombo = new Tine.widgets.container.selectionComboBox({
                                    columnWidth: 1,
                                    id: this.app.appName + 'EditDialogContainerSelector' + Ext.id(),
                                    fieldLabel: _('Saved in'),
                                    ref: '../../../../../../../../containerSelect',
                                    name: this.recordClass.getMeta('containerProperty'),
                                    recordClass: this.recordClass,
                                    containerName: this.app.i18n.n_hidden(this.recordClass.getMeta('containerName'), this.recordClass.getMeta('containersName'), 1),
                                    containersName: this.app.i18n._hidden(this.recordClass.getMeta('containersName')),
                                    appName: this.app.appName,
                                    requiredGrant: this.record.data.id ? ['editGrant'] : ['addGrant'],
                                    disabled: true
                                }), Ext.apply(this.perspectiveCombo.getAttendeeContainerField(), {
                                    columnWidth: 1
                                })]]
                            }]
                        }]
                    }, {
                        xtype: 'tabpanel',
                        deferredRender: false,
                        activeTab: 0,
                        border: false,
                        height: 235,
                        form: true,
                        items: [
                            this.attendeeGridPanel
                        ]
                    }]
                }]
            }, new Tine.widgets.activities.ActivitiesTabPanel({
                app: this.appName,
                record_id: (this.record) ? this.record.id : '',
                record_model: this.appName + '_Model_' + this.recordClass.getMeta('modelName')
            })]
        };
    },    
    
    initComponent: function() {
        var organizerCombo;
        this.attendeeGridPanel = new Tine.Webconference.AttendeeGridPanel({
            bbar: [{
                xtype: 'label',
                html: Tine.Tinebase.appMgr.get('Webconference').i18n._('Organizer') + "&nbsp;"
            }, organizerCombo = Tine.widgets.form.RecordPickerManager.get('Addressbook', 'Contact', {
                width: 300,
                name: 'organizer',
                userOnly: true,
                getValue: function() {
                    var id = Tine.Addressbook.SearchCombo.prototype.getValue.apply(this, arguments),
                        record = this.store.getById(id);
                    return record ? record.data : id;
                }
            })]
        });
        
        // auto location
        this.attendeeGridPanel.on('afteredit', function(o) {
            if (o.field == 'user_id'
                && o.record.get('user_id')
                && o.record.get('user_id').is_location
            ) {
                this.getForm().findField('location').setValue(
                    this.attendeeGridPanel.renderAttenderResourceName(o.record.get('user_id'))
                );
            }
        }, this);
        
        this.on('render', function() {this.getForm().add(organizerCombo);}, this);
        this.attendeeStore = this.attendeeGridPanel.getStore();
        this.perspectiveCombo = new Tine.Webconference.PerspectiveCombo({
            editDialog: this
        });
        Tine.Webconference.RoomEditDialog.superclass.initComponent.call(this);
    },
    
    /**
     * copy record
     * 
     * TODO change attender status?
     */
    doCopyRecord: function() {
        Tine.Webconference.RoomEditDialog.superclass.doCopyRecord.call(this);
        
        // remove attender ids
        Ext.each(this.record.data.attendee, function(attender) {
            delete attender.id;
        }, this);
        
        // Webconference is the only app with record based grants -> user gets edit grant for all fields when copying
        this.record.set('editGrant', true);
        Tine.log.debug('Tine.Webconference.RoomEditDialog::doCopyRecord() -> record:');
    },

    /**
     * handles attendee
     */ 
    handleRelations: function() {
        if(this.record.data.hasOwnProperty('relations')) {
            var addAttendee = [],
                addRelationConfig = {};
                
            Ext.each(this.plugins, function(plugin) {
                if(plugin.ptype == 'addrelations_edit_dialog') {
                    addRelationConfig = plugin.relationConfig;
                }
            }, this);

            Ext.each(this.record.data.relations, function(relation) {
                var add = true;
                Ext.each(this.record.data.attendee, function(attender) {
                    if(attender.user_id.id == relation.related_record.id) {
                        add = false;
                        return false;
                    }
                }, this);
                
                if(add) {
                    var att = new Tine.Webconference.Model.Attender(Ext.apply(Tine.Webconference.Model.Attender.getDefaultData(), addRelationConfig), 'new-' + Ext.id());
                    att.set('user_id', relation.related_record);
                    addAttendee.push(att.data);
                }
            }, this);
            var att = this.record.get('id') ? this.record.data.attendee.concat(addAttendee) : addAttendee;
            this.record.set('attendee', att);
            delete this.record.data.relations;
        }
    },

    
    /**
     * is called after all subpanels have been loaded
     */ 
    onAfterRecordLoad: function() {
        this.handleRelations();
        Tine.Webconference.RoomEditDialog.superclass.onAfterRecordLoad.call(this);
        
        this.attendeeGridPanel.onRecordLoad(this.record);
	
        // apply grants
        if (! this.record.get('editGrant')) {
            this.getForm().items.each(function(f){
                if(f.isFormField && f.requiredGrant !== undefined){
                    f.setDisabled(! this.record.get(f.requiredGrant));
                }
            }, this);
        }
        
        this.perspectiveCombo.loadPerspective();
        // disable container selection combo if user has no right to edit
        this.containerSelect.setDisabled.defer(20, this.containerSelect, [(! this.record.get('editGrant'))]);
    },

    /**
     * generic request exception handler
     * 
     * @param {Object} exception
     */
    onRequestFailed: function(exception) {
        this.saving = false;
        if (exception.code === 629) {
            this.onDuplicateException.apply(this, arguments);
        } else if (exception.code === 404) {
            Ext.Msg.show({
               title:   this.app.i18n._('Save Room Error'),
               msg:     this.app.i18n._('Could not save Room.') + (exception.message ? ' (' + exception.message + ')' : ''),
               icon:    Ext.MessageBox.ERROR,
               buttons: Ext.Msg.OK
            });
	} 
	else {
            Tine.Tinebase.ExceptionHandler.handleRequestException(exception);
        }
        this.loadMask.hide();
    },

    onRecordUpdate: function() {     
        Tine.Webconference.RoomEditDialog.superclass.onRecordUpdate.apply(this, arguments);
        this.attendeeGridPanel.onRecordUpdate(this.record);
        this.perspectiveCombo.updatePerspective();
    }    
});

/**
 * Room Edit Popup
 * 
 * @param   {Object} config
 * @return  {Ext.ux.Window}
 */
Tine.Webconference.RoomEditDialog.openWindow = function (config) {
    var id = (config.record && config.record.id) ? config.record.id : 0;
    var window = Tine.WindowFactory.getWindow({
        width: 800,
        height: 600,
        name: Tine.Webconference.RoomEditDialog.prototype.windowNamePrefix + id,
        contentPanelConstructor: 'Tine.Webconference.RoomEditDialog',
        contentPanelConstructorConfig: config
    });
    return window;
};