/* 
 * Tine 2.0
 * 
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Edgar de Lucca <edgar.lucca@serpro.gov.br>
 * @copyright   Copyright (c) 2012 Metaways Infosystems GmbH (http://www.metaways.de)
 */
 
Ext.ns('Tine.Webconference');

/**
 * @class     Tine.Webconference.WebconferenceDetailsPanel
 * @namespace Webconference
 * @extends   Tine.widgets.grid.DetailsPanel
 * @author    Edgar de Lucca <edgar.lucca@serpro.gov.br>
 */

Tine.Webconference.RoomDetailsPanel = Ext.extend(Tine.widgets.grid.DetailsPanel, {
    app: null,
    
    /**
     * @cfg {Number} defaultHeight
     * default Heights
     */
    defaultHeight: 200,
    
    initComponent: function() {
        this.summaryRecord = new Ext.data.Record({
            count: 0
        }, 0);
        Tine.Webconference.RoomDetailsPanel.superclass.initComponent.call(this);
    },
    
    /**
     * default panel w.o. data
     * 
     * @return {Ext.ux.display.DisplayPanel}
     */
    getDefaultInfosPanel: function() {
        return this.getMultiRecordsPanel();
    },
    
    /**
     * get panel for multi selection aggregates/information
     * 
     * @return {Ext.Panel}
     */
    
    getMultiRecordsPanel: function() {
        if (! this.multiRecordsPanel) {
            this.multiRecordsPanel = new Ext.ux.display.DisplayPanel(
                this.wrapPanel([{
                    xtype: 'ux.displayfield',
                    name: 'count',
                    fieldLabel: this.app.i18n._('Total Webconference Records')
                }], 160)
            );
        }
        return this.multiRecordsPanel;
    },
    
    /**
     * renders attendee names
     * 
     * @param {Array} attendeeData
     * @return {String}
     */
    attendeeRenderer: function(attendeeData) {
        if (! attendeeData) {
            return _('No Information');
        }
        var attendeeStore = Tine.Webconference.Model.Attender.getAttendeeStore(attendeeData);
        var a = [];
        attendeeStore.each(function(attender) {
            var name = Tine.Webconference.AttendeeGridPanel.prototype.renderAttenderName.call(Tine.Webconference.AttendeeGridPanel.prototype, attender.get('user_id'), false, attender),
            role = Tine.Tinebase.widgets.keyfield.Renderer.render('Webconference', 'wconfRoles', attender.get('role'));
            a.push(name + ' (' + role + ')');
        });
        
        return a.join("\n");
    },
   
    /**
     * main webconference details panel
     * 
     * @return {Ext.ux.display.DisplayPanel}
     */
    getSingleRecordPanel: function() {
        if (! this.singleRecordPanel) {
            this.singleRecordPanel = new Ext.ux.display.DisplayPanel(
                this.wrapPanel([{
                    xtype: 'ux.displayfield',
                    name: 'attendee',
                    nl2br: true,
                    htmlEncode: false,
                    fieldLabel: this.app.i18n._('Attendee'),
                    renderer: this.attendeeRenderer
                }], 80)
            );
        }
        return this.singleRecordPanel;
    },

    /**
     * update example record details panel
     * 
     * @param {Tine.Tinebase.data.Record} record
     * @param {Mixed} body
     */
    updateDetails: function(record, body) {
        this.getSingleRecordPanel().loadRecord.defer(100, this.getSingleRecordPanel(), [record]);
    },
    /**
     * show default template
     * 
     * @param {Mixed} body
     */
    showDefault: function(body) {
        this.showMulti(this.grid.getSelectionModel());
    },

    /**
     * show template for multiple rows
     * 
     * @param {Ext.grid.RowSelectionModel} sm
     * @param {Mixed} body
     */
    showMulti: function(sm, body) {
        if (sm.getCount() === 0) {
            var count = this.grid.store.proxy.jsonReader.jsonData.totalcount;
        } else {
            var count = sm.getCount();
        }
        this.summaryRecord.set('count', count);
        this.getMultiRecordsPanel().loadRecord.defer(100, this.getMultiRecordsPanel(), [this.summaryRecord]);
    }

});


