Ext.namespace('Tine.Webconference');

var WebconferenceStatus = {
    'A' : 'Active',
    'E' : 'Expired'
};

var WebconferenceRole = {
    'MODERATOR' : 'Moderator',
    'ATTENDEE' : 'Attendee'
};

Tine.Webconference.RoomGridPanel = Ext.extend(Tine.widgets.grid.GridPanel, {
    app: 'webconference',
    recordClass: Tine.Webconference.Model.Room,
    hasFavoritesPanel: true,
    action_deleteRecord: false,

    /**
     * id of current account
     *
     * @type Number
     * @property currentAccountId
     */
    currentAccountId: null,

    conferenceDialog: null,

    /**
     * @private grid cfg
     */
    defaultSortInfo: {
        field: 'title',
        dir: 'ASC'
    },

    /**
     * @private
     */
    initComponent: function() {
        this.initDetailsPanel();
        this.currentAccountId = Tine.Tinebase.registry.get('currentAccount').accountId;
        this.recordProxy = Tine.Webconference.roomRecordBackend;
        this.gridConfig.cm = this.getColumnModel();

        this.defaultFilters = [
            {field: 'status', operator: 'in', value: 'A'},
            {field: 'container_id', operator: 'equals', value: {path: '/personal/' + Tine.Tinebase.registry.get('currentAccount').accountId}}
        ];
        Tine.Webconference.RoomGridPanel.superclass.initComponent.call(this);
    },

    roleRenderer: function(role,metadata) {
        metadata.attr = 'ext:qtip="' + Tine.Tinebase.appMgr.get('Webconference').i18n._('Double Click to join') + '"';
        return this.app.i18n._(WebconferenceRole[role]);
    },

    statusRenderer: function(status, metadata) {
        metadata.attr = 'ext:qtip="' + Tine.Tinebase.appMgr.get('Webconference').i18n._('Double Click to join') + '"';
        return this.app.i18n._(WebconferenceStatus[status]);
        return status;
    },

    tooltipRenderer : function(value, metadata) {
        metadata.attr = 'ext:qtip="' + Tine.Tinebase.appMgr.get('Webconference').i18n._('Double Click to join') + '"';
        return value;
    },

    getColumnModel: function(){
        return new Ext.grid.ColumnModel({
            defaults: {
                sortable: true,
                resizable: true
            },
            columns: [
                {
                    id: 'title',
                    header: this.app.i18n._("Title"),
                    width: 350,
                    dataIndex: 'title',
                    renderer: this.tooltipRenderer.createDelegate(this)
                }, {
                    id: 'room_name',
                    header: this.app.i18n._("Room name"),
                    width: 150,
                    dataIndex: 'room_name',
                    hidden: true,
                    renderer: this.tooltipRenderer.createDelegate(this)
                }, {
                    id: 'status',
                    header: this.app.i18n._("Status"),
                    width: 100,
                    dataIndex: 'status',
                    renderer: this.statusRenderer.createDelegate(this)
                }, {
                    id: 'creation_time',
                    header: this.app.i18n._("Creation Time"),
                    width: 100,
                    dataIndex: 'creation_time',
                    renderer: Tine.Tinebase.common.dateTimeRenderer
                }
            ]
        });
    },

    initActions: function(){
        this.actions_startConference = new Ext.Action({
            text : this.app.i18n._('Start Conference'),
            requiredGrant: 'readGrant',
            tooltip : this.app.i18n._('Initiates a new conference'),
            handler : this.onStartConference,
            iconCls : 'action_start_conference',
            allowMultiple: false,
            disabled: true,
            scope : this
        });

        this.actionUpdater = new Tine.widgets.ActionUpdater({
            actions: this.recordActions,
            grantsProperty: false,
            containerProperty: false
        });

        this.actionUpdater.addActions([
            this.actions_startConference
        ]);
        Tine.Webconference.RoomGridPanel.superclass.initActions.call(this);
    },

    getActionToolbarItems: function() {
        return [
            Ext.apply(new Ext.Button(this.actions_startConference), {
            scale: 'medium',
            rowspan: 2,
            iconAlign: 'top'
            })
        ];
    },

    onStartConference: function(btn){
        var selectedRows = this.grid.getSelectionModel().getSelections();
        if (selectedRows.length === 0) {
            return;
        }
        this.initConference(selectedRows[0]);
    },

    onRowDblClick: function(grid, row, e) {
        var selModel = grid.getSelectionModel();
        if (!selModel.isSelected(row)) {
            this.updateOnSelectionChange = this.updateDetailsPanelOnCtxMenu;
            selModel.selectRow(row);
        }
        var store = selModel.getSelected();
        this.initConference(store);
    },

    isMeetingActive: function (roomName, configId) {
        Tine.Webconference.isMeetingActive(roomName, configId, function (response) {
            return response;
        });
    },

    initConference: function(store){
        var myAttenderRecord = store.getMyAttenderRecord().data;
        if (myAttenderRecord === undefined) {
            Ext.MessageBox.alert(this.app.i18n._('Attender'), this.app.i18n._('Do not you an attender in Webconference!'));
            return;
        }
        var record = store.data;
        if (record.status === 'E') {
            Ext.MessageBox.alert(this.app.i18n._('Room Expired'), this.app.i18n._('The Webconference you are trying to join no longer exists'));
            return;
        }

        var _this = this;
        Tine.Webconference.isMeetingActive(record.room_name, record.wconf_config_id, function (response) {
            if (!response.active) {
                Ext.MessageBox.alert(_this.app.i18n._('Room Expired'), _this.app.i18n._(response.message));
                _this.loadGridData();
                return;
            } else {
                _this.startConference(store);
            }
        });
    },

    startConference: function (store) {
        var myAttenderRecord = store.getMyAttenderRecord().data;
        var record = store.data;
        Ext.MessageBox.confirm('',
            String.format(this.app.i18n._('Do you want to join the room {0}'), record.title) + ' ?',
            function (btn) {
                if (btn === 'yes') {
                    Tine.Tinebase.appMgr.get('Webconference').roomId = record.id;
                    Tine.Tinebase.appMgr.get('Webconference').record = store;
                    Tine.Tinebase.appMgr.get('Webconference').logAccessLogin();
                    this.app.conferenceDialog = Tine.Webconference.ConferenceDialog.openWindow({
                        url: myAttenderRecord.room_url,
                        roomId: record.id,
                        moderator: myAttenderRecord.role === 'MODERATOR',
                        record: record,
                        listeners: {
                            scope: this,
                            'refresh': this.loadGridData
                        }
                    });
                }
            }, this);
    },

    onStoreBeforeLoadRecords: function (o, options, success) {
        return this.lastStoreTransactionId === options.transactionId;
    },

    /**
     * called after a new set of Records has been loaded
     *
     * @param  {Ext.data.Store} this.store
     * @param  {Array}          loaded records
     * @param  {Array}          load options
     * @return {Void}
     */
    onStoreLoad: function(store, records, options) {
        if (this.filterToolbar) {
            this.filterToolbar.setValue(store.proxy.jsonReader.jsonData.filter);
        }
        // update container tree
        Tine.Tinebase.appMgr.get('Webconference').getMainScreen().getWestPanel().getContainerTreePanel().getFilterPlugin().setValue(store.proxy.jsonReader.jsonData.filter);

        // restore selection
        if (Ext.isArray(options.preserveSelection)) {
            Ext.each(options.preserveSelection, function(record) {
                var row = this.store.indexOfId(record.id);
                if (row >= 0) {
                    this.grid.getSelectionModel().selectRow(row, true);
                }
            }, this);
        }

        // restore scroller
        if (Ext.isNumber(options.preserveScroller)) {
            this.grid.getView().scroller.dom.scrollTop = options.preserveScroller;
        }

        // reset autoRefresh
        if (window.isMainWindow && this.autoRefreshInterval) {
            this.autoRefreshTask.delay(this.autoRefreshInterval * 1000);
        }
    },

    /**
     * @private
     */
    initDetailsPanel: function() {
        this.detailsPanel = new Tine.Webconference.RoomDetailsPanel({
            grid : this,
            app: this.app
        });
    }
});
