<?php
/**
 * Tine 2.0
 *
 * @package     Webconference
 * @subpackage  Setup
 * @license     http://www.gnu.org/licenses/agpl.html AGPL3
 * @copyright   Copyright (c) 2013 Metaways Infosystems GmbH (http://www.metaways.de)
 * @author      Philipp Schüle <p.schuele@metaways.de>
 */
class Webconference_Setup_Update_Release0 extends Setup_Update_Abstract
{
    /**
    * this function does nothing. It's from the dark ages without setup being functional
    */
    public function update_1()
    {
        $this->validateTableVersion('wconf_room', '1');
        try {
            $this->_backend->dropForeignKey('wconf_room', 'wconf_room::container_id--container::id');
            $this->_backend->dropCol('wconf_room', 'container_id');
        } catch (Exception $exc) {
            echo "salutation field container_id not exists.\n";
        }
        $this->setTableVersion('wconf_room', '2');
        $this->setApplicationVersion('Webconference', '0.2');
    }
    /**
    * update to 0.2
    * - add default grant for anyone to resources
    */
    public function update_2()
    {
        $this->validateTableVersion('wconf_room', '2');
        $declaration = new Setup_Backend_Schema_Field_Xml(
        '<field>
            <name>organizer</name>
            <type>text</type>
            <length>40</length>
        </field>');
        $this->_backend->addCol('wconf_room', $declaration);

        $declaration = new Setup_Backend_Schema_Index_Xml('
        <index>
            <name>organizer</name>
            <field>
                <name>organizer</name>
            </field>
        </index>');
        $this->_backend->addIndex('wconf_room', $declaration);

        $declaration = new Setup_Backend_Schema_Field_Xml('
        <field>
            <name>container_id</name>
            <type>integer</type>
        </field>');
        try {
            $this->_backend->addCol('wconf_room', $declaration);
        } catch (Exception $e) {
            echo "salutation field container_id already exists.\n";
        }

        $declaration = new Setup_Backend_Schema_Index_Xml('
        <index>
            <name>wconf_room::container_id--container::id</name>
            <field>
                <name>container_id</name>
            </field>
            <foreign>true</foreign>
            <reference>
                <table>container</table>
                <field>id</field>
            </reference>
        </index>');
        try {
            $this->_backend->addForeignKey('wconf_room', $declaration);
        } catch (Exception $e) {
            echo "salutation index wconf_room::container_id--container::id already exists.\n";
        }

        $this->setTableVersion('wconf_room', 3);

        $tableDefinition = "
        <table>
            <name>wconf_attendee</name>
            <version>1</version>
            <declaration>
                <field>
                    <name>id</name>
                    <type>text</type>
                    <length>40</length>
                    <notnull>true</notnull>
                </field>
                <field>
                    <name>wconf_room_id</name>
                    <type>text</type>
                    <length>40</length>
                    <notnull>true</notnull>
                </field>
                <field>
                    <name>room_url</name>
                    <type>text</type>
                    <length>255</length>
                    <notnull>true</notnull>
                </field>
                <field>
                    <name>user_id</name>
                    <type>text</type>
                    <length>40</length>
                    <notnull>true</notnull>
                </field>
                <field>
                    <name>user_type</name>
                    <type>text</type>
                    <length>32</length>
                    <default>user</default>
                    <notnull>true</notnull>
                </field>
                <field>
                    <name>call_date</name>
                    <type>datetime</type>
                </field>
                <field>
                    <name>role</name>
                    <type>text</type>
                    <length>32</length>
                    <default>OWNER</default>
                    <notnull>true</notnull>
                </field>
                <field>
                    <name>displaycontainer_id</name>
                    <type>integer</type>
                </field>

                <!-- defaults for tine system -->
                <field>
                    <name>created_by</name>
                    <type>text</type>
                    <length>40</length>
                </field>
                <field>
                    <name>creation_time</name>
                    <type>datetime</type>
                </field>
                <field>
                    <name>last_modified_by</name>
                    <type>text</type>
                    <length>40</length>
                </field>
                <field>
                    <name>last_modified_time</name>
                    <type>datetime</type>
                </field>
                <field>
                    <name>is_deleted</name>
                    <type>boolean</type>
                    <default>false</default>
                </field>
                <field>
                    <name>deleted_by</name>
                    <type>text</type>
                    <length>40</length>
                </field>
                <field>
                    <name>deleted_time</name>
                    <type>datetime</type>
                </field>
                <!-- end defaults for tine system -->
                <index>
                    <name>id</name>
                    <primary>true</primary>
                    <field>
                        <name>id</name>
                    </field>
                </index>
                <index>
                    <name>wconf_attendee::wconf_room_id-wconf_room::id</name>
                    <field>
                        <name>wconf_room_id</name>
                    </field>
                    <foreign>true</foreign>
                    <reference>
                        <table>wconf_room</table>
                        <field>id</field>
                        <ondelete>CASCADE</ondelete>
                        <!-- add onupdate? -->
                    </reference>
                </index>
                <index>
                    <name>user_id</name>
                    <field>
                        <name>user_id</name>
                    </field>
                </index>
                <index>
                    <name>user_type</name>
                    <field>
                        <name>user_type</name>
                    </field>
                </index>
                <index>
                    <name>wconf_attendee::displaycontainer_id--container::id</name>
                    <field>
                        <name>displaycontainer_id</name>
                    </field>
                    <foreign>true</foreign>
                    <reference>
                        <table>container</table>
                        <field>id</field>
                    </reference>
                </index>
                <index>
                    <name>wconf_room_id</name>
                    <field>
                        <name>wconf_room_id</name>
                    </field>
                </index>
                <index>
                    <name>displaycontainer_id</name>
                    <field>
                        <name>displaycontainer_id</name>
                    </field>
                </index>
            </declaration>
        </table>";
        $table = Setup_Backend_Schema_Table_Factory::factory('String', $tableDefinition);
        $this->_backend->createTable($table);
        $pfe = new Tinebase_PersistentFilter_Backend_Sql();
        $commonValues = array(
            'account_id'        => NULL,
            'application_id'    => Tinebase_Application::getInstance()->getApplicationByName('Webconference')->getId(),
            'model'             => 'Webconference_Model_RoomFilter',
        );

        $myEventsPFilter = $pfe->create(new Tinebase_Model_PersistentFilter(array_merge($commonValues, array(
            'name'              => 'All my rooms' ,
            'description'       => "All Rooms I attend", // _("All events I attend")
            'filters'           =>
            array(
                array('field' => 'status', 'operator' => 'in', 'value' => array('A')),
                array('field' => 'attender'    , 'operator' => 'equals', 'value' => array(
                    'user_type' => Webconference_Model_Attender::USERTYPE_USER,
                    'user_id'   => Addressbook_Model_Contact::CURRENTCONTACT,))
                )
        ))));

        $pfe->create(new Tinebase_Model_PersistentFilter(array_merge($commonValues, array(
            'name'              => "I'm organizer",
            'description'       => "Rooms I'm the organizer of",
            'filters'           => array(
                array('field' => 'status', 'operator' => 'in', 'value' => array('A')),
                array('field' => 'organizer', 'operator' => 'equals', 'value' => Addressbook_Model_Contact::CURRENTCONTACT)
            )
        ))));

        $cb = new Tinebase_Backend_Sql(array(
            'modelName' => 'Tinebase_Model_Config',
            'tableName' => 'config',
        ));

        $attendeeRolesConfig = array(
            'name'    => Webconference_Config::ATTENDEE_ROLES,
            'records' => array(
            array('id' => 'MODERATOR', 'value' => 'Moderator', 'system' => true),
            array('id' => 'ATTENDEE', 'value' => 'Attendee', 'system' => true),
            ),
        );

        $cb->create(new Tinebase_Model_Config(array(
            'application_id'    => Tinebase_Application::getInstance()->getApplicationByName('Webconference')->getId(),
            'name'              => Webconference_Config::ATTENDEE_ROLES,
            'value'             => json_encode($attendeeRolesConfig),
        )));

        $roomStatusConfig = array(
            'name'    => Webconference_Config::ROOM_STATUS,
            'records' => array(
            array('id' => 'A', 'value' => 'Active', 'system' => true),
            array('id' => 'E', 'value' => 'Expired', 'system' => true),),
        );

        $cb->create(new Tinebase_Model_Config(array(
            'application_id'    => Tinebase_Application::getInstance()->getApplicationByName('Webconference')->getId(),
            'name'              => Webconference_Config::ROOM_STATUS,
            'value'             => json_encode($roomStatusConfig),
        )));

        $this->setApplicationVersion('Webconference', '1.0');
    }
}
