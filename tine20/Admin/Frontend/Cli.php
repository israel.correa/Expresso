<?php
/**
 * Tine 2.0
 * @package     Admin
 * @subpackage  Frontend
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Philipp Schuele <p.schuele@metaways.de>
 * @copyright   Copyright (c) 2009-2010 Metaways Infosystems GmbH (http://www.metaways.de)
 * 
 */

/**
 * cli server for Admin
 *
 * This class handles cli requests for the Admin
 *
 * @package     Admin
 * @subpackage  Frontend
 */
class Admin_Frontend_Cli extends Tinebase_Frontend_Cli_Abstract
{
    /**
     * the internal name of the application
     *
     * @var string
     */
    protected $_applicationName = 'Admin';
    
    /**
     * help array with function names and param descriptions
     */
    protected $_help = array(
        'importUser' => array(
            'description'   => 'Import new users into the Admin.',
            'params'        => array(
                'filenames'   => 'Filename(s) of import file(s) [required]',
                'definition'  => 'Name of the import definition or filename [required] -> for example admin_user_import_csv(.xml)',
            )
        ),
    );
    
    /**
     * import users
     *
     * @param Zend_Console_Getopt $_opts
     */
    public function importUser($_opts)
    {
        parent::_import($_opts);
    }

    /**
     * import groups
     *
     * @param Zend_Console_Getopt $_opts
     */
    public function importGroups($_opts)
    {
        parent::_import($_opts);
    }

    /**
     * repair groups
     *
     * * add missing lists
     * * checks if list container has been deleted (and hides groups if that's the case)
     *
     * @see 0010401: add repair script for groups without list_ids
     */
    public function repairGroups()
    {
        $count = 0;
        $be = new Tinebase_Group_Sql();
        $listBackend = new Addressbook_Backend_List();

        $groups = $be->getGroups();

        foreach ($groups as $group) {
            if ($group->list_id == null) {
                $list = Addressbook_Controller_List::getInstance()->createByGroup($group);
                $group->list_id = $list->getId();
                $group->visibility = Tinebase_Model_Group::VISIBILITY_DISPLAYED;
                if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__
                        . ' Add missing list for group ' . $group->name);
                $be->updateGroupInSqlBackend($group);
                $count++;
            } else if ($group->visibility === Tinebase_Model_Group::VISIBILITY_DISPLAYED) {
                try {
                    $list = $listBackend->get($group->list_id);
                    $listContainer = Tinebase_Container::getInstance()->get($list->container_id);
                } catch (Tinebase_Exception_NotFound $tenf) {
                    if (Tinebase_Core::isLogLevel(Zend_Log::INFO)) Tinebase_Core::getLogger()->info(__METHOD__ . '::' . __LINE__
                            . ' Hide group '  . $group->name . ' without list / list container.');
                    $group->visibility = Tinebase_Model_Group::VISIBILITY_HIDDEN;
                    $be->updateGroupInSqlBackend($group);
                    $count++;
                }
            }
        }
        echo $count . " groups repaired!\n";
    }
}
