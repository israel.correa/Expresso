<?php

/**
 * Tine 2.0
 *
 * @package     Admin
 * @subpackage  Controller
 * @license     http://www.gnu.org/licenses/agpl.html AGPL Version 3
 * @author      Mário César Kolling <mario.kolling@serpro.gov.br>
 * @copyright   Copyright (c) 2007-2012 Metaways Infosystems GmbH (http://www.metaways.de)
 * @copyright   Copyright (c) 2014 SERPRO (https://www.serpro.gov.br)
 *
 */

/**
 * CkeckEmails Controller for Admin_Controller
 *
 * @package     Admin
 * @subpackage  Controller
 */
class Admin_Controller_CheckEmail extends Tinebase_Controller_Abstract
{

    /**
     * holds the instance of the singleton
     *
     * @var Admin_Controller_MailList
     */
    private static $_instance = NULL;

    /**
     * the constructor
     *
     * don't use the constructor. use the singleton
     */
    private function __construct()
    {
        $this->_applicationName = 'Admin';
    }

    /**
     * don't clone. Use the singleton.
     *
     */
    private function __clone()
    {
    }

     /**
     * the singleton pattern
     *
     * @return Admin_Controller_MailList
     */
    public static function getInstance()
    {
        if (self::$_instance === NULL) {
            self::$_instance = new Admin_Controller_CheckEmail();
        }
        return self::$_instance;
    }

     /**
     *Check the availability of the email on the user backend
     *
     * @param array $_arrEmails
     * @return array
     */
    public function checkEmail(array $_arrEmails)
    {
        $this->checkRight('MANAGE_ACCOUNTS');
        $fromUser = Tinebase_User::getInstance()->checkEmail($_arrEmails);
        $fromList = Admin_Controller_MailList::getInstance()->checkEmail($_arrEmails);

        $returnData = array_merge(array_intersect($fromUser['data'], $fromList['data']),
                        array_diff($fromUser['data'], $fromList['data']));

        $return = array(
            'data'          => $returnData,
            'totalcount'    => count($returnData),
            'status'        => $fromUser['status'] === 'success' && $fromList['status'] === 'success' ?
                                    'success' : 'failure',
        );

        return $return;
    }

}
