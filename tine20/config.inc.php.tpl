<?php
// GLOBAL CONFIGURATION FILE
// NOTE: You can either:
//  - copy this file to config.inc.php and add change config values
//  - create an empty config.inc.php, make it writeable to the webserver and edit config via the setup.php interface
//  - make this directory temporary writeable to the webserver and edit config via the setup.php interface

return array(
    /*************************************************************************
    /*------------------------- GENERAL CONFIGURATION-----------------------*/
    /************************************************************************/

    //APPLICATION ADMINISTRATOR ACCOUNT (SETUP ACCESS)
    'setupuser' =>
        array (
            'username' => '[USERNAME]',
            'password' => '[PASSWORD]',
        ),

    //TEMPORARY FILES
    'tmpdir' => '',

    //TEMPORARY UPLOAD FILES
    'filesdir' => '',

    //SESSIONS
    'session' =>
        array (
            'lifetime' => 86400,
            'backend' => 'File',
            'path' => '',
            'host' => 'localhost',
            'port' => 6379,
            'storeAclIntoSession' => false,
            'storePreferenceIntoSession' => false,
        ),

    //DATA CACHE
    'caching' =>
        array (
            'active' => false,
            'customexpirable' => false,
            'backend' => 'File',
            'lifetime' => 900,
            'path' => '',

            //REDIS
            'redis' =>
            array (
                    'host' => 'localhost',
                    'port' => 6379,
                ),

            //MEMCACHED
            'memcached' =>
                array (
                    'host' => 'localhost',
                    'port' => 11211,
                ),
        ),

    /*************************************************************************
    /*------------------------------ ADDRESSBOOK ----------------------------*/
    /************************************************************************/

    //#2 - AVAILABILITY MAP
    'mapPanel' => 0,

    /*************************************************************************
    /*--------------------------- DEVELOPMENT ------------------------------*/
    /************************************************************************/
    //APPLICATION LOG RESOURCE
    /* 'active': if resource is enabled(boolean);
     * 'priority': captured event detail level. How high
     *             the level, more details (and data) will be inserted as
     *             content of log file. Levels are:
     *
     *               0=EMERGENCY
     *               1=ALERT
     *               2=CRITICAL
     *               3=ERROR
     *               4=WARN
     *               5=NOTICE
     *               6=INFO
     *               7=DEBUG
     *               8=TRACE
     *
     * 'filename': where log file is.*/

    'logger' =>
        array (
            'active' => false,
            'priority' => 7,
            'filename' => '/tmp/tine20.log',
        ),

    //PROFILER RESOURCE SUPPORT
    'profiler' =>
        array (
            'xhprof' => false,
            'queryProfiles' => false,
            'queryProfilesDetails' => false
        ),
    /*************************************************************************
    /*---------------------------- MESSAGES --------------------------------*/
    /************************************************************************/

    //MESSAGE QUEUE VIA REDIS
    'actionqueue' =>
        array (
            'active' => false,
            'backend' => 'Redis',
            'host' => 'localhost',
            'port' => 6379,
        ),

    'email' =>
        array (
            /* MAXIMUM NUMBER OF UNKNOWN CONTACTS TO BE ADDED TO COLLECTED CATALOGS
            * used to limit maximum number of unknown contacts
            * to be added to addressbook when user sends a message.
            */
            'maxContactAddToUnknown' => 10,
            //MAXIMUM E-MAIL MESSAGE SIZE
            'maxMessageSize' => 15000000,
        ),

    /*************************************************************************
    /*------------------------------- MOBILE --------------------------------*/
    /************************************************************************/

    /* REDIRECTING OF MOBILE DEVICE USER
     * 'plugins': used to configure options for plugable behaviors, like
     * redirecting for mobile device access
     */
    'plugins' =>
        array (
            'active' => false,
            'mobile' =>
                array (
                    'redirect' => false,
                    'url' => '',
                ),
        ),

    /*************************************************************************
    /*------------------------------ SECURITY ------------------------------*/
    /************************************************************************/

    //MAXIMUM NUMBER OF USEFUL LOGIN TRIALS BEFORE LOCKING USER
    'maxLoginFailures' => 20,

    // DIGITAL CERTIFICATE
    'modssl' =>
        array (
            'username_callback' => '',
            'casfile' => '',
            'crlspath' => '',
        ),

    //CAPTCHA (ANTI ROBOT TOOL FOR AUTHENTICATION SCREEN)
    'captcha' =>
        array (
            'count' => 3,
        ),

    /* CORS REQUEST COMPATIBILITY
     * 'allowedJsonOrigins': used into "Config.php" e "Json.php".
     * Define URLs/IPs that allow exchange of JSON in out of HTTP
     * headers requests for complaining CORS.
    */
    'allowedJsonOrigins' =>
        array (
            0 => '',
            1 => '',
        ),

    /* SESSION IP VALIDATION
     * 'sessionIpValidation': used into some scripts, is
     * responsible for validating user session by IP (access
     * constraint to a logon).
    */
    'sessionIpValidation' =>
        array (
            'active' => false,
            'source' => 'ip',
        ),

    /* DIGITAL CERTIFICATE
     * 'certificate': used to definition of digital certificate.
     */
    'certificate' =>
        array (
            'active' => false,
            'useKeyEscrow' => false,
            'masterCertificate' => '',
        ),

    /*************************************************************************
    /*--------------------------- SUPPORT AND INFRASTRUCTURE ---------------*/
    /************************************************************************/

    //URL FOR USER BUG REPORT
    'bugreportUrl' => '',

    //USER DOCUMENTATION
    'helpdoc' =>
        array (
            'active' => false,
            'text' => '',
            'title' => '',
            'url' => '',
        ),
    /* EVOLUTION SURVEY OF TINE20
     * 'denySurveys': used into file "LoginPannel.js"
     * Create a graphical element for attending of user
     * in quality survey of Tine 2.0.
    */
    'denySurveys' => true,

    /* REDIRECTING FOR ANOTHER FRONTEND
     * 'mapPanel': used in several files (among them,
     * "ContactEditDialog.js", enables support to availability
     * contact map.
    */
    'redirecting' =>
        array (
            'active' => false,
            'ldapAttribute' => '',
            'cookieName' => '',
            'defaultCookieValue' => '',
        ),
    /* STATE PROVIDER FOR VISUAL OBJECT POSITIONING
     * 'stateProvider': used into some scripts, it is used
     * for defining a state provider. Default value is 'localStorage'.
    */
    'stateprovider' =>
        array (
            'provider' => 'localStorage',
        ),

    /*************************************************************************
    /*-------------------------- VISUALIZATION -----------------------------*/
    /************************************************************************/

    //CONFIGURAÇÕES DO TEMA EM USO
    'theme' => array(
                'load' => false,
                'path' => '',
                'useBlueAsBase' => true,
                'backgroundImageUrl' => '',
                'messageImageUrl' => '',
                'messageLinkUrl' => '',
                'accessibleLinkUrl' => '',
        ),

    /*************************************************************************
    /*-------------------------- MULTIDOMAIN -------------------------------*/
    /************************************************************************/
    'domaindata' => array('domain' => 'DEFAULT DOMAIN'),
);