Tine.Calendar.Printer.EventViewRenderer = Ext.extend(Tine.Calendar.Printer.BaseRenderer, {
    paperHeight: 200,
    stylesheetPath: 'Calendar/css/newPrint.css',
    generateBody: function(view) {
        body = [];
        var start = view.record.data.dtstart.format('Y-m-d H:i') + ' ('+ view.record.data.originator_tz  +')';
        var Time = view.record.data.dtstart.format('H:i') + ' - ' + view.record.data.dtend.format('H:i') + ' ('+ view.record.data.originator_tz  +')';
        Attendees=[];
        var containerName='';
        for (var i=0; i<view.record.data.attendee.length; i++) {
            Attendees += this.attendeesTpl.apply({
                        attendee: view.record.data.attendee[i].user_id.n_fn
                        });
            if(Tine.Tinebase.registry.get('currentAccount').contact_id == view.record.data.attendee[i].user_id.id){
               if(view.record.data.attendee[i].displaycontainer_id instanceof Object)
                      {
                          containerName = view.record.data.attendee[i].displaycontainer_id.name;
                      }
            }
        }

        body = this.eventTpl.apply({
                startDate: start,
                endDate: view.record.data.dtend.format('Y-m-d H:i'),
                summary: Ext.util.Format.htmlEncode(view.record.data.summary),
                time: Time,
                container: containerName,
                local: view.record.data.location ? view.record.data.location : '' ,
                organizer: view.record.data.organizer.n_fn,
                description: view.record.data.description ?  view.record.data.description.replace(/\r?\n/g, '<br />') : '',
                attendees: Attendees,
                translateStart: Tine.Tinebase.appMgr.get('Calendar').i18n._('Start Time'),
                translateEnd: Tine.Tinebase.appMgr.get('Calendar').i18n._('End Time'),
                translateTimes: Tine.Tinebase.appMgr.get('Calendar').i18n._('Time'),
                translateLocal: Tine.Tinebase.appMgr.get('Calendar').i18n._('Location'),
                translateOrganizer: Tine.Tinebase.appMgr.get('Calendar').i18n._('Organizer'),
                translateDescription: Tine.Tinebase.appMgr.get('Calendar').i18n._('Description'),
                translateNotes: Tine.Tinebase.appMgr.get('Calendar').i18n._('Notes'),
                translateAttendees: Tine.Tinebase.appMgr.get('Calendar').i18n._('Attendees')
       });
       return body;
    },

    getTitle: function(view) {
                var containerName='';
                for (var i=0; i<view.record.data.attendee.length; i++) {
                   if(Tine.Tinebase.registry.get('currentAccount').contact_id == view.record.data.attendee[i].user_id.id){
                      if(view.record.data.attendee[i].displaycontainer_id instanceof Object)
                      {
                         containerName = view.record.data.attendee[i].displaycontainer_id.name;
                      }
                   }
              }
               return Ext.util.Format.htmlEncode(containerName);
    },

    attendeesTpl: new Ext.XTemplate(
        '<li>{attendee}</li>'
    ),

    /**
     * @property eventTpl
     * @type Ext.XTemplate
     *  override eventTpl from base
     */
    eventTpl: new Ext.XTemplate(
        '<div>',
             '<div align="left" style="border-bottom-width: 1; border-bottom-color: gray; border-bottom-style: solid; margin-top: 0; " name="title" id="testePrint"><h1>{summary}</h1>',
             '<p>{container}</p>',
             '</div>',
             '<table>',
             '<tr>',
             '<td width="65%" align="justify" valign="top">',
             '<div class="pt2" name="Dates" id="DatesPrint">',
             '<p class="bottom">{translateStart}:</p><h4 class="top" >{startDate}</h4>',
             '<p class="bottom">{translateEnd}:</p><h4 class="top" >{endDate}</h4>',
             '</div>',
             '<div class="pt2" name="hora" id="horaPrint"><p class="bottom">{translateTimes}:</p><h4 class="top" >{time}</h4></div>',
             '<div class="pt2" name="local" id="localPrint"><p class="bottom">{translateLocal}:</p><h4 class="top" >{local}</h4></div>',
             '<div class="pt2" name="organizer" id="organizerPrint"><p class="bottom">{translateOrganizer}:</p><h4 class="top" >{organizer}</h4></div>',
             '<div class="pt2" name="desc" id="descPrint"><p style="margin-bottom: 1;">{translateDescription}:</p><p style="word-break:break-all;">{description}</p></div>',
             '<div class="pt2" name="desc" id="notePrint"><p class="bottom">{translateNotes}:</p></div>',
             '</td>',
             '<td style="border-left-style: dotted; border-left-color: gray; border-left-width: 1; " width="35%" align="justify" valign="top">',
             '<div class="pt2" margin-left: 10px; " name="atteends" id="atteendsPrint"><h4>{translateAttendees}</h4></div>',
             '<div align="left" name="atteend" id="atteendPrint">',
             '<ul class="ex1">',
             '{attendees}',
             '</ul>',
             '</div>',
             '</td>',
             '</tr>',
             '</table>',
        '</div>'
    )

});
